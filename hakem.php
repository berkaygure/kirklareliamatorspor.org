<? include_once "_header_.php" ?>
<?

?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-8">
                <h4 class="blog-title">Hakem ve Gözlemci Listeleri</h4>

                <article style="padding: 20px;">
                    <?
                    $path = "files/hakem_ve_gozlemci/";
                    $dokumanlar = scandir($path);
                    if(count($dokumanlar)-2 <= 0){
                        ?>
                        <div class="text-md-center">
                        <img src="img/warning.png" alt="Hiç Dosya Yüklenmemiş">
                        <h4> Hiç Dosya Yüklenmemiş</h4>
                        </div>
                        <?
                    }else {?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Dosya</th>
                                    <th>Boyut</th>
                                    <th>Tarih</th>

                                </tr>
                            </thead>
                            <tbody>
                        <?
                        foreach ($dokumanlar as $doc) { ?>
                            <?if(strlen($doc) > 3){?>
                                <tr>
                                    <td><a class="btn btn-primary btn-sm"  href="download.php?t=hakem&file=<?=base64_encode($doc)?>"><i class="fa fa-download"></i> İndir </a></td>

                                    <td><?= $doc ?></td>

                                    <td>
                                        <span class="tag tag-default tag-pill float-xs-right"><?=file_size($path . $doc)?></span>
                                    </td>
                                    <td>
                                        <span class="tag tag-default tag-primary float-xs-right"><?=gen_tarih($path . $doc)?></span>
                                    </td>
                                </tr>
                            <?}?>


                        <?}?>
                            </tbody>
                        </table>
                        <?
                    }
                    ?>
                </article>
                <div class="clearfix"></div>

            </div>
            <div class="col-md-4">
                <div class="card card-outline-success">
                    <div class="card-header">
                        <h6><i class="fa fa-cloud"></i> Kırklareli 5 günlük Hava Tahmini</h6>
                    </div>
                    <ul class="list-group">
                        <img src="http://www.mgm.gov.tr/sunum/tahmin-show-2.aspx?m=KIRKLARELI&basla=0&bitir=5&rC=111&rZ=fff" class="img-fluid" alt="KIRKLARELİ" />
                    </ul>
                </div>
                <? include_once "widget/leagueTable/league_table.php";?>
            </div>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
