<?php
if(isset($_GET["id"])) {
    include "_header_.php";
    $ligDataQuery = $database->prepare("SELECT * FROM leagues WHERE ID=:id");
    $ligDataQuery->bindValue(":id", $_GET["id"]);
    $ligDataQuery->execute();
    $ligData = $ligDataQuery->fetch(PDO::FETCH_ASSOC);
    $hafta = $ligData["AKTIF_HAFTA"];

    ?>

    <div class="container">
        <div class="outer-content">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="lig-title text-warning"><?=$ligData["league_name"]?></h2>
                    <input type="hidden" value="<?=$ligData["ID"]?>" id="ligId">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#fikstur" role="tab">Fikstür, Sonuçlar ve Puan Durumu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#takimlar" role="tab">Takımlar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#istatistik" role="tab">İstatistikler</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="fikstur" role="tabpanel">
                            <div class="row" style="background-color: #eee;padding: 10px;">
                                <div class="col-md-9">
                                    <h4 style="padding-top: 6px;"><?=$config["sezon"]?> Sezonu - <span id="textHafta"><?=$hafta?></span> . Hafta</h4>
                                </div>
                                <div class="col-md-3 text-md-right">
                                    <select  id="fiksturHafta" class="form-control">
                                        <?for($i = 1 ; $i <= $ligData["week_count"]; $i++):?>
                                            <option <?=($i==$hafta)?"selected":""?> value="<?=($i)?>"><?=$i?>. Hafta</option>
                                        <?endfor;?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-12">
                                    <table class="table  text-md-center table-striped">
                                        <tbody id="fixtureContent">
                                    <?
                                    $fikstur = $database->query("SELECT * FROM  maclar WHERE  LIG_ID=" . $_GET["id"] . " and HAFTA=" . $hafta)->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($fikstur as $f):
                                        $home = $database->query("SELECT tname FROM takimlar WHERE id=" . $f["hteam"])->fetch(PDO::FETCH_ASSOC);
                                        $away = $database->query("SELECT tname FROM takimlar WHERE id=" . $f["ateam"])->fetch(PDO::FETCH_ASSOC);
                                        ?>
                                        <tr>
                                            <td class="vert-align"><?=$home["tname"]?></td>
                                            <td class="vert-align">
                                                <? if($f["hscore"]==-1 || $f["ascore"]==-1) {?>

                                                    <span class="tag tag-primary"><i class="fa fa-calendar"></i> <?=($f["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["tarih"])?></span><br>
                                                    <span class="tag tag-warning">-</span> -
                                                    <span class="tag tag-warning">-</span><br>
                                                    <?if(trim($f["STAD"])!="YOK"):?>
                                                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["STAD"]?></span><br>
                                                    <?endif;?>
                                                <? } else {?>
                                                    <span class="tag tag-default"><i class="fa fa-calendar"></i> <?=($f["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["tarih"])?></span><br>
                                                    <span class="tag tag-warning"><?=$f["hscore"]?></span> -
                                                    <span class="tag tag-warning"><?=$f["ascore"]?></span><br>
                                                    <?if(trim($f["STAD"])!="YOK"):?>
                                                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["STAD"]?></span><br>
                                                    <?endif;?>
                                                <? }?>
                                                <a href="<?=$config["base"]?>mac/<?=$f["id"]?>">Maç Detayı</a>

                                            </td>
                                            <td class="vert-align"><?=$away["tname"]?></td>
                                        </tr>
                                    <?endforeach;?>

                                        </tbody>
                                    </table>
                                        <a id="printCurrent" class="btn btn-primary btn-sm" href="javascript:void(0)">Seçili Haftayı Yazdır</a>
                                        <a id="printWithTable" class="btn btn-primary btn-sm" href="javascript:void(0)">Seçili Haftayı Puan Durumu ile Birlikte Yazdır</a>
                                        <a id="printAll" class="btn btn-primary btn-sm" href="javascript:void(0)">Tüm Fikstürü Yazdır</a>

                                    <div style="margin-bottom: 10px;" class="clearfix"></div>
                                    <div class="alert alert-info" role="alert">
                                        <strong>*</strong> Saha, maç raporu ve daha fazla bilgi için maç detayına tıklayınız.
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4 style="padding-top: 6px;">Puan Durumu - <span id="textHaftaTable"><?=$hafta?></span>. Hafta</h4>
                                </div>
                                <div class="col-md-6 offset-md-6 text-md-right">
                                    <a id="genel" href="javascript:void(0)"><span style="padding: 5px 10px 5px 10px" class="tag tag-success">Genel</span></a>
                                    <a id="home" href="javascript:void(0)"><span  style="padding: 5px 10px 5px 10px" class="tag tag-info">İç Saha</span></a>
                                    <a id="away" href="javascript:void(0)"><span  style="padding: 5px 10px 5px 10px" class="tag tag-info">Dış Saha</span></a>
                                </div>
                                <div class="col-md-12" style="margin-top: 6px">
                                    <table class="table table-striped table-sm">
                                        <thead>
                                        <tr style="font-size:11px;">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class=" text-md-center">O</th>
                                            <th class=" text-md-center">G</th>
                                            <th class=" text-md-center">B</th>
                                            <th class=" text-md-center">M</th>
                                            <th class=" text-md-center">A</th>
                                            <th class=" text-md-center">Y</th>
                                            <th class=" text-md-center">AV</th>
                                            <th class=" text-md-center">P</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tableContent">
                                        <?php
                                        $ligID = $ligData["ID"];
                                        $table = getLeagueTableNew($database,$ligID,$hafta);
                                        $gecenHafta = false;
                                        if($hafta > 1){
                                            $gecenHafta = getLeagueTableNew($database,$ligID,$hafta-1);
                                        }
                                        ?>
                                        <?for($i=0; $i < count($table); $i++):?>
                                            <tr>
                                                <td class=" text-md-center">
                                                    <?if($gecenHafta):?>
                                                        <? $last = getTeamFromTable($gecenHafta,$table[$i]["Team"]);?>
                                                        <?if($last > $i):?>
                                                            <i title="Geçen Hafta <?=($last+1)?>" style="color: #2ecc55;" class="fa fa-arrow-up"></i>
                                                        <? elseif($last < $i):?>
                                                            <i title="Geçen Hafta <?=($last+1)?>" style="color: #cd0000;" class="fa fa-arrow-down"></i>
                                                        <? else:?>
                                                            <i title="Geçen Hafta <?=($last+1)?>" class="fa fa-circle"></i>
                                                        <?endif?>
                                                    <? else:?>
                                                        <i class="fa fa-circle"></i>
                                                    <?endif;?>
                                                </td>
                                                <td>
                                                    <h5><?=($i+1)?></h5>
                                                </td>
                                                <td><?=$table[$i]["Team"]?><br><small><?=$table[$i]["Test"]?></small></td>
                                                <td class=" text-md-center"><?=$table[$i]["P"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["W"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["D"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["L"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["F"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["A"]?></td>
                                                <td class=" text-md-center"><?=$table[$i]["GD"]?></td>
                                                <td class=" text-md-center"><strong><?=$table[$i]["Pts"]?></strong></td>
                                            </tr>
                                        <?endfor;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!--<div class="tab-pane" id="puandurumu" role="tabpanel">
                            <div class="row" style="background-color: #eee;padding: 10px;">
                                <div class="col-md-9">
                                    <h4 style="padding-top: 6px;">2016-2017 Sezonu - <span id="textHaftaTable">8</span>. Hafta</h4>
                                </div>
                                <div class="col-md-3 text-md-right">
                                    <select id="tableHafta" class="form-control">
                                        <?for($i = 1 ; $i <= $ligData["week_count"]; $i++):?>
                                            <option <?=($i==$hafta)?"selected":""?> value="<?=($i)?>"><?=$i?>. Hafta</option>
                                        <?endfor;?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">

                            </div>
                        </div>-->
                        <div class="tab-pane" id="takimlar" role="tabpanel">
                            <div class="row" style="background-color: #eee;padding: 10px;">
                                <div class="col-md-12">
                                    <h4 style="padding-top: 6px;">Takımlar</h4>
                                </div>
                            </div>
                            <table style="margin-top: 10px" class="table table-hover table-striped text-md-center">
                                <thead>
                                <tr>
                                    <th class="text-md-left"></th>
                                    <th class="text-md-center">Takım Notları</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? $takimlar = $database->query("SELECT * FROM takimlar WHERE lig_id =" . $_GET["id"])->fetchAll(PDO::FETCH_ASSOC);?>
                                <? foreach ($takimlar as $takim):?>
                                <tr>
                                    <td class="text-md-left vert-align"><?=$takim["tname"]?></td>

                                    <td class="text-md-center vert-align"><?=$takim["takim_notlari"]?></td>
                                </tr>
                                <? endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="istatistik" role="tabpanel">
                            <div class="row" style="background-color: #eee;padding: 10px;">
                                <div class="col-md-12">
                                    <h4 style="padding-top: 6px;">İstatistikler</h4>
                                </div>
                            </div>
                            <p style="padding: 10px;text-align: center">Bu kısım hazırlanıyor!<br><i>Anlayışınız için teşekkürler...</i></p>
                            <!--<table style="margin-top: 10px" class="table  table-striped ">
                                <tbody>
                                <tr>
                                    <td >En çok gol atan takım</td>
                                    <td class="text-md-center">
                                        <? $min = 0; ?>
                                        <? for($i = 0; $i < count($table); $i++):?>

                                        <?endfor;?>
                                        <strong>Veliçeşmespor</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>En fazla gol yiyen takım</td>
                                    <td class="text-md-center"><strong>1.3 gol (Veliçeşmespor)</strong></td>
                                </tr>
                                <tr>
                                    <td>En fazla maç kazanan takım</td>
                                    <td class="text-md-center"><strong>1.3 gol (Veliçeşmespor)</strong></td>
                                </tr>
                                <tr>
                                    <td>En fazla maç kaybeden takım</td>
                                    <td class="text-md-center"><strong>1.3 gol (Veliçeşmespor)</strong></td>
                                </tr>
                                <tr>
                                    <td>En çok berabere kalan takım</td>
                                    <td class="text-md-center"><strong>1.3 gol (Veliçeşmespor)</strong></td>
                                </tr>
                                <tr>
                                    <td>En farklı maç</td>
                                    <td class="text-md-center"><strong>1.3 gol (Veliçeşmespor)</strong></td>
                                </tr>
                                </tbody>
                            </table>-->


                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="card card-outline-success">
                        <div class="card-block text-md-center">
                            <a href="takvim.php?lig=<?=$_GET["id"]?>" target="_blank" class="btn btn-primary "><i class="fa fa-download"></i> Lig Takvimi İndir</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="leagueOverlay" class="panel-overlay-full"><img src="img/loading.gif"> </div>

    <?

    include "_footer_.php";
}else{
    echo "404";
}