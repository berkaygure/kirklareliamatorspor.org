<?         session_destroy(); ?>
<a style="display: none" class="open-popup" href="#popup">Open popup</a>

<div id="popup" class="text-xs-center" style="display: none;  color:#fff">
    <h1>Uygulamayı Edinin</h1>
    <img src="img/app.png" style="height: 200px;width: 100px"/>
    <img src="img/app2.png" style="height: 200px;width: 100px"/>
    <p>Daha iyi bir deneyim için Kırklareli Amatör Spor uygulaması <b>ücretsiz</b> edinebilirsiniz.İstemiyorsanız sağ üsteki çarpıya tıklayıp siteden devam edin.</p>
    <a href="https://play.google.com/store/apps/details?id=com.lyrayazilim.kirklareliamatorspor" class="btn btn-primary btn-xs">Şimdi İndir</a>
</div>
<? include "_header_.php"; ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span style="color:#333">Önemli Duyuru</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <img src="/img/19mayis.png" class="img-fluid" style="">
                </div>
            </div>
        </div>
    </div>
<? include "taskk.php";?>
<div class="container">
        <div class="outer-content">
            <!-- slider -->
            <div class="row">
                <div class="col-md-12">
                    <div id="sliderCarousel" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?
                            $i = 0;
                            $duyurular = $database->query("SELECT * FROM duyurular WHERE manset=1 ORDER by tarih DESC LIMIT 0,5 ")->fetchAll(PDO::FETCH_ASSOC);
                            foreach($duyurular as $duyuru): ?>
                                <div class="carousel-item <?=($i==0)?"active":""?>">
                                    <a href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>">
                                        <img style="width: 741px;height: 400px" src="upload/haberler/<?=$duyuru["resim"]?>">
                                    </a>
                                    <div class="carousel-caption">
                                        <h4><a href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>"><?=$duyuru["baslik"]?></a></h4>
                                        <p>
                                            <? $ozetLen = strlen($duyuru["aciklama"]);
                                            echo mb_substr(stripslashes($duyuru["aciklama"]),0,120);
                                            if($ozetLen > 120){
                                                echo "...";
                                            }
                                            ?>
                                            <br>
                                            <span class="tag tag-primary" ><?=turkcetarih('j F Y',$duyuru["tarih"]);  ?></span>
                                        </p>
                                    </div>
                                </div><!-- End Item -->
                             <?$i++?>
                            <?endforeach;?>

                        </div><!-- End Carousel Inner -->


                        <ul class="list-group col-sm-4">
                            <?$i = 0; foreach($duyurular as $duyuru): ?>
                            <li data-target="#sliderCarousel" data-slide-to="<?=$i?>" class="list-group-item <?=($i==0)?"active":""?>"><?=$duyuru["baslik"]?></li>
                            <?$i++;endforeach;?>
                        </ul>

                        <!-- Controls -->
                        <div class="carousel-controls">
                            <a class="left carousel-control" href="#sliderCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#sliderCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>

                    </div><!-- End Carousel -->
                </div>
            </div>
            <!-- end slider -->

            <div class="cards">
                <div class="row">
                    <div class="col-md-2">
                        <a href="<?=$config["base"]?>statuler.html">
                            <img class="img-rounded m-x-auto d-block" src="img/home/statu.png" alt="Amatör Lig Statüleri">
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a href="<?=$config["base"]?>dokuman-bankasi.html">
                            <img class="img-rounded m-x-auto d-block" src="img/home/dokuman.png" alt="Doküman Bankası">
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a href="<?=$config["base"]?>haftanin-maclari.html">
                            <img class="img-rounded m-x-auto d-block" src="img/home/fixture.png" alt="Haftalık Müsabaka Programı">
                        </a>
                    </div>

                    <div class="col-md-2">
                        <a target="_blank" href="http://www.tff.org/Resources/TFF/Documents/LIGLER/Amatorler/Amator_2019-2020.pdf">
                            <img class="img-rounded m-x-auto d-block" src="img/home/kilavuz.png?v=1" alt="19/20 Amatör Futbol Liglerinde Uygulanacak Esaslar">
                        </a>
                    </div>


                    <div class="col-md-2">
                        <a target="_blank" href="http://www.resmigazete.gov.tr/eskiler/2011/04/20110414-6.htm">
                            <img class="img-rounded m-x-auto d-block" src="img/home/siddet.png" alt="Sporda Şiddet Yasası">
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a target="_blank" href="http://www.tff.org/default.aspx?pageId=919">
                        <img class="img-rounded m-x-auto d-block" src="img/home/online.png" alt="Online Başvurular">
                        </a>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-outline-warning">
                            <h6 class="card-header  card-warning"><i class="fa fa-newspaper-o"></i> Arşivden Haberler</h6>
                            <div class="card-block">
                                <div class="row">
                                    <?$arsivDuyuru = $database->query("SELECT * FROM duyurular WHERE 1=1 ORDER by tarih DESC LIMIT 5,4 ")->fetchAll(PDO::FETCH_ASSOC);?>
                                    <? foreach ($arsivDuyuru as $duyuru): ?>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <a href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>"><img style="width: 320px;height: 238px;" class="card-img-top" src="upload/haberler/<?=$duyuru["resim"]?>" alt="<?=$duyuru["baslik"]?>"></a>
                                            <div class="card-block">
                                                <h5 class="card-title"><a href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>">
                                                    <? $baslikLen = strlen($duyuru["baslik"]);
                                                    echo mb_substr(stripslashes($duyuru["baslik"]),0,55);
                                                    if($baslikLen > 55){
                                                        echo " ...";
                                                    }
                                                    ?></a>
                                                </h5>
                                                <p class="card-text">
                                                    <? $ozetLen = strlen($duyuru["aciklama"]);
                                                    echo mb_substr(stripslashes($duyuru["aciklama"]),0,110);
                                                    if($ozetLen > 110){
                                                        echo " ...";
                                                    }
                                                    ?>
                                                </p>
                                                <a href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>" class="btn btn-sm btn-primary">Devamını Oku <i class="fa fa-chevron-right"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <? endforeach;?>


                                </div>


                            </div>
                        </div>

                        <div class="card card-outline-success">
                            <h6 class="card-header  card-success"><i class="fa fa-newspaper-o"></i> TASKK'dan Haberler</h6>
                            <div class="card-block">
                                <div class="row">
                                    <?$i=0;?>
                                    <?foreach ($deger as $d): ?>
                                        <?if($i < 4):?>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <img style="width: 320px;height: 238px;" class="card-img-top" src="<?=$d["RESIM"]?>" alt="<?=$d["BASLIK"]?>">
                                            <div class="card-block">
                                                <h5 title="<?=$d["BASLIK"]?>" class="card-title">
                                                    <? $baslikLen = strlen($d["BASLIK"]);
                                                        echo mb_substr($d["BASLIK"],0,40);
                                                        if($baslikLen > 55){
                                                            echo " ...";
                                                        }
                                                    ?>
                                                </h5>
                                                <p class="card-text">
                                                    <? $ozetLen = strlen($d["OZET"]);
                                                    echo mb_substr($d["OZET"],0,95);
                                                    if($ozetLen > 110){
                                                        echo "...";
                                                    }
                                                    ?>
                                                </p>
                                                <a target="_blank" href="<?=$d["LINK"]?>" class="btn btn-sm btn-primary">Devamını Oku <i class="fa fa-chevron-right"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                        <?endif;;?>
                                        <?$i++;?>
                                    <? endforeach;?>



                                </div>


                            </div>
                        </div>


                    </div>

                    <div class="col-md-4">


                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h6><i class="fa fa-link"></i> Önemli Bağlantılar</h6>
                            </div>
                            <ul class="list-group">
                                <a href="http://www.fifa.com/" target="_blank" title="Fifa" class="list-group-item"> <img src="img/logos/fifa32.png">  FIFA</a>
                                <a href="http://www.uefa.com/" target="_blank" title="Uefa" class="list-group-item"> <img src="img/logos/uefa32.png">  UEFA</a>
                                <a href="http://www.taskk.org.tr/" target="_blank" title="Türkiye Amatör Spor Kulüpleri Konfederasyonu" class="list-group-item"> <img src="img/logos/taskk32.png">  TASKK</a>
                                <a href="http://tff.org/" target="_blank" title="Türkiye Futbol Federasyonu" class="list-group-item"> <img src="img/logos/tff32.png">  TFF</a>
                                <a href="http://www.tff.org/Default.aspx?pageID=130" target="_blank" title="Futbolcu Sorgulama" class="list-group-item"> <img src="img/logos/shoe32.png">  Futbolcu Sorgulama</a>
                                <a href="http://www.tff.org/Resources/TFF/Documents/00000016/TFF/MHK/IFAB-2016-2017-OYUN-KURALLARI-KITABI.pdf" target="_blank" title="Futbol Oyun Kuralları" class="list-group-item"> <img src="img/logos/tff32.png">  Futbol Oyun Kuralları</a>

                            </ul>
                        </div>



                        <? include "widget/leagueTable/league_table.php";?>

                        <? include "widget/weekMatches/matches.php";?>


                        <div class="card card-outline-success">
                            <div class="card-header">
                                <h6><i class="fa fa-cloud"></i> Kırklareli 5 günlük Hava Tahmini</h6>
                            </div>
                            <ul class="list-group">
                                <img src="http://www.mgm.gov.tr/sunum/tahmin-show-2.aspx?m=KIRKLARELI&basla=0&bitir=5&rC=111&rZ=fff" class="img-fluid" alt="KIRKLARELİ" />
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class='row'>
                <div class='col-md-12'>
                    <div class="carousel slide media-carousel" id="media">
                        <div class="carousel-inner">
                            <div class="carousel-item  active">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-1.png"></a>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-2.png"></a>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-3.png"></a>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-4.png"></a>
                                    </div>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-5.png"></a>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <a class="thumbnail" href="#"><img alt="" src="img/clients/client-6.png"></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#media" class="right carousel-control">›</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
<? include "_footer_.php"; ?>
