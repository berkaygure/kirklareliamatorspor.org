<?php
/**
 * Created by PhpStorm.
 * User: Berkay
 * Date: 12.11.2015
 * Time: 11:30
 */

class fikstur
{
    private $takim = 0;
    private $db = "";
    private $hafta ="";
    private $sezon ="";
    private $ligid = 0;
    function __construct($fixType, PDO $db)
    {
        $this->takim = $fixType;
        $this->db = $db;
    }

    public function kaydet($takimlar, $ligid, $sezon, $hafta)
    {
        $this->hafta = $hafta;
        $this->sezon = $sezon;
        $this->ligid=$ligid;
        switch($this->takim){
            case "3":
                $this->save3($takimlar);
                break;
            case "4":
                $this->save4($takimlar);
                break;
            case "5":
                $this->save5($takimlar);
                break;
            case "6":
                $this->save6($takimlar);
                break;
            case "7":
                $this->save7($takimlar);
                break;
            case "8":
                $this->save8($takimlar);
                break;
            case "9":
                $this->save9($takimlar);
                break;
            case "10":
                $this->save10($takimlar);
                break;
            case "11":
                $this->save11($takimlar);
                break;
            case "12":
                $this->save12($takimlar);
                break;
            case "13":
                $this->save13($takimlar);
                break;
            case "14":
                $this->save14($takimlar);
                break;
            case "15":
                $this->save15($takimlar);
                break;
        }

    }
    private function save3($takimlar){
        $this->saveDB($takimlar["T1"],$takimlar["T2"],1,4);

        $this->saveDB($takimlar["T3"],$takimlar["T1"],2,5);

        $this->saveDB($takimlar["T2"],$takimlar["T3"],3,6);
    }
    private function save4($takimlar){
        $this->saveDB($takimlar["T1"],$takimlar["T4"],1,4);
        $this->saveDB($takimlar["T2"],$takimlar["T3"],1,4);

        $this->saveDB($takimlar["T4"],$takimlar["T2"],2,5);
        $this->saveDB($takimlar["T3"],$takimlar["T1"],2,5);

        $this->saveDB($takimlar["T1"],$takimlar["T2"],3,6);
        $this->saveDB($takimlar["T3"],$takimlar["T4"],3,6);
    }

    private function save5($takimlar){
        $this->saveDB($takimlar["T1"],$takimlar["T4"],1,6);
        $this->saveDB($takimlar["T3"],$takimlar["T2"],1,6);


        $this->saveDB($takimlar["T5"],$takimlar["T3"],2,7);
        $this->saveDB($takimlar["T2"],$takimlar["T1"],2,7);

        $this->saveDB($takimlar["T4"],$takimlar["T2"],3,8);
        $this->saveDB($takimlar["T1"],$takimlar["T5"],3,8);


        $this->saveDB($takimlar["T3"],$takimlar["T1"],4,9);
        $this->saveDB($takimlar["T5"],$takimlar["T4"],4,9);


        $this->saveDB($takimlar["T2"],$takimlar["T5"],5,10);
        $this->saveDB($takimlar["T4"],$takimlar["T3"],5,10);
    }


    private function save6($takimlar){
        $this->saveDB($takimlar["T6"],$takimlar["T1"],1,6);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],1,6);
        $this->saveDB($takimlar["T3"],$takimlar["T4"],1,6);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],2,7);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],2,7);
        $this->saveDB($takimlar["T2"],$takimlar["T3"],2,7);

        $this->saveDB($takimlar["T1"],$takimlar["T4"],3,8);
        $this->saveDB($takimlar["T5"],$takimlar["T3"],3,8);
        $this->saveDB($takimlar["T6"],$takimlar["T2"],3,8);


        $this->saveDB($takimlar["T3"],$takimlar["T1"],4,9);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],4,9);
        $this->saveDB($takimlar["T5"],$takimlar["T6"],4,9);


        $this->saveDB($takimlar["T1"],$takimlar["T2"],5,10);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],5,10);
        $this->saveDB($takimlar["T4"],$takimlar["T5"],5,10);

    }


    private function save7($takimlar){
        $this->saveDB($takimlar["T7"],$takimlar["T2"],1,8);
        $this->saveDB($takimlar["T3"],$takimlar["T6"],1,8);
        $this->saveDB($takimlar["T5"],$takimlar["T4"],1,8);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],2,9);
        $this->saveDB($takimlar["T4"],$takimlar["T7"],2,9);
        $this->saveDB($takimlar["T6"],$takimlar["T5"],2,9);


        $this->saveDB($takimlar["T2"],$takimlar["T4"],3,10);
        $this->saveDB($takimlar["T5"],$takimlar["T1"],3,10);
        $this->saveDB($takimlar["T7"],$takimlar["T6"],3,10);



        $this->saveDB($takimlar["T3"],$takimlar["T5"],4,11);
        $this->saveDB($takimlar["T6"],$takimlar["T2"],4,11);
        $this->saveDB($takimlar["T1"],$takimlar["T7"],4,11);


        $this->saveDB($takimlar["T4"],$takimlar["T6"],5,12);
        $this->saveDB($takimlar["T7"],$takimlar["T3"],5,12);
        $this->saveDB($takimlar["T2"],$takimlar["T1"],5,12);



        $this->saveDB($takimlar["T5"],$takimlar["T7"],6,13);
        $this->saveDB($takimlar["T1"],$takimlar["T4"],6,13);
        $this->saveDB($takimlar["T3"],$takimlar["T2"],6,13);



        $this->saveDB($takimlar["T6"],$takimlar["T1"],7,14);
        $this->saveDB($takimlar["T2"],$takimlar["T5"],7,14);
        $this->saveDB($takimlar["T4"],$takimlar["T3"],7,14);


    }

    private function save8($takimlar){
        $this->saveDB($takimlar["T3"],$takimlar["T2"],1,8);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],1,8);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],1,8);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],1,8);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],2,9);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],2,9);
        $this->saveDB($takimlar["T7"],$takimlar["T6"],2,9);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],2,9);


        $this->saveDB($takimlar["T4"],$takimlar["T3"],3,10);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],3,10);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],3,10);
        $this->saveDB($takimlar["T7"],$takimlar["T8"],3,10);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],4,11);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],4,11);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],4,11);
        $this->saveDB($takimlar["T8"],$takimlar["T4"],4,11);


        $this->saveDB($takimlar["T5"],$takimlar["T4"],5,12);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],5,12);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],5,12);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],5,12);



        $this->saveDB($takimlar["T2"],$takimlar["T1"],6,13);
        $this->saveDB($takimlar["T3"],$takimlar["T7"],6,13);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],6,13);
        $this->saveDB($takimlar["T5"],$takimlar["T8"],6,13);



        $this->saveDB($takimlar["T1"],$takimlar["T3"],7,14);
        $this->saveDB($takimlar["T6"],$takimlar["T5"],7,14);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],7,14);
        $this->saveDB($takimlar["T8"],$takimlar["T2"],7,14);


    }

    private function save9($takimlar){
        $this->saveDB($takimlar["T8"],$takimlar["T1"],1,10);
        $this->saveDB($takimlar["T2"],$takimlar["T7"],1,10);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],1,10);
        $this->saveDB($takimlar["T4"],$takimlar["T5"],1,10);

        $this->saveDB($takimlar["T7"],$takimlar["T9"],2,11);
        $this->saveDB($takimlar["T1"],$takimlar["T6"],2,11);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],2,11);
        $this->saveDB($takimlar["T3"],$takimlar["T4"],2,11);


        $this->saveDB($takimlar["T6"],$takimlar["T8"],3,12);
        $this->saveDB($takimlar["T9"],$takimlar["T5"],3,12);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],3,12);
        $this->saveDB($takimlar["T2"],$takimlar["T3"],3,12);



        $this->saveDB($takimlar["T5"],$takimlar["T7"],4,13);
        $this->saveDB($takimlar["T8"],$takimlar["T4"],4,13);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],4,13);
        $this->saveDB($takimlar["T1"],$takimlar["T2"],4,13);


        $this->saveDB($takimlar["T4"],$takimlar["T6"],5,14);
        $this->saveDB($takimlar["T7"],$takimlar["T3"],5,14);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],5,14);
        $this->saveDB($takimlar["T9"],$takimlar["T1"],5,14);



        $this->saveDB($takimlar["T3"],$takimlar["T5"],6,15);
        $this->saveDB($takimlar["T6"],$takimlar["T2"],6,15);
        $this->saveDB($takimlar["T1"],$takimlar["T7"],6,15);
        $this->saveDB($takimlar["T8"],$takimlar["T9"],6,15);



        $this->saveDB($takimlar["T2"],$takimlar["T4"],7,16);
        $this->saveDB($takimlar["T5"],$takimlar["T1"],7,16);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],7,16);
        $this->saveDB($takimlar["T7"],$takimlar["T8"],7,16);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],8,17);
        $this->saveDB($takimlar["T4"],$takimlar["T9"],8,17);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],8,17);
        $this->saveDB($takimlar["T6"],$takimlar["T7"],8,17);


        $this->saveDB($takimlar["T9"],$takimlar["T2"],9,18);
        $this->saveDB($takimlar["T3"],$takimlar["T8"],9,18);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],9,18);
        $this->saveDB($takimlar["T5"],$takimlar["T6"],9,18);


    }


    private function save10($takimlar){
        $this->saveDB($takimlar["T3"],$takimlar["T2"],1,10);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],1,10);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],1,10);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],1,10);
        $this->saveDB($takimlar["T7"],$takimlar["T10"],1,10);

        $this->saveDB($takimlar["T1"],$takimlar["T5"],2,11);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],2,11);
        $this->saveDB($takimlar["T8"],$takimlar["T7"],2,11);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],2,11);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],2,11);


        $this->saveDB($takimlar["T4"],$takimlar["T3"],3,12);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],3,12);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],3,12);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],3,12);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],3,12);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],4,13);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],4,13);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],4,13);
        $this->saveDB($takimlar["T9"],$takimlar["T8"],4,13);
        $this->saveDB($takimlar["T10"],$takimlar["T4"],4,13);


        $this->saveDB($takimlar["T5"],$takimlar["T4"],5,14);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],5,14);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],5,14);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],5,14);
        $this->saveDB($takimlar["T9"],$takimlar["T10"],5,14);



        $this->saveDB($takimlar["T1"],$takimlar["T9"],6,15);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],6,15);
        $this->saveDB($takimlar["T3"],$takimlar["T7"],6,15);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],6,15);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],6,15);



        $this->saveDB($takimlar["T6"],$takimlar["T5"],7,16);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],7,16);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],7,16);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],7,16);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],7,16);


        $this->saveDB($takimlar["T2"],$takimlar["T1"],8,17);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],8,17);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],8,17);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],8,17);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],8,17);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],9,18);
        $this->saveDB($takimlar["T7"],$takimlar["T6"],9,18);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],9,18);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],9,18);
        $this->saveDB($takimlar["T10"],$takimlar["T2"],9,18);


    }

    private function save11($takimlar)
    {
        //hafta 1
        $this->saveDB($takimlar["T2"],$takimlar["T1"],1,12);
        $this->saveDB($takimlar["T3"],$takimlar["T11"],1,12);
        $this->saveDB($takimlar["T4"],$takimlar["T10"],1,12);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],1,12);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],1,12);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],2,13);
        $this->saveDB($takimlar["T8"],$takimlar["T7"],2,13);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],2,13);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],2,13);
        $this->saveDB($takimlar["T11"],$takimlar["T4"],2,13);


        $this->saveDB($takimlar["T3"],$takimlar["T2"],3,14);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],3,14);
        $this->saveDB($takimlar["T5"],$takimlar["T11"],3,14);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],3,14);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],3,14);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],4,15);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],4,15);
        $this->saveDB($takimlar["T9"],$takimlar["T8"],4,15);
        $this->saveDB($takimlar["T10"],$takimlar["T7"],4,15);
        $this->saveDB($takimlar["T11"],$takimlar["T6"],4,15);


        $this->saveDB($takimlar["T4"],$takimlar["T3"],5,16);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],5,16);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],5,16);
        $this->saveDB($takimlar["T7"],$takimlar["T11"],5,16);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],5,16);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],6,17);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],6,17);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],6,17);
        $this->saveDB($takimlar["T10"],$takimlar["T9"],6,17);
        $this->saveDB($takimlar["T11"],$takimlar["T8"],6,17);



        $this->saveDB($takimlar["T5"],$takimlar["T4"],7,18);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],7,18);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],7,18);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],7,18);
        $this->saveDB($takimlar["T9"],$takimlar["T11"],7,18);


        $this->saveDB($takimlar["T1"],$takimlar["T9"],8,19);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],8,19);
        $this->saveDB($takimlar["T3"],$takimlar["T7"],8,19);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],8,19);
        $this->saveDB($takimlar["T11"],$takimlar["T10"],8,19);


        $this->saveDB($takimlar["T6"],$takimlar["T5"],9,20);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],9,20);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],9,20);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],9,20);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],9,20);


        $this->saveDB($takimlar["T1"],$takimlar["T11"],10,21);
        $this->saveDB($takimlar["T2"],$takimlar["T10"],10,21);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],10,21);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],10,21);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],10,21);


        $this->saveDB($takimlar["T7"],$takimlar["T6"],11,22);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],11,22);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],11,22);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],11,22);
        $this->saveDB($takimlar["T11"],$takimlar["T2"],11,22);
    }


    private function save12($takimlar)
    {
        //hafta 1
        $this->saveDB($takimlar["T2"],$takimlar["T1"],1,12);
        $this->saveDB($takimlar["T3"],$takimlar["T11"],1,12);
        $this->saveDB($takimlar["T4"],$takimlar["T10"],1,12);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],1,12);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],1,12);
        $this->saveDB($takimlar["T7"],$takimlar["T12"],1,12);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],2,13);
        $this->saveDB($takimlar["T8"],$takimlar["T7"],2,13);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],2,13);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],2,13);
        $this->saveDB($takimlar["T11"],$takimlar["T4"],2,13);
        $this->saveDB($takimlar["T12"],$takimlar["T2"],2,13);


        $this->saveDB($takimlar["T3"],$takimlar["T2"],3,14);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],3,14);
        $this->saveDB($takimlar["T5"],$takimlar["T11"],3,14);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],3,14);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],3,14);
        $this->saveDB($takimlar["T8"],$takimlar["T12"],3,14);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],4,15);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],4,15);
        $this->saveDB($takimlar["T9"],$takimlar["T8"],4,15);
        $this->saveDB($takimlar["T10"],$takimlar["T7"],4,15);
        $this->saveDB($takimlar["T11"],$takimlar["T6"],4,15);
        $this->saveDB($takimlar["T12"],$takimlar["T3"],4,15);


        $this->saveDB($takimlar["T4"],$takimlar["T3"],5,16);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],5,16);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],5,16);
        $this->saveDB($takimlar["T7"],$takimlar["T11"],5,16);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],5,16);
        $this->saveDB($takimlar["T9"],$takimlar["T12"],5,16);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],6,17);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],6,17);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],6,17);
        $this->saveDB($takimlar["T10"],$takimlar["T9"],6,17);
        $this->saveDB($takimlar["T11"],$takimlar["T8"],6,17);
        $this->saveDB($takimlar["T12"],$takimlar["T4"],6,17);



        $this->saveDB($takimlar["T5"],$takimlar["T4"],7,18);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],7,18);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],7,18);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],7,18);
        $this->saveDB($takimlar["T9"],$takimlar["T11"],7,18);
        $this->saveDB($takimlar["T10"],$takimlar["T12"],7,18);


        $this->saveDB($takimlar["T1"],$takimlar["T9"],8,19);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],8,19);
        $this->saveDB($takimlar["T3"],$takimlar["T7"],8,19);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],8,19);
        $this->saveDB($takimlar["T11"],$takimlar["T10"],8,19);
        $this->saveDB($takimlar["T12"],$takimlar["T5"],8,19);


        $this->saveDB($takimlar["T6"],$takimlar["T5"],9,20);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],9,20);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],9,20);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],9,20);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],9,20);
        $this->saveDB($takimlar["T11"],$takimlar["T12"],9,20);


        $this->saveDB($takimlar["T1"],$takimlar["T11"],10,21);
        $this->saveDB($takimlar["T2"],$takimlar["T10"],10,21);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],10,21);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],10,21);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],10,21);
        $this->saveDB($takimlar["T12"],$takimlar["T6"],10,21);


        $this->saveDB($takimlar["T7"],$takimlar["T6"],11,22);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],11,22);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],11,22);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],11,22);
        $this->saveDB($takimlar["T11"],$takimlar["T2"],11,22);
        $this->saveDB($takimlar["T12"],$takimlar["T1"],11,22);
    }


    private function save13($takimlar)
    {
        //hafta 1
        $this->saveDB($takimlar["T8"],$takimlar["T7"],1,14);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],1,14);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],1,14);
        $this->saveDB($takimlar["T11"],$takimlar["T4"],1,14);
        $this->saveDB($takimlar["T12"],$takimlar["T3"],1,14);
        $this->saveDB($takimlar["T13"],$takimlar["T2"],1,14);


        $this->saveDB($takimlar["T2"],$takimlar["T1"],2,15);
        $this->saveDB($takimlar["T3"],$takimlar["T13"],2,15);
        $this->saveDB($takimlar["T4"],$takimlar["T12"],2,15);
        $this->saveDB($takimlar["T5"],$takimlar["T11"],2,15);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],2,15);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],2,15);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],3,16);
        $this->saveDB($takimlar["T9"],$takimlar["T8"],3,16);
        $this->saveDB($takimlar["T10"],$takimlar["T7"],3,16);
        $this->saveDB($takimlar["T11"],$takimlar["T6"],3,16);
        $this->saveDB($takimlar["T12"],$takimlar["T5"],3,16);
        $this->saveDB($takimlar["T13"],$takimlar["T4"],3,16);


        $this->saveDB($takimlar["T3"],$takimlar["T2"],4,17);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],4,17);
        $this->saveDB($takimlar["T5"],$takimlar["T13"],4,17);
        $this->saveDB($takimlar["T6"],$takimlar["T12"],4,17);
        $this->saveDB($takimlar["T7"],$takimlar["T1"],4,17);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],4,17);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],5,18);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],5,18);
        $this->saveDB($takimlar["T10"],$takimlar["T9"],5,18);
        $this->saveDB($takimlar["T11"],$takimlar["T8"],5,18);
        $this->saveDB($takimlar["T12"],$takimlar["T7"],5,18);
        $this->saveDB($takimlar["T13"],$takimlar["T6"],5,18);



        $this->saveDB($takimlar["T4"],$takimlar["T3"],6,19);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],6,19);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],6,19);
        $this->saveDB($takimlar["T7"],$takimlar["T13"],6,19);
        $this->saveDB($takimlar["T8"],$takimlar["T12"],6,19);
        $this->saveDB($takimlar["T9"],$takimlar["T11"],6,19);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],7,20);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],7,20);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],7,20);
        $this->saveDB($takimlar["T11"],$takimlar["T10"],7,20);
        $this->saveDB($takimlar["T12"],$takimlar["T9"],7,20);
        $this->saveDB($takimlar["T13"],$takimlar["T8"],7,20);


        $this->saveDB($takimlar["T5"],$takimlar["T4"],8,21);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],8,21);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],8,21);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],8,21);
        $this->saveDB($takimlar["T9"],$takimlar["T13"],8,21);
        $this->saveDB($takimlar["T10"],$takimlar["T12"],8,21);


        $this->saveDB($takimlar["T1"],$takimlar["T9"],9,22);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],9,22);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],9,22);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],9,22);
        $this->saveDB($takimlar["T12"],$takimlar["T11"],9,22);
        $this->saveDB($takimlar["T13"],$takimlar["T10"],9,22);


        $this->saveDB($takimlar["T6"],$takimlar["T5"],10,23);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],10,23);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],10,23);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],10,23);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],10,23);
        $this->saveDB($takimlar["T11"],$takimlar["T13"],10,23);


        $this->saveDB($takimlar["T1"],$takimlar["T11"],11,24);
        $this->saveDB($takimlar["T2"],$takimlar["T10"],11,24);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],11,24);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],11,24);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],11,24);
        $this->saveDB($takimlar["T13"],$takimlar["T12"],11,24);


        $this->saveDB($takimlar["T7"],$takimlar["T6"],12,25);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],12,25);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],12,25);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],12,25);
        $this->saveDB($takimlar["T11"],$takimlar["T2"],12,25);
        $this->saveDB($takimlar["T12"],$takimlar["T1"],12,25);



        $this->saveDB($takimlar["T1"],$takimlar["T13"],13,26);
        $this->saveDB($takimlar["T2"],$takimlar["T12"],13,26);
        $this->saveDB($takimlar["T3"],$takimlar["T11"],13,26);
        $this->saveDB($takimlar["T4"],$takimlar["T10"],13,26);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],13,26);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],13,26);
    }


    private function save14($takimlar)
    {
        //hafta 1
        $this->saveDB($takimlar["T8"],$takimlar["T7"],1,14);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],1,14);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],1,14);
        $this->saveDB($takimlar["T11"],$takimlar["T4"],1,14);
        $this->saveDB($takimlar["T12"],$takimlar["T3"],1,14);
        $this->saveDB($takimlar["T13"],$takimlar["T2"],1,14);
        $this->saveDB($takimlar["T14"],$takimlar["T1"],1,14);


        $this->saveDB($takimlar["T2"],$takimlar["T1"],2,15);
        $this->saveDB($takimlar["T3"],$takimlar["T13"],2,15);
        $this->saveDB($takimlar["T4"],$takimlar["T12"],2,15);
        $this->saveDB($takimlar["T5"],$takimlar["T11"],2,15);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],2,15);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],2,15);
        $this->saveDB($takimlar["T8"],$takimlar["T14"],2,15);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],3,16);
        $this->saveDB($takimlar["T9"],$takimlar["T8"],3,16);
        $this->saveDB($takimlar["T10"],$takimlar["T7"],3,16);
        $this->saveDB($takimlar["T11"],$takimlar["T6"],3,16);
        $this->saveDB($takimlar["T12"],$takimlar["T5"],3,16);
        $this->saveDB($takimlar["T13"],$takimlar["T4"],3,16);
        $this->saveDB($takimlar["T14"],$takimlar["T2"],3,16);


        $this->saveDB($takimlar["T3"],$takimlar["T2"],4,17);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],4,17);
        $this->saveDB($takimlar["T5"],$takimlar["T13"],4,17);
        $this->saveDB($takimlar["T6"],$takimlar["T12"],4,17);
        $this->saveDB($takimlar["T7"],$takimlar["T1"],4,17);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],4,17);
        $this->saveDB($takimlar["T9"],$takimlar["T14"],4,17);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],5,18);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],5,18);
        $this->saveDB($takimlar["T10"],$takimlar["T9"],5,18);
        $this->saveDB($takimlar["T11"],$takimlar["T8"],5,18);
        $this->saveDB($takimlar["T12"],$takimlar["T7"],5,18);
        $this->saveDB($takimlar["T13"],$takimlar["T6"],5,18);
        $this->saveDB($takimlar["T14"],$takimlar["T3"],5,18);



        $this->saveDB($takimlar["T4"],$takimlar["T3"],6,19);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],6,19);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],6,19);
        $this->saveDB($takimlar["T7"],$takimlar["T13"],6,19);
        $this->saveDB($takimlar["T8"],$takimlar["T12"],6,19);
        $this->saveDB($takimlar["T9"],$takimlar["T11"],6,19);
        $this->saveDB($takimlar["T10"],$takimlar["T14"],6,19);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],7,20);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],7,20);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],7,20);
        $this->saveDB($takimlar["T11"],$takimlar["T10"],7,20);
        $this->saveDB($takimlar["T12"],$takimlar["T9"],7,20);
        $this->saveDB($takimlar["T13"],$takimlar["T8"],7,20);
        $this->saveDB($takimlar["T14"],$takimlar["T4"],7,20);


        $this->saveDB($takimlar["T5"],$takimlar["T4"],8,21);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],8,21);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],8,21);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],8,21);
        $this->saveDB($takimlar["T9"],$takimlar["T13"],8,21);
        $this->saveDB($takimlar["T10"],$takimlar["T12"],8,21);
        $this->saveDB($takimlar["T11"],$takimlar["T14"],8,21);


        $this->saveDB($takimlar["T1"],$takimlar["T9"],9,22);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],9,22);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],9,22);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],9,22);
        $this->saveDB($takimlar["T12"],$takimlar["T11"],9,22);
        $this->saveDB($takimlar["T13"],$takimlar["T10"],9,22);
        $this->saveDB($takimlar["T14"],$takimlar["T5"],9,22);


        $this->saveDB($takimlar["T6"],$takimlar["T5"],10,23);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],10,23);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],10,23);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],10,23);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],10,23);
        $this->saveDB($takimlar["T11"],$takimlar["T13"],10,23);
        $this->saveDB($takimlar["T12"],$takimlar["T4"],10,23);


        $this->saveDB($takimlar["T1"],$takimlar["T11"],11,24);
        $this->saveDB($takimlar["T2"],$takimlar["T10"],11,24);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],11,24);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],11,24);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],11,24);
        $this->saveDB($takimlar["T13"],$takimlar["T12"],11,24);
        $this->saveDB($takimlar["T14"],$takimlar["T6"],11,24);


        $this->saveDB($takimlar["T7"],$takimlar["T6"],12,25);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],12,25);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],12,25);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],12,25);
        $this->saveDB($takimlar["T11"],$takimlar["T2"],12,25);
        $this->saveDB($takimlar["T12"],$takimlar["T1"],12,25);
        $this->saveDB($takimlar["T13"],$takimlar["T14"],12,25);



        $this->saveDB($takimlar["T1"],$takimlar["T13"],13,26);
        $this->saveDB($takimlar["T2"],$takimlar["T12"],13,26);
        $this->saveDB($takimlar["T3"],$takimlar["T11"],13,26);
        $this->saveDB($takimlar["T4"],$takimlar["T10"],13,26);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],13,26);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],13,26);
        $this->saveDB($takimlar["T14"],$takimlar["T7"],13,26);
    }

    private function save15($takimlar)
    {
        //hafta 1
        $this->saveDB($takimlar["T2"],$takimlar["T1"],1,16);
        $this->saveDB($takimlar["T4"],$takimlar["T15"],1,16);
        $this->saveDB($takimlar["T4"],$takimlar["T14"],1,16);
        $this->saveDB($takimlar["T5"],$takimlar["T13"],1,16);
        $this->saveDB($takimlar["T6"],$takimlar["T12"],1,16);
        $this->saveDB($takimlar["T7"],$takimlar["T11"],1,16);
        $this->saveDB($takimlar["T8"],$takimlar["T10"],1,16);


        $this->saveDB($takimlar["T1"],$takimlar["T3"],2,17);
        $this->saveDB($takimlar["T10"],$takimlar["T9"],2,17);
        $this->saveDB($takimlar["T11"],$takimlar["T8"],2,17);
        $this->saveDB($takimlar["T12"],$takimlar["T7"],2,17);
        $this->saveDB($takimlar["T13"],$takimlar["T6"],2,17);
        $this->saveDB($takimlar["T14"],$takimlar["T5"],2,17);
        $this->saveDB($takimlar["T15"],$takimlar["T4"],2,17);


        $this->saveDB($takimlar["T3"],$takimlar["T2"],3,18);
        $this->saveDB($takimlar["T4"],$takimlar["T1"],3,18);
        $this->saveDB($takimlar["T5"],$takimlar["T15"],3,18);
        $this->saveDB($takimlar["T6"],$takimlar["T14"],3,18);
        $this->saveDB($takimlar["T7"],$takimlar["T13"],3,18);
        $this->saveDB($takimlar["T8"],$takimlar["T12"],3,18);
        $this->saveDB($takimlar["T9"],$takimlar["T11"],3,18);


        $this->saveDB($takimlar["T1"],$takimlar["T5"],4,19);
        $this->saveDB($takimlar["T2"],$takimlar["T4"],4,19);
        $this->saveDB($takimlar["T11"],$takimlar["T10"],4,19);
        $this->saveDB($takimlar["T12"],$takimlar["T9"],4,19);
        $this->saveDB($takimlar["T13"],$takimlar["T8"],4,19);
        $this->saveDB($takimlar["T14"],$takimlar["T7"],4,19);
        $this->saveDB($takimlar["T15"],$takimlar["T6"],4,19);


        $this->saveDB($takimlar["T4"],$takimlar["T3"],5,20);
        $this->saveDB($takimlar["T5"],$takimlar["T2"],5,20);
        $this->saveDB($takimlar["T6"],$takimlar["T1"],5,20);
        $this->saveDB($takimlar["T7"],$takimlar["T15"],5,20);
        $this->saveDB($takimlar["T8"],$takimlar["T14"],5,20);
        $this->saveDB($takimlar["T9"],$takimlar["T13"],5,20);
        $this->saveDB($takimlar["T10"],$takimlar["T12"],5,20);



        $this->saveDB($takimlar["T1"],$takimlar["T7"],6,21);
        $this->saveDB($takimlar["T2"],$takimlar["T6"],6,21);
        $this->saveDB($takimlar["T3"],$takimlar["T5"],6,21);
        $this->saveDB($takimlar["T12"],$takimlar["T11"],6,21);
        $this->saveDB($takimlar["T13"],$takimlar["T10"],6,21);
        $this->saveDB($takimlar["T14"],$takimlar["T9"],6,21);
        $this->saveDB($takimlar["T15"],$takimlar["T8"],6,21);



        $this->saveDB($takimlar["T5"],$takimlar["T4"],7,22);
        $this->saveDB($takimlar["T6"],$takimlar["T3"],7,22);
        $this->saveDB($takimlar["T7"],$takimlar["T2"],7,22);
        $this->saveDB($takimlar["T8"],$takimlar["T1"],7,22);
        $this->saveDB($takimlar["T9"],$takimlar["T15"],7,22);
        $this->saveDB($takimlar["T10"],$takimlar["T14"],7,22);
        $this->saveDB($takimlar["T11"],$takimlar["T13"],7,22);


        $this->saveDB($takimlar["T1"],$takimlar["T9"],8,23);
        $this->saveDB($takimlar["T2"],$takimlar["T8"],8,23);
        $this->saveDB($takimlar["T3"],$takimlar["T7"],8,23);
        $this->saveDB($takimlar["T4"],$takimlar["T6"],8,23);
        $this->saveDB($takimlar["T13"],$takimlar["T12"],8,23);
        $this->saveDB($takimlar["T14"],$takimlar["T11"],8,23);
        $this->saveDB($takimlar["T15"],$takimlar["T10"],8,23);


        $this->saveDB($takimlar["T6"],$takimlar["T5"],9,24);
        $this->saveDB($takimlar["T7"],$takimlar["T4"],9,24);
        $this->saveDB($takimlar["T8"],$takimlar["T3"],9,24);
        $this->saveDB($takimlar["T9"],$takimlar["T2"],9,24);
        $this->saveDB($takimlar["T10"],$takimlar["T1"],9,24);
        $this->saveDB($takimlar["T11"],$takimlar["T15"],9,24);
        $this->saveDB($takimlar["T12"],$takimlar["T14"],9,24);


        $this->saveDB($takimlar["T1"],$takimlar["T11"],10,25);
        $this->saveDB($takimlar["T2"],$takimlar["T10"],10,25);
        $this->saveDB($takimlar["T3"],$takimlar["T9"],10,25);
        $this->saveDB($takimlar["T4"],$takimlar["T8"],10,25);
        $this->saveDB($takimlar["T5"],$takimlar["T7"],10,25);
        $this->saveDB($takimlar["T14"],$takimlar["T13"],10,25);
        $this->saveDB($takimlar["T15"],$takimlar["T12"],10,25);


        $this->saveDB($takimlar["T7"],$takimlar["T6"],11,26);
        $this->saveDB($takimlar["T8"],$takimlar["T5"],11,26);
        $this->saveDB($takimlar["T9"],$takimlar["T4"],11,26);
        $this->saveDB($takimlar["T10"],$takimlar["T3"],11,26);
        $this->saveDB($takimlar["T11"],$takimlar["T2"],11,26);
        $this->saveDB($takimlar["T12"],$takimlar["T1"],11,26);
        $this->saveDB($takimlar["T13"],$takimlar["T15"],11,26);


        $this->saveDB($takimlar["T1"],$takimlar["T13"],12,27);
        $this->saveDB($takimlar["T2"],$takimlar["T12"],12,27);
        $this->saveDB($takimlar["T3"],$takimlar["T11"],12,27);
        $this->saveDB($takimlar["T4"],$takimlar["T10"],12,27);
        $this->saveDB($takimlar["T5"],$takimlar["T9"],12,27);
        $this->saveDB($takimlar["T6"],$takimlar["T8"],12,27);
        $this->saveDB($takimlar["T15"],$takimlar["T14"],12,27);



        $this->saveDB($takimlar["T8"],$takimlar["T7"],13,28);
        $this->saveDB($takimlar["T9"],$takimlar["T6"],13,28);
        $this->saveDB($takimlar["T10"],$takimlar["T5"],13,28);
        $this->saveDB($takimlar["T11"],$takimlar["T4"],13,28);
        $this->saveDB($takimlar["T12"],$takimlar["T3"],13,28);
        $this->saveDB($takimlar["T13"],$takimlar["T2"],13,28);
        $this->saveDB($takimlar["T14"],$takimlar["T1"],13,28);


        $this->saveDB($takimlar["T1"],$takimlar["T15"],14,29);
        $this->saveDB($takimlar["T2"],$takimlar["T14"],14,29);
        $this->saveDB($takimlar["T3"],$takimlar["T14"],14,29);
        $this->saveDB($takimlar["T4"],$takimlar["T12"],14,29);
        $this->saveDB($takimlar["T5"],$takimlar["T11"],14,29);
        $this->saveDB($takimlar["T6"],$takimlar["T10"],14,29);
        $this->saveDB($takimlar["T7"],$takimlar["T9"],14,29);



        $this->saveDB($takimlar["T9"],$takimlar["T8"],15,30);
        $this->saveDB($takimlar["T10"],$takimlar["T7"],15,30);
        $this->saveDB($takimlar["T11"],$takimlar["T6"],15,30);
        $this->saveDB($takimlar["T12"],$takimlar["T5"],15,30);
        $this->saveDB($takimlar["T13"],$takimlar["T4"],15,30);
        $this->saveDB($takimlar["T14"],$takimlar["T3"],15,30);
        $this->saveDB($takimlar["T15"],$takimlar["T2"],15,30);
    }

    private function saveDB($hteam,$ateam,$hafta,$ters=0)
    {
        $tarih = date('Y-m-d H:i:s');
        $msth = $this->db->prepare("INSERT INTO maclar(tarih, hteam, ateam, hscore, ascore, playing, LIG_ID, HAFTA,SEZON) VALUES(:tarih,:hteam,:ateam,:hscore,:ascore,:playing,:lig,:hafta,:sezon)");
        $msth->bindValue(":tarih",$tarih,PDO::PARAM_STR);
        $msth->bindValue(":hteam",$hteam["ID"],PDO::PARAM_INT);
        $msth->bindValue(":ateam",$ateam["ID"],PDO::PARAM_INT);
        $msth->bindValue(":hscore",-1,PDO::PARAM_INT);
        $msth->bindValue(":ascore",-1,PDO::PARAM_INT);
        $msth->bindValue(":playing",0,PDO::PARAM_INT);
        $msth->bindValue(":lig",$this->ligid,PDO::PARAM_INT);
        $msth->bindValue(":hafta",$hafta,PDO::PARAM_INT);
        $msth->bindValue(":sezon",$this->sezon,PDO::PARAM_STR);
        $msth->execute();
        if($ters > 0){
            $msth2 = $this->db->prepare("INSERT INTO maclar(tarih, hteam, ateam, hscore, ascore, playing, LIG_ID, HAFTA,SEZON) VALUES(:tarih,:hteam,:ateam,:hscore,:ascore,:playing,:lig,:hafta,:sezon)");
            $msth2->bindValue(":tarih",$tarih,PDO::PARAM_STR);
            $msth2->bindValue(":hteam",$ateam["ID"],PDO::PARAM_INT);
            $msth2->bindValue(":ateam",$hteam["ID"],PDO::PARAM_INT);
            $msth2->bindValue(":hscore",-1,PDO::PARAM_INT);
            $msth2->bindValue(":ascore",-1,PDO::PARAM_INT);
            $msth2->bindValue(":playing",0,PDO::PARAM_INT);
            $msth2->bindValue(":lig",$this->ligid,PDO::PARAM_INT);
            $msth2->bindValue(":hafta",$ters,PDO::PARAM_INT);
            $msth2->bindValue(":sezon",$this->sezon,PDO::PARAM_STR);
            $msth2->execute();
        }
    }
}