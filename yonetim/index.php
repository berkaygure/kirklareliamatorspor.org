<?@ob_start();session_start();?>
<? include_once "../api/SendNotification.php"; ?>

<? include_once "config.php"; ?>


<?
$action =  "";
if(isset($_GET["action"])){
    $action = $_GET["action"];
}
switch($action) {
    case "dash":
        if(!isset($_SESSION["user"])){
            header("location: index.php?action=login");

        }
        include "header.php";
        ?>

        <div class="span9">
            <div class="content">
                <div class="btn-controls">
                    <div class="btn-box-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span12">
                                    <a href="index.php?action=duyurular#" class="btn-box small span4"><i class="icon-bullhorn"></i><b>Duyurular</b>
                                    </a><a href="index.php?action=ligler" class="btn-box small span4"><i class="icon-group"></i><b>Ligler</b>
                                    </a><a href="index.php?action=sayfalar" class="btn-box small span4"><i class="icon-exchange"></i><b>Sayfalar</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/#btn-controls-->
                <!--/.module-->
                <div class="row">
                    <div class="span12">
                        <h3>Lig ve Fikstür Yönetimi</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/ZJbuiD0ENR4" frameborder="0" allowfullscreen></iframe>
                        <h3>Doküman Bankası Yönetimi</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Y6gdjcd-Zuo" frameborder="0" allowfullscreen></iframe>
                        <h3>Duyuru Yönetimi</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/ERrjtK-kw4c" frameborder="0" allowfullscreen></iframe>
                        <h3>Disiplin ve Tertip Kurulu Kararı Yönetimi</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/sWnrE7ggBWY" frameborder="0" allowfullscreen></iframe>
                        <h3>Sarı ve Kırmızı Kart Cezalıları Düzenleme</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/TCqF5qfCHlQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <!--/.content-->
        </div>
        <!--/.span9-->

        <?
        include "footer.php";

        break;
    case "deleteGaleri":
        if($_POST){
            unlink("../files/resimler/" . $_POST["path"]. "/" .$_POST["id"]);
        }
        break;
    case "uploadGaleri":
        include_once "php/verot/class.upload.php";
        $tarih = date('Y-m-d H:i:s');

        $handle = new upload($_FILES['file']);
        if ($handle->uploaded) {
            $handle->file_new_name_body = seo($tarih);
            $handle->image_resize = true;
            $handle->image_x = 500;
            $handle->image_y = 300;
            $handle->image_ratio_y = true;
            $handle->process('../files/resimler/' . $_GET["id"] . "/");
            if ($handle->processed) {
                $handle->clean();
            } else {
                echo 'error : ' . $handle->error;
            }
        }
        break;
    case "dGaleri":
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Resimler</h3>
                    </div>
                    <div class="module-body">
                        <?
                        $path = "../files/resimler/".$_GET["id"] . "/";
                        $dokumanlar = scandir($path);
                        if(count($dokumanlar)-2 >0) { ?>
                            <div class="row">
                            <?
                            foreach ($dokumanlar as $doc) {
                                if (strlen($doc) > 3) { ?>
                                    <div class="span2" style="margin-bottom: 5px;">
                                        <img style="width: 100%;height: 140px;" src="../files/resimler/<?= $_GET["id"] ?>/<?=$doc?>">
                                        <button data-path="<?=$_GET["id"]?>" data-id="<?=$doc?>" class="remove btn btn-block btn-danger">Sil</button>
                                    </div>
                                <?
                                }
                            }

                            ?>
                            </div>

                                <?
                        }
                        ?>
                        <form style="margin-top: 10px;" id="dz" action="index.php?action=uploadGaleri&id=<?=$_GET["id"]?>" class="dropzone">
                            <div class="dropzone-previews"></div>
                            <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                <input name="file" type="file" multiple />
                            </div>

                        </form>

                        <!-- <div id="fileManager"></div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "dosya":
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Dosya Yöneticisi</h3>
                    </div>
                    <div class="module-body">
                        <iframe  width="100%" height="650" frameborder="0"
                                 src="filemanager/dialog.php?type=0&fldr=dokumanlar">
                        </iframe>
                       <!-- <div id="fileManager"></div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "musabaka":
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Müsabaka Programı</h3>
                    </div>
                    <div class="module-body">
                        <iframe  width="100%" height="650" frameborder="0" src="filemanager/dialog.php?type=0&fldr=haftalikmusabaka">
                        </iframe>
                        <!-- <div id="fileManager"></div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "statu":
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Statüler</h3>
                    </div>
                    <div class="module-body">
                        <iframe  width="100%" height="650" frameborder="0" src="filemanager/dialog.php?type=0&fldr=statuler">
                        </iframe>
                        <!-- <div id="fileManager"></div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "hakem":
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Hakem ve Gözlemci Listesi</h3>
                    </div>
                    <div class="module-body">
                        <iframe  width="100%" height="650" frameborder="0" src="filemanager/dialog.php?type=0&fldr=hakem_ve_gozlemci">
                        </iframe>
                        <!-- <div id="fileManager"></div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "ligayar":
        $ligData = $database->query("SELECT * FROM leagues WHERE ID=" . $_GET["id"])->fetchAll(PDO::FETCH_ASSOC);
        if($_POST){
            $ligadi = trim($_POST["ligadi"]);
            $ikili = $_POST["ikili"];
            if(strlen($ligadi) > 4) {
                /** @var PDO $database */
                $up = $database->prepare("UPDATE leagues SET league_name= :lname, ikili_averaj= :ikili WHERE ID=:id");
                $up->bindValue(":id", $_GET["id"], PDO::PARAM_STR);
                $up->bindValue(":lname", $ligadi, PDO::PARAM_STR);
                $up->bindValue(":ikili", $ikili, PDO::PARAM_BOOL);
                $up->execute();
                $message = array("TYPE"=>"SUCCES","TEXT"=>"Başarıyla Kaydedildi.");
            }else{
                $message = array("TYPE"=>"TYPE","TEXT"=>"Başarıyla Kaydedildi.");

            }

        }else{
            $ligadi = $ligData[0]["league_name"];
            $ikili = $ligData[0]["ikili_averaj"];
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Ayarlar: <?=$ligData[0]["league_name"]?></h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="SUCCES") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=ligayar&id=<?=$_GET["id"]?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Lig Adi</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=$ligadi?>" name="ligadi" placeholder="Lig Adı" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">İkili Averaj?</label>
                                <div class="controls">
                                    <select name="ikili" class="form-control" id="">
                                        <option <?=(!$ikili) ? 'selected' : ''?> value="0">Kapalı</option>
                                        <option <?=($ikili) ? 'selected' : ''?> value="1">Açık</option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;

    case "takimlar":
        $sth = $database->prepare("SELECT * FROM leagues WHERE ID=:id");
        $sth->bindValue(":id", $_GET["id"]);
        $sth->execute();
        $ligData = $sth->fetchAll(PDO::FETCH_ASSOC);
        if($_POST)
        {
            $takimlar = array();
            for($i=0; $i < $ligData[0]["team_count"]; $i++){
                $takimlar["T" . ($i+1)]= array(
                    "id" => $_POST["id" . $i],
                    "ADI" => $_POST["takim" . $i],
                    "CEZA" => $_POST["ceza" . $i],
                    "NOTLAR" => $_POST["notlar" . $i],
                    "WEBSITE" => $_POST["website" . $i]
                );
                $tsth = $database->prepare("UPDATE takimlar SET tname=:tname,ceza=:ceza,takim_notlari=:notlar,website=:site,seo=:seo WHERE id=:id");
                $tsth->bindValue(":tname", $takimlar["T" . ($i+1)]["ADI"],PDO::PARAM_STR);
                $tsth->bindValue(":ceza", $takimlar["T" . ($i+1)]["CEZA"],PDO::PARAM_INT);
                $tsth->bindValue(":notlar", $takimlar["T" . ($i+1)]["NOTLAR"],PDO::PARAM_STR);
                $tsth->bindValue(":site", $takimlar["T" . ($i+1)]["WEBSITE"],PDO::PARAM_STR);
                $tsth->bindValue(":seo", seo($takimlar["T" . ($i+1)]["ADI"]),PDO::PARAM_STR);
                $tsth->bindValue(":id",$takimlar["T" . ($i+1)]["id"] ,PDO::PARAM_INT);
                $tsth->execute();

                unset($tsth);
            }


        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Takımlar : <?=$ligData[0]["leagune_name"]?></h3>
                    </div>
                    <div class="module-body">

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=takimlar&id=<?=$_GET["id"]?>">
                            <table class="table table-bordered">
                                <tr>
                                    <th>#</th>
                                    <th>Takım Adı</th>
                                    <th>Cezalar</th>
                                    <th>Takım notları</th>
                                    <th>Web Sitesi</th>
                                    <th>Takımı Çek</th>
                                </tr>
                                <?
                                $data = $database->query("SELECT * FROM takimlar WHERE lig_id=" . $ligData[0]["ID"] ." ORDER by id")->fetchAll(PDO::FETCH_ASSOC);
                                for($i = 0 ; $i < count($data); $i++){
                                    ?>
                                    <tr>
                                        <td><b><?=$i+1?>.</b> <input class="span12" type="hidden" value="<?=$data[$i]["id"]?>" name="id<?=$i?>"></td>
                                        <td ><input class="span12" type="text" value="<?=$data[$i]["tname"]?>" placeholder="Takım Adı" name="takim<?=$i?>"></td>
                                        <td ><input class="span12" type="text"value="<?=$data[$i]["ceza"]?>" placeholder="Ceza" name="ceza<?=$i?>"></td>
                                        <td><input class="span12" type="text" value="<?=$data[$i]["takim_notlari"]?>" placeholder="Notlar" name="notlar<?=$i?>"></td>
                                        <td ><input  class="span12" type="text" value="<?=$data[$i]["website"]?>" placeholder="Web Sitesi" name="website<?=$i?>"></td>
                                        <td><a href="index.php?action=takimcek&id=<?=$data[$i]["id"]?>" class="btn btn-primary">Takımı Çek</a></td>
                                    </tr>
                                <?}?>
                            </table>
                            <br>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="button" onclick="javascript:history.back()" class="btn">Geri</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "puan":
        $sth = $database->prepare("SELECT * FROM leagues WHERE ID=:id");
        $sth->bindValue(":id", $_GET["id"]);
        $sth->execute();
        $ligData = $sth->fetchAll(PDO::FETCH_ASSOC);
        $sh = $database->prepare(
            "SELECT
                  tname AS Team, Sum(P) AS P,Sum(W) AS W,Sum(D) AS D,Sum(L) AS L,
                  SUM(F) as F,SUM(A) AS A,SUM(GD) AS GD,SUM(Pts) AS Pts
                FROM(
                    SELECT
                    hteam Team, playing,
                    IF(hscore > -1,1,0) P,
                    IF(hscore > -1 and hscore > ascore,1,0) W,
                    IF(hscore > -1 and hscore = ascore,1,0) D,
                    IF(hscore > -1 and hscore < ascore,1,0) L,
                    IF(hscore > -1,hscore,0) F,
                    IF(hscore > -1,ascore,0) A,
                    IF(hscore > -1,hscore-ascore,0) GD,
                    CASE WHEN hscore > -1 and hscore > ascore THEN 3 WHEN hscore > -1 and hscore = ascore THEN 1 ELSE 0 END PTS
                  FROM maclar
                  UNION ALL
                  SELECT
                    ateam,playing,
                    IF(ascore > -1,1,0) P,
                    IF(ascore > -1 and hscore < ascore,1,0) W,
                    IF(ascore > -1 and hscore = ascore,1,0) D,
                    IF(ascore > -1  and hscore > ascore,1,0) L,
                    IF(ascore > -1 ,ascore,0) F,
                    IF(ascore > -1,hscore,0) A,
                    IF(ascore > -1,ascore-hscore,0) GD,
                    CASE WHEN ascore > -1 and hscore < ascore THEN 3 WHEN ascore > -1 and hscore = ascore THEN 1 ELSE 0 END
                  FROM maclar
                ) as tot
                JOIN takimlar t ON tot.Team=t.id
                WHERE LIG_ID = :id
                GROUP BY Team
                ORDER BY SUM(Pts) DESC,SUM(GD) DESC, Team ASC
            ");

        $sh->bindValue(":id", $_GET["id"]);
        $sh->execute();
        $getLig = $sh->fetchAll(PDO::FETCH_ASSOC);
        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Puan Tablosu : <?=$ligData[0]["league_name"]?></h3>
                    </div>
                    <div class="module-body">

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hovered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Sıra
                                </th>
                                <th>
                                    Takım
                                </th>
                                <th>
                                    O
                                </th>
                                <th>
                                    G
                                </th>
                                <th>
                                    B
                                </th>
                                <th>
                                    M
                                </th>
                                <th>
                                    A
                                </th>
                                <th>
                                    Y
                                </th>
                                <th>
                                    AV
                                </th>
                                <th>
                                    PU
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            for($i=0; $i < count($getLig); $i++){
                                echo '<tr>';
                                echo '<td>'.($i+1).'</td>';
                                echo '<td>'.$getLig[$i]["Team"].'</td>';
                                echo '<td>'.$getLig[$i]["P"].'</td>';
                                echo '<td>'.$getLig[$i]["W"].'</td>';
                                echo '<td>'.$getLig[$i]["D"].'</td>';
                                echo'<td>'.$getLig[$i]["L"].'</td>';
                                echo '<td>'.$getLig[$i]["F"].'</td>';
                                echo '<td>'.$getLig[$i]["A"].'</td>';
                                echo '<td>'.$getLig[$i]["GD"].'</td>';
                                echo '<td>'.$getLig[$i]["Pts"].'</td>';
                                echo '</tr>';

                            }
                            ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php"; ?>
        <?

        break;

    case "ligsil":
        $ligid = $_GET["id"];
        $database->query("DELETE FROM leagues WHERE ID=" . $ligid);
        $database->query("DELETE FROM maclar WHERE LIG_ID=" . $ligid);
        $database->query("DELETE FROM takimlar WHERE lig_id=" . $ligid);
        header("location: index.php?action=ligler");
        break;
    case "ligler":
        $sezon = $config["sezon"];
        if(isset($_GET["sezon"])){
            $sezon = $_GET["sezon"];
        }
        $sth = $database->prepare("SELECT * FROM leagues WHERE sezon='".$sezon."' ORDER BY ID DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3 class="pull-left">Ligler</h3>

                        <form class="pull-right" method="get" id="form">
                            <input type="hidden" value="ligler" name="action">
                            <select class="control" name="sezon" id="sezon" onchange="document.getElementById('form').submit();">
                                <option <?=$sezon=="19/20"?"selected":""?> value="19/20">2019/2020</option>
				<option <?=$sezon=="18/19"?"selected":""?> value="18/19">2018/2019</option>
				<option <?=$sezon=="17/18"?"selected":""?> value="17/18">2017/2018</option>
                                <option <?=$sezon=="16/17"?"selected":""?>  value="16/17">2016/2017</option> 
			   </select>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="module-body table">


                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Lig Adı
                                </th>
                                <th>
                                    Takım Sayısı
                                </th>
                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <a href="index.php?action=fikstur&id=<?=$d["ID"]?>&hafta=<?=($d["AKTIF_HAFTA"]=="")?1:$d["AKTIF_HAFTA"]?>"><?=$d["league_name"]?></a>
                                    </td>
                                    <td class="center">
                                        <?=$d["team_count"]?>
                                    </td>
                                    <td class="align-right"  >
                                        <a href="index.php?action=fikstur&id=<?=$d["ID"]?>&hafta=<?=($d["AKTIF_HAFTA"]=="")?1:$d["AKTIF_HAFTA"]?>" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Fikstür</a>
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                                                Daha Fazla
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" style="z-index: 100000;">
                                                <li><a tabindex="-1" href="index.php?action=puan&id=<?=$d["ID"]?>">Puan Durumu</a></li>
                                                <li><a tabindex="-1"href="index.php?action=takimlar&id=<?=$d["ID"]?>">Takımlar</a></li>
                                                <li><a tabindex="-1" href="index.php?action=fyazdir&id=<?=$d["ID"]?>">Geçerli Haftayı Yazdır</a></li>
                                                <li><a tabindex="-1" target="_blank" href="http://kirklareliamatorspor.org/print.php?action=fixtureAll&lig=<?=$d["ID"]?>">Tüm Fikstürü Yazdır</a></li>
                                            </ul>
                                        </div>
                                        <a href="index.php?action=ligayar&id=<?=$d["ID"]?>" class="btn btn-mini btn-warning"><i class="icon-cogs"></i> Ayarlar</a>

                                        <a onclick="$.ligSil('<?=$d["ID"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>

                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;
    case "fikstur":

        $ligid = $_GET["id"];
        $hafta = $_GET["hafta"];
        $ligBilgiler  = $database->query("SELECT * FROM leagues WHERE ID=" . $ligid)->fetchAll(PDO::FETCH_ASSOC);
        if($_POST){
            $database->query("UPDATE leagues SET AKTIF_HAFTA=" . $_POST["seciliHafta"] ." WHERE ID=" . $ligid);
            $ligBilgiler[0]["AKTIF_HAFTA"]= $_POST["seciliHafta"];

            for($i=0; $i < $ligBilgiler[0]["match_count"]; $i++) {
                try {
                    $database->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                    $up = $database->prepare("UPDATE maclar SET tarih=:tarih, hteam=:hteam, ateam=:ateam, hscore=:hscore, ascore=:ascore, STAD=:stad, ACIKLAMA=:aciklama WHERE id=:id");
                    $up->bindValue(":id", $_POST["mac" . $i], PDO::PARAM_INT);
                    if(strlen(trim($_POST["tarih" . $i])) < 3){
                        $up->bindValue(":tarih", "NULL", PDO::PARAM_STR);

                    }else{
                        $up->bindValue(":tarih", mysqlTarih($_POST["tarih" . $i]), PDO::PARAM_STR);

                    }
                    $up->bindValue(":hteam", $_POST["hteam" . $i], PDO::PARAM_INT);
                    $up->bindValue(":ateam", $_POST["ateam" . $i], PDO::PARAM_INT);
                    $up->bindValue(":hscore", ($_POST["hscore" . $i]=="-")?"-1":$_POST["hscore" . $i], PDO::PARAM_INT);
                    $up->bindValue(":ascore", ($_POST["ascore" . $i]=="-")?"-1":$_POST["ascore" . $i], PDO::PARAM_INT);
                    $up->bindValue(":stad", (($_POST["stad" . $i])==""?"YOK":$_POST["stad" . $i]), PDO::PARAM_STR);
                    $up->bindValue(":aciklama", (($_POST["aciklama" . $i])==""?"YOK":$_POST["aciklama" . $i]), PDO::PARAM_STR);
                    $up->execute();
                    $message = array("TEXT"=>"Kaydetme Başarılı","TYPE"=>"SUCCESS");
                } catch (PDOException $ex) {
                    $message = array("TEXT"=>$ex->getMessage(),"TYPE"=>"ERROR");
                }
                unset($up);
            }
           // sendAll($database, "Fikstür bilgisi eklendi ",$ligBilgiler[0]["league_name"] . "-" . $hafta . ". hafta");

        }
        $datalar = $database->query("SELECT * FROM maclar WHERE LIG_ID=" . $ligid . " and HAFTA=" . $hafta . " ORDER BY ID")->fetchAll(PDO::FETCH_ASSOC);

        include_once "header.php";

        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Fikstür Girin: <?=$config["sezon"]?> Sezonu <?=$ligBilgiler[0]["league_name"]?> <?=$hafta?>. Hafta Maçları</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>
                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="SUCCESS") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>
                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=fikstur&id=<?=$ligid?>&hafta=<?=$hafta?>">
                            <a target="_blank" href="fikstur.php?lig=<?=$ligid?>&hafta=<?=$hafta?>" class="btn btn-primary pull-right" style="margin-bottom:5px;"><i class="glyphicon glyphicon-print"></i> Yazdır</a>
                            <select name="hafta" data-lig="<?=$ligid?>" class="span12" id="haftaDegistir">
                                <?
                                for($i=1; $i <= $ligBilgiler[0]["week_count"]; $i++){?>
                                    <option  <?=($i==$hafta)?"selected":""?> value="<?=$i?>"><?=$i?>. hafta</option>

                                <?}?>
                            </select>
                            <div class="clearfix"></div>
                            <table style="margin-top: 6px;" class="table table-bordered">
                                <tr>
                                    <th style="text-align: center">Zaman</th>
                                    <th style="text-align: center">Ev Sahibi</th>
                                    <th style="text-align: center">Deplasman</th>
                                    <th style="text-align: center">Skor</th>
                                    <th style="text-align: center">Ex</th>
                                    <th style="text-align: center">Özel Karar</th>
                                    <th style="text-align: center">Saha</th>
                                    <th style="text-align: center">Rapor</th>
                                </tr>
                                <?
                                $mSayi = -1;
                                foreach ($datalar as $data) {
                                    $mSayi++;
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="mac<?=$mSayi?>" value="<?=$data["id"]?>"/>
                                            <input type="text"  name="tarih<?=$mSayi?>" value="<?=($data["tarih"] == "0000-00-00 00:00:00")?"":turkcetarih('j.M.Y H.i',$data["tarih"])?>" class="span12 datetimepicker">
                                        </td>
                                        <td>
                                            <select name="hteam<?=$mSayi?>" class="span12">
                                                <option <?=($data["hteam"]==0)?"selected":""?> value="0">-</option>
                                                <?
                                                $takimlar = $database->query("SELECT * FROM takimlar WHERE lig_id=" .$ligid)->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($takimlar as $takim) { ?>
                                                    <option <?=($takim["id"]==$data["hteam"])?"selected":""?>  value="<?=$takim["id"]?>"><?=$takim["tname"]?></option>
                                                <?}

                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name="ateam<?=$mSayi?>" class="span12">
                                                <option <?=($data["ateam"]==0)?"selected":""?> value="0">-</option>
                                                <?
                                                foreach ($takimlar as $takim) { ?>
                                                    <option <?=($takim["id"]==$data["ateam"])?"selected":""?>  value="<?=$takim["id"]?>"><?=$takim["tname"]?></option>
                                                <?}

                                                ?>
                                            </select>

                                        </td>
                                        <td width="75">
                                            <input type="text"  name="hscore<?=$mSayi?>" value="<?=($data["hscore"]=="-1"?"-":$data["hscore"])?>" class="span5">-<input type="text"   name="ascore<?=$mSayi?>" value="<?=($data["ascore"]=="-1"?"-":$data["ascore"])?>"  class="span5">
                                        </td>
                                        <td>
                                            <select  name="ex<?=$mSayi?>" class="span12">

                                                <option value="0" selected>-</option>
                                                <option>(Uzt.)</option>
                                                <option>(Pen.)</option>
                                            </select>

                                        </td>
                                        <td>

                                            <select  name="ozel<?=$mSayi?>" class="span12">
                                                <option selected>-</option>
                                                <option>Ev Sahibi Kazanır</option>
                                                <option>Deplasman Kazanı</option>
                                                <option>İki Taraflı Sonuç</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input  name="stad<?=$mSayi?>" value="<?=$data["STAD"]?>" placeholder="Stad" type="text" class="span12">
                                        </td>
                                        <td>
                                            <input  name="aciklama<?=$mSayi?>" placeholder="Rapor" value="<?=$data["ACIKLAMA"]?>" type="text" class="span12">

                                        </td>
                                    </tr>
                                <?
                                }

                                ?>
                            </table>
                            <br>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Şuanki Hafta</label>
                                <div class="controls">
                                    <select name="seciliHafta" class="span12">
                                        <?
                                        for($i=1; $i <= $ligBilgiler[0]["week_count"]; $i++){
                                            if($i==$hafta){?>
                                                <option  <?=($ligBilgiler[0]["AKTIF_HAFTA"])==$hafta?"selected":""?> value="<?=$hafta?>">Seçili hafta (<?=$i?>. hafta)</option>

                                            <?
                                            }else{
                                                ?>
                                                <option <?=($ligBilgiler[0]["AKTIF_HAFTA"])==$i?"selected":""?> value="<?=$i?>"><?=$i?></option>
                                            <?
                                            }

                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="button" onclick="javascript:history.back()" class="btn">Geri</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "ligyarat":
        include_once "header.php";

        if(!isset($_GET["step"])){


            ?>
            <div class="span9">
                <div class="content">
                    <div class="module">
                        <div class="module-head">
                            <h3>Lig Yarat [1/4]</h3>
                        </div>
                        <div class="module-body">

                            <form class="form-horizontal row-fluid" method="post" action="index.php?action=ligyarat&step=1">
                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Lig Adı</label>
                                    <div class="controls">
                                        <input type="text" id="basicinput" name="ligadi" placeholder="Lig Adı" class="span8">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Takım Sayısı</label>
                                    <div class="controls">
                                        <input type="number" id="basicinput" name="takimsayisi" placeholder="Takım Sayısı" class="span8">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Hafta Sayısı</label>
                                    <div class="controls">
                                        <input type="number" id="basicinput" name="haftasayisi" placeholder="Hafta Sayısı" class="span8">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Haftada Oynanacak Maç Sayısı</label>
                                    <div class="controls">
                                        <input type="number" id="basicinput" name="macsayisi" placeholder="Haftada Oynanacak Maç Sayısı" class="span8">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Bilgilendirme</label>
                                    <div class="controls">
                                        *** 4 Takım İçin -> Hafta Sayısı: 6 *** Haftalık Maç Sayısı: 2<hr>
                                        *** 5 Takım İçin -> Hafta Sayısı: 10 *** Haftalık Maç Sayısı: 2<hr>
                                        *** 6 Takım İçin -> Hafta Sayısı: 10 *** Haftalık Maç Sayısı: 3<hr>
                                        *** 7 Takım İçin -> Hafta Sayısı: 14 *** Haftalık Maç Sayısı: 3<hr>
                                        *** 8 Takım İçin -> Hafta Sayısı: 14 *** Haftalık Maç Sayısı: 4<hr>
                                        *** 9 Takım İçin -> Hafta Sayısı: 18 *** Haftalık Maç Sayısı: 4<hr>
                                        *** 10 Takım İçin -> Hafta Sayısı: 18 *** Haftalık Maç Sayısı: 5<hr>
                                        *** 11 Takım İçin -> Hafta Sayısı: 22 *** Haftalık Maç Sayısı: 5<hr>
                                        *** 12 Takım İçin -> Hafta Sayısı: 22 *** Haftalık Maç Sayısı: 6<hr>
                                        *** 13 Takım İçin -> Hafta Sayısı: 26 *** Haftalık Maç Sayısı: 6<hr>
                                        *** 14 Takım İçin -> Hafta Sayısı: 26 *** Haftalık Maç Sayısı: 7<hr>
                                    </div>
                                </div>



                                <div class="control-group">
                                    <div class="controls">
                                        <button type="submit" class="btn btn-primary">Kaydet</button>
                                        <button type="reset" class="btn">Temizle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?

        }else{

            switch($_GET["step"]){
                case "1":
                    if($_POST){
                        $data = array();
                        $data["ligadi"] = $_POST["ligadi"];
                        $data["takimsayisi"] = $_POST["takimsayisi"];
                        $data["haftasayisi"] = $_POST["haftasayisi"];
                        $data["macsayisi"] = $_POST["macsayisi"];
                    }
                    ?>
                    <div class="span9">
                        <div class="content">
                            <div class="module">
                                <div class="module-head">
                                    <h3>Lig Yarat [2/4]</h3>
                                </div>
                                <div class="module-body">

                                    <form class="form-horizontal row-fluid" method="post" action="index.php?action=ligyarat&step=2">
                                        <div class="control-group">
                                            <label class="control-label" for="basicinput">Fikstür</label>
                                            <div class="controls">
                                                <label class="radio inline">
                                                    <input type="radio" name="fikstur" value="0" id="fikstur1"> Hazır Fikstür Yok
                                                </label>
                                                <input type="hidden" value="<?=base64_encode(serialize($data))?>" name="data"/>
                                                <?
                                                if($data["takimsayisi"]>3 && $data["takimsayisi"] <= 15){ ?>

                                                    <label class="radio inline">
                                                        <input type="radio" name="fikstur" value="<?=$data["takimsayisi"]?>" id="fikstur2"> <b><?=$data["takimsayisi"]?> Takımlı Fikstür</b>
                                                    </label>
                                                <?
                                                }
                                                ?>
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <div class="controls">
                                                <button type="submit" class="btn btn-primary">Kaydet</button>
                                                <button type="button" onclick="javascript:history.back()" class="btn">Geri</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    break;

                case "2":
                    if($_POST){
                        $data = unserialize(base64_decode($_POST["data"]));
                        $data["fikstur"] = $_POST["fikstur"];
                    }
                    ?>
                    <div class="span9">
                        <div class="content">
                            <div class="module">
                                <div class="module-head">
                                    <h3>Lig Yarat - Takımları Girin [3/4]</h3>
                                </div>
                                <div class="module-body">

                                    <form class="form-horizontal row-fluid" method="post" action="index.php?action=ligyarat&step=3">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Takım Adı</th>
                                                <th>Cezalar</th>
                                                <th>Takım notları</th>
                                                <th>Web Sitesi</th>
                                            </tr>
                                            <?
                                            for($i = 0 ; $i < $data["takimsayisi"]; $i++){
                                                ?>
                                                <tr>
                                                    <td><b><?=$i+1?>.</b></td>
                                                    <td ><input class="span12" type="text" value="" placeholder="Takım Adı" name="takim<?=$i?>"></td>
                                                    <td ><input class="span12" type="text" value="" placeholder="Ceza" name="ceza<?=$i?>"></td>
                                                    <td><input class="span12" type="text" value="" placeholder="Notlar" name="notlar<?=$i?>"></td>
                                                    <td ><input  class="span12" type="text" value="" placeholder="Web Sitesi" name="website<?=$i?>"></td>
                                                </tr>
                                            <?}?>
                                        </table>
                                        <br>
                                        <div class="control-group">
                                            <div class="controls">
                                                <input type="hidden" value="<?=base64_encode(serialize($data))?>" name="data"/>
                                                <button type="submit" class="btn btn-primary">Kaydet</button>
                                                <button type="button" onclick="javascript:history.back()" class="btn">Geri</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    break;



                case "3":
                    if($_POST){
                        $data = unserialize(base64_decode($_POST["data"]));

                        // ligi kaydet
                        // ligi kaydet

                        $sth = $database->prepare("INSERT INTO leagues(league_name, week_count, team_count, match_count, seo,sezon) VALUES(:lig,:hafta,:takim,:mac,:seo,:sezon)");
                        $sth->bindValue(":lig", $data["ligadi"],PDO::PARAM_STR);
                        $sth->bindValue(":hafta", $data["haftasayisi"],PDO::PARAM_INT);
                        $sth->bindValue(":takim", $data["takimsayisi"],PDO::PARAM_INT);
                        $sth->bindValue(":mac", $data["macsayisi"],PDO::PARAM_INT);
                        $sth->bindValue(":seo", seo($data["ligadi"]),PDO::PARAM_STR);
                        $sth->bindValue(":sezon", $config["sezon"],PDO::PARAM_STR);
                        $sth->execute();
                        $lig_id = $database->lastInsertId();

                        // lig kaydedildi
                        $takimlar = array();

                        for($i=0; $i < $data["takimsayisi"]; $i++){
                            $takimlar["T" . ($i+1)]= array(
                                "ADI" => $_POST["takim" . $i],
                                "CEZA" => ($_POST["ceza" . $i] ?: 0),
                                "NOTLAR" => ($_POST["notlar" . $i] ?: " "),
                                "WEBSITE" => ($_POST["website" . $i] ?: " ")
                            );
                            $tsth = $database->prepare("INSERT INTO takimlar(tname,ceza,takim_notlari,website,seo,lig_id) VALUES(:tname,:ceza,:notlar,:sitem,:seo,:ligid)");
                            $tsth->bindValue(":tname", $takimlar["T" . ($i+1)]["ADI"],PDO::PARAM_STR);
                            $tsth->bindValue(":ceza", $takimlar["T" . ($i+1)]["CEZA"],PDO::PARAM_INT);
                            $tsth->bindValue(":notlar", $takimlar["T" . ($i+1)]["NOTLAR"],PDO::PARAM_STR);
                            $tsth->bindValue(":sitem", $takimlar["T" . ($i+1)]["WEBSITE"],PDO::PARAM_STR);
                            $tsth->bindValue(":seo", seo($takimlar["T" . ($i+1)]["ADI"]),PDO::PARAM_STR);
                            $tsth->bindValue(":ligid",$lig_id ,PDO::PARAM_INT);
                            $tsth->execute();
                            $takimlar["T" . ($i+1)]["ID"] = $database->lastInsertId();
                            unset($tsth);
                        }

                        include_once "php/fikstur.php";
                        $saver =  new fikstur($data["fikstur"], $database);
                        $saver->kaydet($takimlar,$lig_id,$config["sezon"],$data["haftasayisi"]);
                    }
                    ?>
                    <div class="span9">
                        <div class="content">
                            <div class="module">
                                <div class="module-head">
                                    <h3>Lig Yarat  [4/4]</h3>
                                </div>
                                <div class="module-body">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        Lig Başaraylıa Yaratılmıştır. Şimdi Fikstürleri Girebilrisiniz. Aşağıdaki Butondan devam edin.
                                    </div>
                                    <div style="text-align: center">
                                        <a href="/yonetim/index.php?action=fikstur&id=<?php echo $lig_id;?>&hafta=1" class="btn btn-primary"> Fikstürü Göster </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    break;
            }

        }



        include_once "footer.php";
        break;


    case "ayarkaydet":
        if($_POST){
            $eskisifre = trim($_POST["eski"]);
            $sifre = trim($_POST["sifre"]);
            $sifretekrar = trim($_POST["sifretekrar"]);

            try {
                if(strlen($eskisifre) > 6 && strlen($sifre) > 6) {
                    $query = $database->query("SELECT * FROM kullanici WHERE k_sifre='$eskisifre'", PDO::FETCH_ASSOC);
                    if ($say = $query->rowCount()) {
                        if ($say > 0) {
                            if($eskisifre==$sifre){
                                $sth = $database->prepare("UPDATE kullanici SET k_sifre=:sifre WHERE id=:id");
                                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                                $sth->bindValue(":sifre", $sifre, PDO::PARAM_STR);
                                $sth->execute();
                                header("location: index.php?action=ayarkaydet&msg=ok");
                            }else{
                                $message = array("TYPE"=>"ERROR", "TITLE"=>"Şifre DEğiştirlemedi", "TEXT"=>"Şifre DEğiştirlemedi. Şifreleriniz Uyuşmuyor.");

                            }
                        }else{
                            $message = array("TYPE"=>"ERROR", "TITLE"=>"Şifre DEğiştirlemedi", "TEXT"=>"Eski Şifreniz Yanlış");

                        }
                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Şifre DEğiştirlemedi", "TEXT"=>"Muhtemelen Eski Şifreniz Yanlış");

                    }



                }else{
                    $message = array("TYPE"=>"ERROR", "TITLE"=>"Şifre DEğiştirlemedi", "TEXT"=>"Şifre DEğiştirlemedi. Şifreniz 6 Karaketerden Uzun olmalı");
                }
            }catch(PDOException $ex){
                die($ex->getMessage());
            }

        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Ayarlar</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if($_GET["msg"]=="ok"){
                            ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                Şifreniz Güncellenmiştir. Artık Yeni Şifreniz İle Giriş Yapabilirsiniz.
                            </div>
                        <?
                        }
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=ayarkaydet">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Eski Şifre</label>
                                <div class="controls">
                                    <input type="password" id="basicinput" name="eski" placeholder="Eski Şifreniz" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Yeni Şifre</label>
                                <div class="controls">
                                    <input type="password" id="basicinput"  name="sifre" placeholder="Yeni Şifreniz" class="span8">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Şifre Tekrarı</label>
                                <div class="controls">
                                    <input type="password" id="basicinput" placeholder="Şifrenizin Tekrarı" name="sifretekrar" class="span8">
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;


    case "menusil":
        if($_GET["id"]!=""){
            $database->query("DELETE FROM menuler WHERE id=" . $_GET["id"]);
            $database->query("UPDATE menuler SET ustid=0 WHERE id=" . $_GET["id"]);
            header("location: index.php?action=menuler&msg=del");
        }
        break;
    case "menukaydet":
        if($_POST){
            $baslik = addslashes(strip_tags(trim($_POST["baslik"])));
            $link = $_POST["link"];
            $sayfa = $_POST["sayfa"];
            $ust = $_POST["ust"];
            $sira = $_POST["sira"];
            if($_GET["id"]==""){
                try {
                    if($baslik != "") {
                        $sth = $database->prepare("INSERT INTO menuler(baslik,link,sayfa,ustid,sira) VALUES(:baslik,:link,:sayfa,:ust,:sira)");
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":link", $link, PDO::PARAM_STR);
                        $sth->bindValue(":sayfa", $sayfa, PDO::PARAM_INT);
                        $sth->bindValue(":ust", $ust, PDO::PARAM_INT);
                        $sth->bindValue(":sira", $sira, PDO::PARAM_INT);
                        $sth->execute();
                        header("location: index.php?action=menuler&msg=ok");

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Menü Eklenemedi", "TEXT"=>"Menü Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($baslik != "") {
                        $sth = $database->prepare("UPDATE menuler SET baslik=:baslik,link=:link,sayfa=:sayfa,ustid=:ust,sira=:sira WHERE id=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":link", $link, PDO::PARAM_STR);
                        $sth->bindValue(":sayfa", $sayfa, PDO::PARAM_INT);
                        $sth->bindValue(":ust", $ust, PDO::PARAM_INT);
                        $sth->bindValue(":sira", $sira, PDO::PARAM_INT);

                        $sth->execute();
                        header("location: index.php?action=menuler&msg=ok");

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Menü Eklenemedi", "TEXT"=>"Menü Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM menuler WHERE id=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];
                $baslik = stripslashes($data->baslik);
                $link = $data->link;
                $sayfa = $data->sayfa;
                $ust = $data->ustid;
                $sira = $data->sira;
            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Menü Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=menukaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Başlık</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baslik)?>" name="baslik" placeholder="Başlık" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Link</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($link)?>" name="link" placeholder="Link" class="span8">
                                    <span class="help-inline">Eğer Sayfa Seçecekseniz Boş geçiniz</span>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="basicinput">Sayfa</label>
                                <div class="controls">
                                    <select name="sayfa" class="span8">
                                        <option value="0">Yok</option>
                                        <?
                                        $sayfalar = $database->query("SELECT * FROM sayfalar")->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($sayfalar as $s) {?>
                                            <option <?=($s["id"]==$sayfa)?"selected":""?> value="<?=$s["id"]?>"><?=$s["baslik"]?></option>
                                        <?}?>
                                    </select>
                                    <span class="help-inline">Eğer Link Girecekseniz Dikkate Alınmayacak.</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">ÜSt Menüsü</label>
                                <div class="controls">
                                    <select name="ust" class="span8">
                                        <option value="0">Yok</option>
                                        <?
                                        $ustmenler = $database->query("SELECT * FROM menuler WHERE ustid=0 ORDER BY sira")->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($ustmenler as $u) {?>
                                            <option <?=($u["id"]==$ust)?"selected":""?> value="<?=$u["id"]?>"><?=$u["baslik"]?></option>
                                        <?}?>
                                    </select>
                                    <span class="help-inline">Yoksa Boş Geçiniz.</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Menü Sırası</label>
                                <div class="controls">
                                    <input type="number" id="basicinput" value="<?=$sira?>" name="sira" placeholder="Menu Sırası" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "menuler":
        $sth = $database->prepare("SELECT * FROM menuler WHERE ustid=0 ORDER BY id DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Menüler</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Menü Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/" target="_blank">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Başlık
                                </th>
                                <th>
                                    Link
                                </th>
                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?
                                        if($d["sayfa"] != 0){
                                            $sayfaData = $database->query("SELECT seo FROM sayfalar WHERE id=" . $d["sayfa"])->fetchAll(PDO::FETCH_ASSOC);
                                            echo "http://kirklareliamatorspor.org/sayfalar/".$sayfaData[0]["seo"].".html";
                                        }else{
                                            echo $d["link"];
                                        }
                                        ?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="index.php?action=altmenuler&m=<?=$d["id"]?>" class="btn btn-mini btn-primary"><i class="icon-arrow-down"></i> Alt Menüleri</a>
                                        <a href="index.php?action=menukaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.menuSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;

    case "altmenuler":
        $menud = $database->query("SELECT baslik FROM menuler WHERE id=" . $_GET["m"])->fetchAll(PDO::FETCH_ASSOC);
        $sth = $database->prepare("SELECT * FROM menuler WHERE ustid=".$_GET["m"]." ORDER BY id DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3><?=$menud[0]["baslik"]?> Menüsünün Alt Menüleri  [<a href="javascript:window.history.back();">Geri</a> ]</h3>
                    </div>
                    <div class="module-body ">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Menü Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/" target="_blank">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Başlık
                                </th>
                                <th>
                                    Link
                                </th>
                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?
                                        if($d["sayfa"] != 0){
                                            $sayfaData = $database->query("SELECT seo FROM sayfalar WHERE id=" . $d["sayfa"])->fetchAll(PDO::FETCH_ASSOC);
                                            echo "http://kirklareliamatorspor.org/sayfalar/".$sayfaData[0]["seo"].".html";
                                        }else{
                                            echo $d["link"];
                                        }
                                        ?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="index.php?action=menukaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.menuSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;

    case "disiplinsil":
        if($_GET["id"]!=""){
            $database->query("DELETE FROM kararlar WHERE id=" . $_GET["id"]);
            header("location: index.php?action=tertip&msg=del");
        }
        break;
    case "disiplinkaydet":
        if($_POST){
            $baslik = addslashes(strip_tags(trim($_POST["baslik"])));
            $icerik = addslashes($_POST["icerik"]);
            $tarih = $_POST["tarih"];
            if($_GET["id"]==""){
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("INSERT INTO kararlar(baslik,icerik,tarih,seo,tip) VALUES(:baslik,:icerik,:tarih,:seo,:tip)");
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":tip", 1, PDO::PARAM_INT);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->execute();
                        header("location: index.php?action=disiplin&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Disiplin Kararı Eklenemedi", "TEXT"=>"Disiplin Kararı Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("UPDATE kararlar SET baslik=:baslik,icerik=:icerik,seo=:seo,tarih=:tarih WHERE id=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);

                        $sth->execute();
                        header("location: index.php?action=disiplin&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Disiplin Kararı Eklenemedi", "TEXT"=>"Disiplin Kararı Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM kararlar WHERE id=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];
                $baslik = stripslashes($data->baslik);
                $icerik = stripslashes($data->icerik);
                $tarih = stripslashes($data->tarih);
            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Disiplin Komitesi Kararı Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=disiplinkaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Başlık</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baslik)?>" name="baslik" placeholder="Başlık" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">İçerik</label>
                                <div class="controls">
                                    <textarea id="tiny" name="icerik" class="span8" rows="5"><?=stripslashes($icerik)?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Tarih</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($tarih)?>" name="tarih" placeholder="Tarih" class="span8">
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "disiplin":
        $sth = $database->prepare("SELECT * FROM kararlar WHERE tip=1 ORDER BY tarih DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Disiplin Komitesi Kararları</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Karar Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/kararlar/<?=base64_decode($_GET["s"])?>.html">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Başlık
                                </th>
                                <th>
                                    Tarih
                                </th>
                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?=$d["tarih"]?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Göster</a>
                                        <a href="index.php?action=disiplinkaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.disiplinSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;


    case "tertipsil":
        if($_GET["id"]!=""){
            $database->query("DELETE FROM kararlar WHERE id=" . $_GET["id"]);
            header("location: index.php?action=tertip&msg=del");
        }
        break;
    case "tertipkaydet":
        if($_POST){
            $baslik = addslashes(strip_tags(trim($_POST["baslik"])));
            $icerik = addslashes($_POST["icerik"]);
            $tarih = $_POST["tarih"];
            if($_GET["id"]==""){
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("INSERT INTO kararlar(baslik,icerik,tarih,seo,tip) VALUES(:baslik,:icerik,:tarih,:seo,:tip)");
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":tip", 0, PDO::PARAM_INT);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->execute();
                        header("location: index.php?action=tertip&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Tertip Kararı Eklenemedi", "TEXT"=>"Tertip Kararı Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("UPDATE kararlar SET baslik=:baslik,icerik=:icerik,seo=:seo,tarih=:tarih WHERE id=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);

                        $sth->execute();
                        header("location: index.php?action=tertip&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Tertip Kararı Eklenemedi", "TEXT"=>"Tertip Kararı Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM kararlar WHERE id=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];
                $baslik = stripslashes($data->baslik);
                $icerik = stripslashes($data->icerik);
                $tarih = stripslashes($data->tarih);
            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Tertip Komitesi Kararı Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=tertipkaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Başlık</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baslik)?>" name="baslik" placeholder="Başlık" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">İçerik</label>
                                <div class="controls">
                                    <textarea id="tiny" name="icerik" class="span8" rows="5"><?=stripslashes($icerik)?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Tarih</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($tarih)?>" name="tarih" placeholder="Tarih" class="span8">
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "tertip":
        $sth = $database->prepare("SELECT * FROM kararlar WHERE tip=0 ORDER BY tarih DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Tertip Komitesi Kararları</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Karar Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/kararlar/<?=base64_decode($_GET["s"])?>.html">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Başlık
                                </th>
                                <th>
                                    Tarih
                                </th>
                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?=$d["tarih"]?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Göster</a>
                                        <a href="index.php?action=tertipkaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.tertipSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;


    case "kulupsil":
        if($_GET["id"]!=""){
            $database->query("DELETE FROM kulupler WHERE kulid=" . $_GET["id"]);
            header("location: index.php?action=kulupler&msg=del");
        }
        break;
    case "kulupkaydet":
        if($_POST){

            $kulupadi = addslashes(strip_tags(trim($_POST["kulupadi"])));
            $adres = addslashes(strip_tags(trim($_POST["adres"])));
            $telefon = addslashes(strip_tags(trim($_POST["telefon"])));
            $fax = addslashes(strip_tags(trim($_POST["fax"])));
            $baskan = addslashes(strip_tags(trim($_POST["baskan"])));
            $baskantel = addslashes(strip_tags(trim($_POST["baskantel"])));
            $genelkurul = addslashes(strip_tags(trim($_POST["genelkurul"])));
            $songenelkurul =strip_tags(trim($_POST["songenelkurul"]));
            $aciklama = addslashes(strip_tags(trim($_POST["aciklama"])));

            if($_GET["id"]==""){
                try {
                    if($kulupadi != "") {
                        $sth = $database->prepare("INSERT INTO kulupler(ad,adres,telefon,faks,baskan,baskantel,genelkurul,ensongkurul,yazi,kname) VALUES(:ad,:adres,:telefon,:faks,:baskan,:baskantel,:genelkurul,:ensongkurul,:yazi,:kname)");
                        $sth->bindValue(":ad", $kulupadi, PDO::PARAM_STR);
                        $sth->bindValue(":adres", $adres, PDO::PARAM_STR);
                        $sth->bindValue(":telefon", $telefon, PDO::PARAM_STR);
                        $sth->bindValue(":faks", $fax, PDO::PARAM_STR);
                        $sth->bindValue(":baskan", $baskan, PDO::PARAM_STR);
                        $sth->bindValue(":baskantel", $baskantel, PDO::PARAM_STR);
                        $sth->bindValue(":genelkurul", $genelkurul, PDO::PARAM_STR);
                        $sth->bindValue(":ensongkurul", $songenelkurul, PDO::PARAM_STR);
                        $sth->bindValue(":yazi", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":kname", seo($kulupadi), PDO::PARAM_STR);
                        $sth->execute();
                        header("location: index.php?action=kulupler&msg=ok&s=" . base64_encode(seo($kulupadi)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Kulüp Eklenemedi", "TEXT"=>"Kulüp Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($kulupadi != "") {
                        $sth = $database->prepare("UPDATE kulupler SET ad=:ad,adres=:adres,telefon=:telefon,faks=:faks,baskan=:baskan,baskantel=:baskantel,genelkurul=:genelkurul,ensongkurul=:ensongkurul,yazi=:yazi,kname=:kname WHERE kulid=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":ad", $kulupadi, PDO::PARAM_STR);
                        $sth->bindValue(":adres", $adres, PDO::PARAM_STR);
                        $sth->bindValue(":telefon", $telefon, PDO::PARAM_STR);
                        $sth->bindValue(":faks", $fax, PDO::PARAM_STR);
                        $sth->bindValue(":baskan", $baskan, PDO::PARAM_STR);
                        $sth->bindValue(":baskantel", $baskantel, PDO::PARAM_STR);
                        $sth->bindValue(":genelkurul", $genelkurul, PDO::PARAM_STR);
                        $sth->bindValue(":ensongkurul", $songenelkurul, PDO::PARAM_STR);
                        $sth->bindValue(":yazi", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":kname", seo($kulupadi), PDO::PARAM_STR);

                        $sth->execute();
                        header("location: index.php?action=kulupler&msg=ok&s=" . base64_encode(seo($kulupadi)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Kulüp Eklenemedi", "TEXT"=>"Kulüp Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM kulupler WHERE kulid=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];


                $kulupadi = stripslashes($data->ad);
                $adres = stripslashes($data->adres);
                $telefon = stripslashes($data->telefon);
                $fax = stripslashes($data->faks);
                $baskan = stripslashes($data->baskan);
                $baskantel = stripslashes($data->baskantel);
                $genelkurul = stripslashes($data->genelkurul);
                $songenelkurul = stripslashes($data->ensongkurul);
                $aciklama = stripslashes($data->yazi);
            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Kulüp Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=kulupkaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Kulüp Adı</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($kulupadi)?>" name="kulupadi" placeholder="Kuluüp Başlığı" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Adres</label>
                                <div class="controls">
                                    <textarea name="adres" class="span8" rows="5"><?=stripslashes($adres)?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Telefon</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($telefon)?>" name="telefon" placeholder="Telefon" class="span8">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Fax</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($fax)?>" name="fax" placeholder="Fax" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Başkan</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baskan)?>" name="baskan" placeholder="Başkan" class="span8">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Başkan Telefon</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baskantel)?>" name="baskantel" placeholder="Başkan Telefon" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Genelkurul</label>
                                <div class="controls">
                                    <textarea name="genelkurul" class="span8" rows="5"><?=stripslashes($genelkurul)?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Son Genelkurul Tarihi</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($songenelkurul)?>" name="songenelkurul" placeholder="Son Genelkurul Tarihi" class="span8">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="basicinput">Açıklama</label>
                                <div class="controls">
                                    <textarea name="aciklama" class="span8" rows="5"><?=stripslashes($aciklama)?></textarea>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "kulupler":
        $sth = $database->prepare("SELECT * FROM kulupler ORDER BY ad DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Üye Kulüpler</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Kulüp Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/kulupler/<?=base64_decode($_GET["s"])?>.html">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Kulüp Adı</th>
                                <th>Adres</th>
                                <th>Başkan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["ad"]?>
                                    </td>
                                    <td class="center">
                                        <?=$d["adres"]?>
                                    </td>
                                    <td class="center">
                                        <?=$d["baskan"]?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Göster</a>
                                        <a href="index.php?action=kulupkaydet&id=<?=$d["kulid"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.kulupSil('<?=$d["kulid"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;


    case "sayfasil":
        if($_GET["id"]!=""){
            $database->query("DELETE FROM sayfalar WHERE id=" . $_GET["id"]);
            header("location: index.php?action=sayfalar&msg=del");
        }
        break;
    case "sayfakaydet":
        if($_POST){
            $baslik = addslashes(strip_tags(trim($_POST["baslik"])));
            $aciklama = addslashes(strip_tags(trim($_POST["aciklama"])));
            $icerik = addslashes($_POST["icerik"]);
            $tarih = date('Y-m-d H:i:s');
            if($_GET["id"]==""){
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("INSERT INTO sayfalar(baslik,aciklama,icerik,tarih,duzenlenme,seo) VALUES(:baslik,:aciklama,:icerik,:tarih,:duzenleme,:seo)");
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":aciklama", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":duzenleme", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->execute();
                        header("location: index.php?action=sayfalar&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Sayfa Eklenemedi", "TEXT"=>"Sayfa Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($baslik != "" && $icerik != "") {
                        $sth = $database->prepare("UPDATE sayfalar SET baslik=:baslik,aciklama=:aciklama,icerik=:icerik,duzenlenme=:duzenleme,seo=:seo WHERE id=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":aciklama", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":icerik", $icerik, PDO::PARAM_STR);
                        $sth->bindValue(":duzenleme", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);

                        $sth->execute();
                        header("location: index.php?action=sayfalar&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Sayfa Eklenemedi", "TEXT"=>"Sayfa Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM sayfalar WHERE id=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];
                $baslik = stripslashes($data->baslik);
                $aciklama = stripslashes($data->aciklama);
                $icerik = stripslashes($data->icerik);
            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Sayfa Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" method="post" action="index.php?action=sayfakaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Sayfa Başlığı</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baslik)?>" name="baslik" placeholder="Sayfa Başlığı" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Açıklama</label>
                                <div class="controls">
                                    <textarea name="aciklama" class="span8" rows="5"><?=stripslashes($aciklama)?></textarea>
                                </div>
                            </div>



                            <div class="control-group">
                                <label class="control-label" for="basicinput">İçerik</label>
                                <div class="controls">
                                    <textarea id="tiny" name="icerik" class="span8" rows="5"><?=stripslashes($icerik)?></textarea>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "sayfalar":
        $sth = $database->prepare("SELECT * FROM sayfalar ORDER BY tarih DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Sayfalar</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Sayfanız Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/sayfalar/<?=base64_decode($_GET["s"])?>.html">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Sayfa Başlığı
                                </th>
                                <th>
                                    Tarih
                                </th>
                                <th>Okuma</th>

                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?=turkcetarih('j F Y',$d["tarih"]);  ?>
                                    </td>
                                    <td class="center">
                                        <?=$d["okunma"]?>
                                    </td>
                                    <td class="center"  width="250">
                                        <a href="/sayfa/<?=seo($d["baslik"])?>/<?=$d["id"]?>" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Göster</a>
                                        <a href="index.php?action=sayfakaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.sayfaSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;

    case "duyurusil":
        if($_GET["id"]!=""){
            $d = $database->query("SELECT resim FROM duyurular WHERE id=" . $_GET["id"])->fetchAll(PDO::FETCH_ASSOC);
            $r = $d[0]["resim"];
            if($r!="" && $r!="onemli.jpg" && $r!="dikkat.png" && $r!="normal.jpg") {
                unlink("../upload/haberler/" . $r);
            }
            $database->query("DELETE FROM duyurular WHERE id=" . $_GET["id"]);
            header("location: index.php?action=duyurular&msg=del");
        }
        break;
    case "duyurukaydet":
        $resimgoster=-1;
        if($_POST){
            $baslik = addslashes(strip_tags(trim($_POST["baslik"])));
            $aciklama = addslashes(strip_tags(trim($_POST["aciklama"])));
            $duyuru = addslashes($_POST["duyuru"]);
            $manset = $_POST["manset"];
            $resimgoster = $_POST["resimgoster"];
            $tarih = date('Y-m-d H:i:s');
            $resim="normal.jpg";
            if($_POST["resim_tip"]=="") {

                include_once "php/verot/class.upload.php";

                $handle = new upload($_FILES['resim']);
                if ($handle->uploaded) {
                    $handle->file_new_name_body = seo($tarih);
                    $handle->image_resize = true;
                    $handle->image_x = 500;
                    $handle->image_y = 300;
                    $handle->image_ratio_y = true;
                    $handle->process('../upload/haberler/');
                    if ($handle->processed) {
                        $handle->clean();
                    } else {
                        echo 'error : ' . $handle->error;
                    }
                }
                $resim = seo($tarih) . "." .$handle->file_src_name_ext;
            }else{
                $resim = $_POST["resim_tip"];
            }
            if($_GET["id"]==""){
                try {
                    if($baslik != "" && $duyuru != "") {
                        mkdir("../files/resimler/" . seo($baslik));
                        $sth = $database->prepare("INSERT INTO duyurular(baslik,aciklama,duyuru,manset,tarih,duzenleme,seo,resim,resimgoster) VALUES(:baslik,:aciklama,:duyuru,:manset,:tarih,:duzenleme,:seo,:resim,:resimgoster)");
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":aciklama", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":duyuru", $duyuru, PDO::PARAM_STR);
                        $sth->bindValue(":manset", $manset, PDO::PARAM_STR);
                        $sth->bindValue(":resimgoster", $resimgoster, PDO::PARAM_STR);
                        $sth->bindValue(":tarih", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":duzenleme", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->bindValue(":resim", $resim, PDO::PARAM_STR);
                        $sth->execute();
                        sendAll($database, "Duyuru Eklendi",$baslik,"DetailActivity",array());
                        header("location: index.php?action=duyurular&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Duyuru Eklenemedi", "TEXT"=>"Duyuru Eklenmedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }else{
                try {
                    if($baslik != "" && $duyuru != "") {
                        $sth = $database->prepare("UPDATE duyurular SET baslik=:baslik,aciklama=:aciklama,duyuru=:duyuru,manset=:manset,duzenleme=:duzenleme,seo=:seo,resim=:resim,resimgoster=:resimgoster WHERE id=:id");
                        $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                        $sth->bindValue(":baslik", $baslik, PDO::PARAM_STR);
                        $sth->bindValue(":aciklama", $aciklama, PDO::PARAM_STR);
                        $sth->bindValue(":duyuru", $duyuru, PDO::PARAM_STR);
                        $sth->bindValue(":manset", $manset, PDO::PARAM_STR);
                        $sth->bindValue(":resimgoster", $resimgoster, PDO::PARAM_STR);
                        $sth->bindValue(":duzenleme", $tarih, PDO::PARAM_STR);
                        $sth->bindValue(":seo", seo($baslik), PDO::PARAM_STR);
                        $sth->bindValue(":resim", $resim, PDO::PARAM_STR);

                        $sth->execute();
                        header("location: index.php?action=duyurular&msg=ok&s=" . base64_encode(seo($baslik)));

                    }else{
                        $message = array("TYPE"=>"ERROR", "TITLE"=>"Duyuru Eklenemedi", "TEXT"=>"Duyuru Güncellenemedi. Lütfen Tüm Alanları Doldurun.");
                    }
                }catch(PDOException $ex){
                    die($ex->getMessage());
                }
            }

        }else{
            if($_GET["id"]!="") {

                $sth = $database->prepare("SELECT * FROM duyurular WHERE id=:id");
                $sth->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
                $sth->execute();
                $data = $sth->fetchAll(PDO::FETCH_ASSOC);
                $data = (object)$data[0];
                $baslik = stripslashes($data->baslik);
                $aciklama = stripslashes($data->aciklama);
                $duyuru = stripslashes($data->duyuru);
                $resim = $data->resim;
                $manset = $data->manset;
                $resimgoster = $data->resimgoster;

            }
        }
        include_once "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Duyuru Kaydet</h3>
                    </div>
                    <div class="module-body">

                        <?
                        if(is_array($message)){
                            if($message["TYPE"]=="ERROR") { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?=$message["TEXT"]?>
                                </div>
                            <? }
                        }
                        ?>

                        <form class="form-horizontal row-fluid" enctype="multipart/form-data" method="post" action="index.php?action=duyurukaydet<?=($_GET["id"]!=""?"&id=".$_GET["id"]:"")?>">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Duyuru Başlığı</label>
                                <div class="controls">
                                    <input type="text" id="basicinput" value="<?=stripslashes($baslik)?>" name="baslik" placeholder="Duyuru Başlığı" class="span8">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Açıklama</label>
                                <div class="controls">
                                    <textarea name="aciklama" class="span8" rows="5"><?=stripslashes($aciklama)?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Manşette Göster</label>
                                <div class="controls">
                                    <label class="radio inline">
                                        <input type="radio" name="manset" id="optionsRadios1" value="1" checked="">
                                        Evet
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="manset" id="optionsRadios2" value="0" >
                                        Hayır
                                    </label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">İçerikte Resmi Göster</label>
                                <div class="controls">
                                    <label class="radio inline">
                                        <input type="radio" name="resimgoster" id="optionsRadios3" value="1" <?=($resimgoster=="1")?"checked":""?>>
                                        Evet
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="resimgoster" id="optionsRadios4" value="0" <?=($resimgoster=="-1")?"checked":""?> <?=($resimgoster=="0")?"checked":""?>>
                                        Hayır
                                    </label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Duyuru Tipi</label>
                                <div class="controls">
                                    <label class="radio inline">
                                        <input type="radio" name="resim_tip" id="optionsRadios1" <?=($resim=="normal.jpg")?"checked":""?> value="normal.jpg" >
                                        <img src="../upload/haberler/normal.jpg"  width="100" height="75"><br> Normal
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="resim_tip" id="optionsRadios2" <?=($resim=="dikkat.png")?"checked":""?> value="dikkat.png" >
                                        <img src="../upload/haberler/dikkat.png" width="100" height="75"><br> Dikkat
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="resim_tip" id="optionsRadios2" <?=($resim=="onemli.jpg")?"checked":""?> value="onemli.jpg" >
                                        <img src="../upload/haberler/onemli.jpg" width="100" height="75"><br> Önemli

                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="resim_tip" id="optionsRadios2" <?=($resim!="onemli.jpg" && $resim!="dikkat.png" && $resim!="normal.jpg")?"checked":""?> value="">Benim Seçtiğim

                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Resim Seç</label>
                                <div class="controls">
                                    <label class="radio inline">
                                        <input type="file" name="resim"/>
                                        <?
                                        if($resim!="" && $resim!="onemli.jpg" && $resim!="dikkat.png" && $resim!="normal.jpg"){
                                            ?><br>
                                            <img src="../upload/haberler/<?=$resim?>" width="100" height="75">

                                        <?
                                        }
                                        ?>

                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Duyuru</label>
                                <div class="controls">
                                    <textarea id="tiny" name="duyuru" class="span8" rows="5"><?=stripslashes($duyuru)?></textarea>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">Kaydet</button>
                                    <button type="reset" class="btn">Temizle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
        include_once "footer.php";
        break;
    case "duyurular":
        $sth = $database->prepare("SELECT * FROM duyurular ORDER BY tarih DESC");
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        include "header.php";
        ?>
        <div class="span9">
            <div class="content">
                <div class="module">
                    <div class="module-head">
                        <h3>Duyurular</h3>
                    </div>
                    <div class="module-body table">
                        <?
                        if(isset($_GET["msg"])){ ?>
                            <? if($_GET["msg"]=="ok") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Duyurunuz Başarıyla Kaydedildi. Görmek İçin <a href="http://kirklareliamatorspor.org/duyurular/<?=base64_decode($_GET["s"])?>.html">Tıklayın</a>
                                </div>
                            <? } ?>

                            <? if($_GET["msg"]=="del") { ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Başarıyla Silindi!
                                </div>
                            <? } ?>
                        <? } ?>
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                               width="100%">
                            <thead>
                            <tr>
                                <th>
                                    Duyuru Başlığı
                                </th>
                                <th>
                                    Tarih
                                </th>
                                <th>Okuma</th>

                                <th>
                                    İşlemler
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($data as $d) {?>
                                <tr class="odd gradeX">
                                    <td class="center">
                                        <?=$d["baslik"]?>
                                    </td>
                                    <td class="center">
                                        <?=turkcetarih('j F Y',$d["tarih"]);  ?>
                                    </td>
                                    <td class="center">
                                        <?=$d["okunma"]?>
                                    </td>
                                    <td class="center"  width="350">
                                        <a target="_blank" href="/haberler/<?=seo($d["baslik"])?>/<?=$d["id"]?>" class="btn btn-mini btn-primary"><i class="icon-external-link"></i> Göster</a>
                                        <a  href="index.php?action=dGaleri&id=<?=seo($d["baslik"])?>" class="btn btn-mini btn-success"><i class="icon-picture"></i> Galeri</a>
                                        <a href="index.php?action=duyurukaydet&id=<?=$d["id"]?>" class="btn btn-mini btn-warning"><i class="icon-edit"></i> Düzenle</a>
                                        <a onclick="$.duyuruSil('<?=$d["id"]?>')" class="btn btn-mini btn-danger"><i class="icon-trash"></i> Sil</a>
                                    </td>
                                </tr>
                            <?}?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-10">

        </div>
        <?
        include "footer.php";
        ?>
        <?
        break;

    case "logout":
        session_destroy();
        session_unset();
        unset($_SESSION["user"]);
        header("Location:index.php");
        break;
    case "login":
        if(isset($_SESSION["user"])){
            header("location: index.php?action=dash");

        }
        if($_POST){
            $error="";
            $username = stripslashes(trim($_POST["uname"]));
            $password = stripslashes(trim($_POST["upass"]));
            $response=$_POST["g-recaptcha-response"];
            $secret="6LeLSvMUAAAAAPOe4Ef9IuZ9t74e6cWMz9JaNUAq";
            $remoteip=$_SERVER["REMOTE_ADDR"];
      
            if($response && $username && $password){
                $md5 = md5($password);

                $captcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");
                
                $responseKeys = json_decode($captcha, true);
               
                if(intval($responseKeys["success"]) !== 1) {
                    $error= "oturum açılmadı hata captcha";
                }else {
                    try {
                        $query = $database->prepare("SELECT count(*) FROM kullanici WHERE k_adi=? and k_sifre=?");
                        $query->execute([$username, $md5]);
    
                        if ($query->fetchColumn()) {
                            $_SESSION['user'] = true;
                            $_SESSION['name'] = $username;
                            header("location: index.php?action=dash");
                        }else{
                            $error= "oturum açılmadı hata";
    
                        }
                    }catch (PDOException $ex){
                        echo $ex->getMessage();
                    }
                }
            }else{
                $error = "Lütfen Tüm Bilgileri Doldurun";
            }
        }
        ?>
        <!DOCTYPE html>
<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8">

<title>Kırklareli Askf Giriş</title>
<style>
body {
    background-size: cover;
    font-family: Montserrat;
}

.logo {
    width: 213px;
    height: 36px;
    margin: 30px auto;
}

.login-block {
    width: 320px;
    padding: 20px;
    background: #fff;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    margin: 0 auto;
}

.login-block h1 {
    text-align: center;
    color: #000;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.login-block input {
    width: 100%;
    height: 42px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}

.login-block input#username {
    background-size: 16px 80px;
}

.login-block input#username:focus {
    background-size: 16px 80px;
}

.login-block input#password {
    background-size: 16px 80px;
}

.login-block input#password:focus {
    background-size: 16px 80px;
}

.login-block input:active, .login-block input:focus {
    border: 1px solid #ff656c;
}

.login-block button {
    width: 100%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
}

.login-block button:hover {
    background: #ff7b81;
}

</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

<div class="logo"></div>
<div class="login-block">
    <h1>Giriş</h1>
    <form role="form" method="post">
    <input type="text" value="" name="uname" placeholder="Kullanıcı Adı" id="username" />
    <input type="password" value="" name="upass" placeholder="Şifre" id="password" />
         <?
        if(isset($error)){
            ?>
            <div class="form-group" style="color:red;">
                <b><?=$error?></b>
            </div>
        <?}?>
        <div class="g-recaptcha" data-sitekey="6LeLSvMUAAAAAAuAXeEOZQC0ftKkwe6MmY7vym80"></div>

    <button type="submit">Giriş</button>
    </form>
</div>

</body>

</html>

        <?
        break;
    default:
        if($_SESSION["user"]==""){
            header("Location: index.php?action=login");

        }else{
            header("Location: index.php?action=dash");

        }
        break;
}
?>
