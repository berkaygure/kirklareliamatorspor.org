<?php
include "config.php";
?>
<? $ligBilgiler  = $database->query("SELECT * FROM leagues WHERE ID=" . $_GET["lig"])->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$ligBilgiler["league_name"]?> <?=$_GET["hafta"]?>. HAFTA FİKSTÜR VE PUAN DURUMU</title>
    <script>window.print();</script>
</head>

<body>
<?$datalar = $database->query("SELECT * FROM maclar WHERE LIG_ID=" . $_GET["lig"] . " and HAFTA=" . $_GET["hafta"] . " ORDER BY ID")->fetchAll(PDO::FETCH_ASSOC); ?>

<center>
    <h2>KIRKLARELİ AMATÖR SPOR KULÜPLERİ FEDERASYONU</h2>
    <h3><?=$ligBilgiler["league_name"]?> <?=$_GET["hafta"]?>. HAFTA FİKSTÜR VE PUAN DURUMU</h3>
    <table cellpadding="10" style="width:800px;" border="1">
        <tr>
            <th>Tarih</th>
            <th>Ev Sahibi</th>
            <th>Skor</th>
            <th>Deplasman Takım</th>
            <th>Stad</th>

        </tr>
        <?

        $raporlar = array();

        foreach ($datalar as $data) {

            $takim1 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
            $takim2 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC);
            if($data["ACIKLAMA"]!="" && trim($data["ACIKLAMA"])!="YOK"){
                $r["aciklama"] = $data["ACIKLAMA"];
                $r["mac"] = $takim1["tname"] . " - " . $takim2["tname"] . " KARŞILAŞMASI";
                $raporlar[] = $r;
            }
            ?>
            <tr>
                <td align="center"><?=turkcetarih('j.M.Y H.i',$data["tarih"]);  ?></td>
                <td align="center"><?=$takim1["tname"]?></td>
                <td align="center"><?=($data["hscore"]=="-1"?"-":$data["hscore"])?> - <?=($data["ascore"]=="-1"?"-":$data["ascore"])?></td>
                <td align="center"><?=$takim2["tname"]?></td>
                <td align="center"><?=$data["STAD"]?></td>
            </tr>
            <?
        }
        ?>

    </table>

    <? if(count($raporlar)> 0 ) {?>
    <h4>Maç Raporları</h4>
    <table cellpadding="10" style="width:800px;" border="1">
            <tr>
                <td style="font-size:11px">
                    <? foreach ($raporlar as $rapor) {?>
                        * <?=$rapor["aciklama"]?> (<?=$rapor["mac"]?>)<br>
                    <?}?>
                </td>
            </tr>
    </table>
    <?}?>
<?

$sth = $database->prepare("SELECT * FROM leagues WHERE ID=:id");
$sth->bindValue(":id", $_GET["lig"]);
$sth->execute();
$ligData = $sth->fetchAll(PDO::FETCH_ASSOC);
$hafta = $_GET["hafta"];
$sh = $database->prepare(
    "SELECT
                  tname AS Team, Sum(P) AS P,Sum(W) AS W,Sum(D) AS D,Sum(L) AS L,
                  SUM(F) as F,SUM(A) AS A,SUM(GD) AS GD,SUM(Pts) AS Pts
                FROM(
                    SELECT
                    hteam Team, playing,
                    IF(hscore > -1,1,0) P,
                    IF(hscore > -1 and hscore > ascore,1,0) W,
                    IF(hscore > -1 and hscore = ascore,1,0) D,
                    IF(hscore > -1 and hscore < ascore,1,0) L,
                    IF(hscore > -1,hscore,0) F,
                    IF(hscore > -1,ascore,0) A,
                    IF(hscore > -1,hscore-ascore,0) GD,
                    CASE WHEN hscore > -1 and hscore > ascore THEN 3 WHEN hscore > -1 and hscore = ascore THEN 1 ELSE 0 END PTS
                  FROM maclar WHERE HAFTA <= :hafta
                  UNION ALL
                  SELECT
                    ateam,playing,
                    IF(ascore > -1,1,0) P,
                    IF(ascore > -1 and hscore < ascore,1,0) W,
                    IF(ascore > -1 and hscore = ascore,1,0) D,
                    IF(ascore > -1  and hscore > ascore,1,0) L,
                    IF(ascore > -1 ,ascore,0) F,
                    IF(ascore > -1,hscore,0) A,
                    IF(ascore > -1,ascore-hscore,0) GD,
                    CASE WHEN ascore > -1 and hscore < ascore THEN 3 WHEN ascore > -1 and hscore = ascore THEN 1 ELSE 0 END
                  FROM maclar WHERE HAFTA <= :hafta
                ) as tot
                JOIN takimlar t ON tot.Team=t.id
                WHERE LIG_ID = :id
                GROUP BY Team
                ORDER BY SUM(Pts) DESC,SUM(GD) DESC, Team ASC
            ");

$sh->bindValue(":id", $_GET["lig"]);
$sh->bindValue(":hafta",$hafta);
$sh->execute();
$getLig = $sh->fetchAll(PDO::FETCH_ASSOC);
?>


    <br>
    <h3>Puan Tablosu</h3>
    <table cellpadding="10" style="width:800px;" border="1">
        <tr>
            <th width="5%">Sıra</th>
            <th width="60%">Takım</th>
            <th>O</th>
            <th>G</th>
            <th>B</th>
            <th>M</th>
            <th>A</th>
            <th>Y</th>
            <th>AV</th>
            <th>PU</th>
        </tr>
        <?
        for($i=0; $i < count($getLig); $i++){
            echo '<tr>';
            echo '<td style="text-align: center">'.($i+1).'</td>';
            echo '<td>'.$getLig[$i]["Team"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["P"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["W"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["D"].'</td>';
            echo'<td style="text-align: center">'.$getLig[$i]["L"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["F"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["A"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["GD"].'</td>';
            echo '<td style="text-align: center">'.$getLig[$i]["Pts"].'</td>';
            echo '</tr>';

        }
        ?>

    </table>


</center>
</body>
</html>

<?php


