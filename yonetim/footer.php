
</div>
</div>
<!--/.container-->
</div>
<!--/.wrapper-->
<div class="footer">
    <div class="container">
        <b class="copyright">&copy; <?=date("Y")?> kikrlareliamatorspor.org </b>All rights reserved.
    </div>
</div>
<script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<script src="plugins/tinymce/tinymce.min.js"></script>
<script src="craftpip/jquery-confirm.min.js"></script>
<script src="datetimepicker-master/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script src="plugins/dropzone/dist/dropzone.js"></script>
<script>
    $().ready(function() {
        Dropzone.options.dz= {
            dictDefaultMessage: "Dosya Yüklemek İçin Tıkalyın"
        };
        $(".remove").click(function () {
                var id = $(this).data("id");
                var path = $(this).data("path");
            var t = $(this);
                $.ajax({
                    type:"post",
                    url :"index.php?action=deleteGaleri",
                    data:{id:id,path:path},
                    success:function (res) {
                        t.parent().fadeOut();
                    }
                })
        });


        $('form').submit(function() {
            $("#loadingDiv").css("display","block");
            return true;
        });

/*
        function elFinderBrowser(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: 'elfinder-2.x/elfinder.html',// use an absolute path!
                title: 'Dosya Yöneticisi',
                width: 900,
                height: 450,
                lang: 'tr',

                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }*/

        tinymce.init({

            selector: "#tiny",theme: "modern",width: 680,height: 300,
            plugins: [
                "paste contextmenu advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            toolbar1: "paste undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  |  preview code ",
            image_advtab: true ,
            language_url: "tinymce/tr.js",
            relative_urls: true,
            paste_retain_style_properties: "all",
            paste_auto_cleanup_on_paste : true,
            content_css : "../css/custom_content.css",

            contextmenu: "link image inserttable paste | cell row column deletetable",
            external_filemanager_path:"/yonetim/filemanager/",
            filemanager_title:"Dosya Yöneticisi" ,
            external_plugins: { "filemanager" : "/yonetim/filemanager/plugin.min.js"}
        });
           /* tinymce.init({
                selector: '#tiny',
                language_url: "tinymce/tr.js",
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code image'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                file_browser_callback: elFinderBrowser
            });*/

    });
</script>
<!-- Include jQuery, jQuery UI, elFinder (REQUIRED) -->

<script type="text/javascript">
    /*var FileBrowserDialogue = {
        init: function() {
            // Here goes your code for setting your custom things onLoad.
        },
        mySubmit: function (URL) {
            // pass selected file path to TinyMCE
            parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);

            // force the TinyMCE dialog to refresh and fill in the image dimensions
            var t = parent.tinymce.activeEditor.windowManager.windows[0];
            t.find('#src').fire('change');

            // close popup window
            parent.tinymce.activeEditor.windowManager.close();
        }
    }
    $().ready(function() {
        if($("#fileManager").length>0) {
            var elf = $('#fileManager').elfinder({
                url: 'elfinder-2.x/php/connector.php',  // connector URL
                lang: 'tr'
            }).elfinder('instance');
        }
    });*/
    $().ready(function() {
        
        if($("#haftaDegistir").length > 0){
            $("#haftaDegistir").change(function () {

                window.location.href="index.php?action=fikstur&id=" + $("#haftaDegistir").data("lig") + "&hafta=" + this.value;
            })
        }
        
        
       /* var elf = $('#elfinder').elfinder({
            // set your elFinder options here
            url: 'elfinder-2.x/php/connector.php',  // connector URL
            getFileCallback: function(file) { // editor callback
                // file.url - commandsOptions.getfile.onlyURL = false (default)
                // file     - commandsOptions.getfile.onlyURL = true
                FileBrowserDialogue.mySubmit(file); // pass selected file path to TinyMCE
            }
        }).elfinder('instance');

            if($('.datetimepicker').length > 0) {
                jQuery.datetimepicker.setLocale('tr');

                $('.datetimepicker').datetimepicker({
                    dayOfWeekStart:1,
                    allowBlank:true,
                    format: 'd.m.Y H:i'
                });

            }*/

            $.duyuruSil = function(id){
                $.confirm({
                    title: 'Emin Misiniz?',
                    content: 'Duyuruyu Silmek İstediğinize Emin Misiniz?!',
                    confirmButton:'Sil',
                    cancelButton:'Kapat',
                    confirm: function(){
                       window.location.href="index.php?action=duyurusil&id=" + id;
                    },
                    cancel: function(){
                    }
                });
            }

        $.sayfaSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Sayfayı Silmek İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=sayfasil&id=" + id;
                },
                cancel: function(){
                }
            });
        }

        $.kulupSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Kulübü Silmek İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=kulupsil&id=" + id;
                },
                cancel: function(){
                }
            });
        }



        $.tertipSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Kararı Silmek İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=tertipsil&id=" + id;
                },
                cancel: function(){
                }
            });
        }
        $.disiplinSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Kararı Silmek İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=disiplinsil&id=" + id;
                },
                cancel: function(){
                }
            });
        }
        $.menuSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Menüyü İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=menusil&id=" + id;
                },
                cancel: function(){
                }
            });
        }

        $.ligSil = function(id){
            $.confirm({
                title: 'Emin Misiniz?',
                content: 'Bu işlem veri Kaybına Yol Açabilir, Ligi Silmek İstediğinize Emin Misiniz?!',
                confirmButton:'Sil',
                cancelButton:'Kapat',
                confirm: function(){
                    window.location.href="index.php?action=ligsil&id=" + id;
                },
                cancel: function(){
                }
            });
        }

    });
</script>
<script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="scripts/myscript.js"></script>
<script src="scripts/common.js" type="text/javascript"></script>
</body>
</html>