<!DOCTYPE html>
<html lang="en">
<head>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>Kırklareli ASKF Yönetim Paneli v1</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
          rel='stylesheet'>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="craftpip/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="elfinder-2.x/css/elfinder.min.css">
    <link rel="stylesheet" type="text/css" href="datetimepicker-master/datetimepicker-master/jquery.datetimepicker.css"/ >
    <link rel="stylesheet" type="text/css" href="plugins/dropzone/dist/dropzone.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Kırklareli Amatör Spor Yönetim Sistemi </a>
            <div class="nav-collapse collapse navbar-inverse-collapse">

                <ul class="nav pull-right">

                    <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="images/user.png" class="nav-avatar" /> <?=$_SESSION["name"]?>
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?action=ayarkaydet">Ayarlar</a></li>
                            <li class="divider"></li>
                            <li><a href="index.php?action=logout">Çıkış</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>
    </div>
    <!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div id="loadingDiv" class="loading">Yukleniyor&#8230;</div>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="sidebar">
                    <ul class="widget widget-menu unstyled">
                        <li <?=($_GET["action"]=="dash")?'class="active"':""?>>
                            <a href="index.php?action=?dash"><i class="menu-icon icon-dashboard"></i>Ana Sayfa</a>
                        </li>

                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleMenu">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-th-large"></i>Menüler
                            </a>
                            <ul id="toggleMenu" class="<?=($_GET["action"]=="menuler" ||$_GET["action"]=="menukaydet")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=menuler">
                                        <i class="icon-list"></i>
                                        Menüler
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=menukaydet">
                                        <i class="icon-plus-sign"></i>
                                        Menü Ekle
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleDuyuru">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-bullhorn"></i>Duyurular
                            </a>
                            <ul id="toggleDuyuru" class="<?=($_GET["action"]=="duyurular" ||$_GET["action"]=="duyurukaydet")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=duyurular">
                                        <i class="icon-list"></i>
                                        Duyuruları Listele
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=duyurukaydet">
                                        <i class="icon-plus-sign"></i>
                                        Duyuru Ekle
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#togglePages">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-file"></i>Sayfalar
                            </a>
                            <ul id="togglePages" class="<?=($_GET["action"]=="sayfalar" ||$_GET["action"]=="sayfakaydet")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=sayfalar">
                                        <i class="icon-list"></i>
                                        Sayfaları Listele
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=sayfakaydet">
                                        <i class="icon-plus-sign"></i>
                                        Sayfa Ekle
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleTeams">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-tasks"></i>Üye Kulüpler
                            </a>
                            <ul id="toggleTeams" class="<?=($_GET["action"]=="kulupler" ||$_GET["action"]=="kulupkaydet")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=kulupler">
                                        <i class="icon-list"></i>
                                        Kulüpleri Göster
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=kulupkaydet">
                                        <i class="icon-plus-sign"></i>
                                        Yeni Kulüp
                                    </a>
                                </li>
                            </ul>
                        </li>



                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleTertip">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-list-alt"></i> Tertip Komitesi Kararları
                            </a>
                            <ul id="toggleTertip" class="<?=($_GET["action"]=="tertip" ||$_GET["action"]=="tertipkaydet")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=tertip">
                                        <i class="icon-list"></i>
                                        Tertip Komitesi Kararlarını Göster
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=tertipkaydet">
                                        <i class="icon-plus-sign"></i>
                                        Tertip Komitesi Kararı Ekle
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleDisiplin">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-list-alt"></i>  Disiplin Kurulu Kararları
                            </a>
                            <ul id="toggleDisiplin" class="<?=($_GET["action"]=="disiplinkaydet" ||$_GET["action"]=="disiplin")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=disiplin">
                                        <i class="icon-list"></i>
                                        Disiplin Kurulu Kararlarını Göster
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=disiplinkaydet">
                                        <i class="icon-plus-sign"></i>
                                        Disiplin Kurulu Kararı ekle
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li <?=($_GET["action"]=="dosya")?'class="active"':""?>>
                            <a href="index.php?action=dosya"><i class="menu-icon icon-dashboard"></i>Dosya Yöneticisi</a>
                        </li>
                        <li <?=($_GET["action"]=="musabaka")?'class="active"':""?>>
                            <a href="index.php?action=musabaka"><i class="menu-icon icon-dashboard"></i>Haftalık Müsbaka Programı</a>
                        </li>
                        <li <?=($_GET["action"]=="statu")?'class="active"':""?>>
                            <a href="index.php?action=statu"><i class="menu-icon icon-dashboard"></i>Statüler</a>
                        </li>
                        <li <?=($_GET["action"]=="hakem")?'class="active"':""?>>
                            <a href="index.php?action=hakem"><i class="menu-icon icon-dashboard"></i>Hakem ve Gözlemci Listesi</a>
                        </li>
                    </ul>
                    <!--/.widget-nav-->


                    <ul class="widget widget-menu unstyled">
                        <li>
                            <a class="collapsed" data-toggle="collapse" href="#toggleLig">
                                <i class="icon-chevron-down pull-right"></i><i class="menu-icon icon-th-list"></i>Ligler
                            </a>
                            <ul id="toggleLig" class="<?=($_GET["action"]=="ligler" ||$_GET["action"]=="ligyarat")?"":"collapse";?> unstyled">
                                <li>
                                    <a href="index.php?action=ligler">
                                        <i class="icon-list"></i>
                                        Ligleri Göster
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?action=ligyarat">
                                        <i class="icon-plus-sign"></i>
                                        Yeni Lig Yarat
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!--/.widget-nav-->
                    <ul class="widget widget-menu unstyled">
                        <li><a href="index.php?action=ayarkaydet"><i class="menu-icon icon-cog"></i>Ayarlar </a></li>

                        <li><a href="index.php?action=logout"><i class="menu-icon icon-signout"></i>Güvenli Çıkış </a></li>
                    </ul>

                    <!--/.widget-nav-->
                    <ul class="widget widget-menu unstyled">
                        <li><a  target="_blank" href="<?=$config["base"]?>"><i class="menu-icon icon-globe"></i>Siteyi Göster </a></li>

                    </ul>
                </div>
                <!--/.sidebar-->
            </div>
            <!--/.span3-->
