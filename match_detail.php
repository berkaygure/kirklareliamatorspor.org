<? include_once "_header_.php" ?>
<?
    $match = $database->query("SELECT * FROM maclar WHERE id=" . $_GET["id"])->fetch(PDO::FETCH_ASSOC);
    $lig = $database->query("SELECT * FROM leagues WHERE ID=" . $match["LIG_ID"])->fetch(PDO::FETCH_ASSOC);
    $home = $database->query("SELECT * FROM takimlar WHERE id=" . $match["hteam"])->fetch(PDO::FETCH_ASSOC);
    $away = $database->query("SELECT * FROM takimlar WHERE id=" . $match["ateam"])->fetch(PDO::FETCH_ASSOC);
    $homeLast5 = $database->query("SELECT * FROM maclar WHERE (hteam=" . $home["id"] . " or ateam=" . $home["id"] . ") and (hscore <> -1 and ascore <> -1) ORDER BY tarih DESC LIMIT 0,5")->fetchAll(PDO::FETCH_ASSOC);
    $awayLast5 = $database->query("SELECT * FROM maclar WHERE (hteam=" . $away["id"] . " or ateam=" . $away["id"] . ") and (hscore <> -1 and ascore <> -1) ORDER BY tarih DESC LIMIT 0,5")->fetchAll(PDO::FETCH_ASSOC);
    $homeLast5Icon = array();
    $awayLast5Icon = array();

    foreach ($homeLast5 as $h){
        if($h["hteam"] == $home["id"]){
            if($h["hscore"] > $h["ascore"]){
                $homeLast5Icon[]='<span class="tag tag-success">G</span>';
            }else if($h["hscore"] < $h["ascore"]){
                $homeLast5Icon[]='<span class="tag tag-danger">M</span>';

            }else{
                $homeLast5Icon[]='<span class="tag tag-primary">B</span>';
            }
        }else{
            if($h["hscore"] > $h["ascore"]){
                $homeLast5Icon[]='<span class="tag tag-danger">M</span>';
            }else if($h["hscore"] < $h["ascore"]){
                $homeLast5Icon[]='<span class="tag tag-success">G</span>';

            }else{
                $homeLast5Icon[]='<span class="tag tag-primary">B</span>';
            }
        }
    }

    foreach ($awayLast5 as $h){
        if($h["hteam"] == $away["id"]){
            if($h["hscore"] > $h["ascore"]){
                $awayLast5Icon[]='<span class="tag tag-success">G</span>';
            }else if($h["hscore"] < $h["ascore"]){
                $awayLast5Icon[]='<span class="tag tag-danger">M</span>';

            }else{
                $awayLast5Icon[]='<span class="tag tag-primary">B</span>';
            }
        }else{
            if($h["hscore"] > $h["ascore"]){
                $awayLast5Icon[]='<span class="tag tag-danger">M</span>';
            }else if($h["hscore"] < $h["ascore"]){
                $awayLast5Icon[]='<span class="tag tag-success">G</span>';

            }else{
                $awayLast5Icon[]='<span class="tag tag-primary">B</span>';
            }
        }
    }
    $homeLast5Icon = array_reverse($homeLast5Icon);
    $table = getLeagueTable($database,$match["LIG_ID"],$match["HAFTA"]);
?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-8">
                <h4 class="blog-title"></h4>
                <div class="row">
                    <div class="col-md-5">
                        <a class="text-md-center list-group-item list-group-item-warning"><h5><?=$home["tname"]?></h5></a>
                        <div class="text-md-right">
                            <?foreach ($homeLast5Icon as $icon):?>
                                <?=$icon?>
                            <?endforeach;?>
                        </div>


                    </div>
                    <div class="col-md-2 text-md-center">
                        <span style="padding: 8px;margin-top:15px;font-size:13px" class="tag tag-warning">
                            <?=(($match["hscore"]!="-1")?$match["hscore"]:"-")?>
                        </span>
                        -
                        <span style="padding:  8px;font-size:13px" class="tag tag-warning">
                            <?=(($match["ascore"]!="-1")?$match["ascore"]:"-")?>
                        </span>
                    </div>
                    <div class="col-md-5">
                        <a class="text-md-center list-group-item list-group-item-warning"><h5><?=$away["tname"]?></h5></a>
                        <div class="text-md-left">
                            <?foreach ($awayLast5Icon as $icon):?>
                                <?=$icon?>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 50px;">
                    <div class="col-md-12">
                        <table class="table text-md-center table-hover">
                            <thead>
                                <tr>
                                    <td class="text-md-center" colspan="4"><strong><?=$home["tname"]?></strong> Oynadığı Son 5 Maç</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?foreach ($homeLast5 as $mac):?>
                                <?
                                $lhome = $database->query("SELECT * FROM takimlar WHERE id=" . $mac["hteam"])->fetch(PDO::FETCH_ASSOC);
                                $laway = $database->query("SELECT * FROM takimlar WHERE id=" . $mac["ateam"])->fetch(PDO::FETCH_ASSOC);

                                ?>
                                <tr <?=($mac["id"]==$match["id"])?'style="background-color: #d9edf7"':""?>>
                                    <td><?=turkcetarih('j.M.Y',$mac["tarih"])?></td>
                                    <td><?=$lhome["tname"]?></td>
                                    <td><?=$mac["hscore"]?>-<?=$mac["ascore"]?></td>
                                    <td><?=$laway["tname"]?></td>
                                </tr>
                            <?endforeach;?>

                            </tbody>
                        </table>
                        <br>
                        <table class="table text-md-center table-hover">
                            <thead>
                            <tr>
                                <td class="text-md-center" colspan="4"><strong><?=$away["tname"]?></strong> Oydağını Son 5 Maç</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?foreach ($awayLast5 as $mac):?>
                                <?
                                $lhome = $database->query("SELECT * FROM takimlar WHERE id=" . $mac["hteam"])->fetch(PDO::FETCH_ASSOC);
                                $laway = $database->query("SELECT * FROM takimlar WHERE id=" . $mac["ateam"])->fetch(PDO::FETCH_ASSOC);

                                ?>
                                <tr <?=($mac["id"]==$match["id"])?'style="background-color: #d9edf7"':""?>>
                                    <td><?=turkcetarih('j.M.Y',$mac["tarih"])?></td>
                                    <td><?=$lhome["tname"]?></td>
                                    <td><?=$mac["hscore"]?>-<?=$mac["ascore"]?></td>
                                    <td><?=$laway["tname"]?></td>
                                </tr>
                            <?endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-outline-success">
                    <div class="card-header">
                        <h6><i class="fa fa-soccer-ball-o"></i> Maç Detayları</h6>
                    </div>
                    <div class="card-block text-md-center" style="padding: 10px">
                        <p class="text-md-center">
                        <dl class="row">
                            <dt class="col-sm-3">Tarih: </dt>
                            <dd class="col-sm-9"><?=turkcetarih('j.M.Y H.i',$match["tarih"])?></dd>
                            <dt class="col-sm-3">Saha </dt>
                            <dd class="col-sm-9"><?=$match["STAD"]?></dd>
                            <dt class="col-sm-3">Lig </dt>
                            <dd class="col-sm-9"><?=$lig["league_name"]?></dd>
                            <dt class="col-sm-3">Hafta </dt>
                            <dd class="col-sm-9"><?=$match["HAFTA"]?></dd>
                        </dl>

                        </p>
                    </div>
                </div>
                <div class="card card-outline-success">
                    <div class="card-header">
                        <h6><i class="fa fa-file-pdf-o"></i> Maç Raporu</h6>
                    </div>
                    <div class="card-block text-md-center" style="padding: 10px">
                        <?if($match["ACIKLAMA"]=="" || trim($match["ACIKLAMA"])=="YOK"){ ?>
                            <p class="text-md-center">
                                <img src="img/warning.png" style="width: 32px;height: 32px;"><br>
                                Bu maç ile ilgili bir rapor bulunmamaktadır.
                            </p>
                            <?}else{?>
                            <p class="text-md-center">
                                <?=stripslashes($match["ACIKLAMA"]);?>
                            </p>
                        <?}?>
                    </div>
                </div>

                <div class="card card-outline-success" style="position: relative" id="leagueTablePanel">
                    <div class="card-header">
                        <h6><i class="fa fa-calendar-check-o"></i> <?=$match["HAFTA"]?>. Hafta Puan Durumu</h6>
                    </div>
                    <div class="card-block">

                        <table class="table  table-striped table-sm">

                            <tbody id="leagueContent">
                            <?for($i=0; $i < count($table); $i++):?>
                                <tr <?=($table[$i]["Team"]==$home["tname"])?'style="background-color:#69a5f4"':""?> <?=($table[$i]["Team"]==$away["tname"])?'style="background-color:#ff005d"':""?>>
                                    <th scope="row"><?=($i+1)?></th>
                                    <td><?=$table[$i]["Team"]?></td>
                                    <td  class="text-md-right"><strong><?=$table[$i]["Pts"]?></strong></td>
                                </tr>
                            <?endfor;?>

                            </tbody>
                        </table>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
