<? include_once "_header_.php" ?>
<?
$database->query("UPDATE duyurular SET okunma=okunma+1 WHERE id=" . $_GET["id"]);
$duyuru = $database->query("SELECT * FROM duyurular WHERE id=" . $_GET["id"])->fetch(PDO::FETCH_ASSOC);
if(!is_array($duyuru)){
    header("location: index.html");
}
$baslik = strip_tags(stripslashes($duyuru["baslik"]));
$icerik = stripslashes($duyuru["duyuru"]);
$tarih = turkcetarih('j F Y',$duyuru["tarih"]);

?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-8 blogShort">
                <h1 class="blog-title"><?=$baslik?></h1>
                <span class="tag tag-primary"><i class="fa fa-calendar"></i> Yayım Tarihi: <?=$tarih?></span>
                <span  class="tag tag-success"><i class="fa fa-eye"></i> Okunma <?=$duyuru["okunma"]?></span>
                <button class="btn btn-sm" onclick="$.setFontSizeUp('article')"><i class="fa fa-font"></i> Yazı Büyüt</button>
                <button class="btn btn-sm" onclick="$.setFontSizeDown('article')"><i class="fa fa-font"></i> Yazı Küçült</button>
                <a target="_blank" href="print.php?action=d&id=<?=$duyuru["id"]?>" class="btn btn-sm btn-default"><i class="fa fa-print"></i> Yazdır</a>
                <hr>
                <?if($duyuru["resimgoster"]=="1"):?>
                <img style="width:300px;height: 250px;margin-right: 20px;margin-bottom: 0px" src="upload/haberler/<?=$duyuru["resim"]?>" alt="<?=$baslik?>" class="pull-left img-responsive thumb margin10 img-thumbnail">
                <?endif;?>
                <article id="article" style="text-align: justify">
                    <p>
                        <?
                        /*
                        $dom = new DOMDocument;
                        $dom->loadHTML(mb_convert_encoding($icerik, 'HTML-ENTITIES', 'UTF-8'));

                        $xpath = new DOMXPath($dom);
                        $nodes = $xpath->query('//*[@style]');
                        foreach ($nodes as $node) {
                            $node->removeAttribute('style');
                        }
                        echo $dom->saveHTML();*/
                        echo $icerik;
                        ?>
                    </p>
                </article>
                <div class="clearfix"></div>
                <?
                $path = "files/resimler/".seo($duyuru["baslik"]) . "/";
                $dokumanlar = scandir($path);
                if(count($dokumanlar)-2 >0) {?>
                <div class="card-outline-primary" style="margin-top: 10px;">
                    <h6 class="card-header "><i class="fa fa-picture-o"></i> Galeri</h6>
                        <?
                        foreach ($dokumanlar as $doc) {
                            if (strlen($doc) > 3) { ?>
                                <div class="col-md-3" style="margin-bottom: 5px;">
                                    <a class="fancybox" rel="group" href="files/resimler/<?=seo($duyuru["baslik"])?>/<?=$doc?>">
                                    <img class="img-fluid img-rounded" src="files/resimler/<?=seo($duyuru["baslik"])?>/<?=$doc?>"/>
                                    </a>
                                </div>
                            <?
                            }
                        }
                        ?>
                </div>

                    <?
                    }
                    ?>
                <div class="clearfix"></div>
                <div class="card card-outline-primary" style="margin-top: 30px;">
                    <h6 class="card-header "><i class="fa fa-newspaper-o"></i> Diğer Haberler</h6>
                    <div class="card-block">
                        <div class="row">
                            <ul class="media-list">
                                <? $haberListeler = $database->query("SELECT * FROM duyurular WHERE id<>".$duyuru["id"] . " ORDER BY tarih limit 0,3")->fetchAll(PDO::FETCH_ASSOC);?>
                                <?foreach ($haberListeler as $haber):?>
                                <li class="media">
                                    <div class="media-left">
                                        <a href="<?=$config["base"]?>haberler/<?=$haber["seo"]?>/<?=$haber["id"]?>">
                                            <img class="media-object" style="width: 75px;height: 75px;" src="upload/haberler/<?=$haber["resim"]?>" alt="<?=stripslashes($haber["baslik"])?>">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading"><a href="<?=$config["base"]?>haberler/<?=$haber["seo"]?>/<?=$haber["id"]?>">
                                                <? $baslikLen = strlen($haber["baslik"]);
                                                echo mb_substr(stripslashes($haber["baslik"]),0,55);
                                                if($baslikLen <= 55){
                                                    echo "... ";
                                                }
                                                ?>
                                            </a>
                                        </h5>
                                        <p>
                                            <? $aciklamaLen = strlen($haber["aciklama"]);?>
                                            <? echo mb_substr(stripslashes($haber["aciklama"]),0,55); ?>
                                            <?if($aciklamaLen > 0 && $aciklamaLen <= 165){?>
                                                <?echo "... "; ?>

                                            <?}?>
                                        </p>
                                    </div>
                                </li>
                                <?endforeach;?>

                            </ul>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-md-4">

                <div class="card card-outline-success">
                    <div class="card-header">
                        <h6><i class="fa fa-cloud"></i> Kırklareli 5 günlük Hava Tahmini</h6>
                    </div>
                    <ul class="list-group">
                        <img src="http://www.mgm.gov.tr/sunum/tahmin-show-2.aspx?m=KIRKLARELI&basla=0&bitir=5&rC=111&rZ=fff" class="img-fluid" alt="KIRKLARELİ" />
                    </ul>
                </div>
                <? include_once "widget/leagueTable/league_table.php";?>

            </div>
        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
