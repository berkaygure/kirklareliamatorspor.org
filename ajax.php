<?php
include_once "yonetim/config.php";

if($_GET["request"]=="fixture"){
    ?>
    <?
    $fikstur = $database->query("SELECT * FROM  maclar WHERE  LIG_ID=" . $_POST["lig"] . " and HAFTA=" . $_POST["hafta"])->fetchAll(PDO::FETCH_ASSOC);
    foreach ($fikstur as $f):
        $home = $database->query("SELECT tname FROM takimlar WHERE id=" . $f["hteam"])->fetch(PDO::FETCH_ASSOC);
        $away = $database->query("SELECT tname FROM takimlar WHERE id=" . $f["ateam"])->fetch(PDO::FETCH_ASSOC);
        ?>
        <tr>
            <td class="vert-align"><?=$home["tname"]?></td>
            <td class="vert-align">
                <? if($f["hsocre"]==-1 || $f["ascore"]==-1) {?>

                    <span class="tag tag-primary"><i class="fa fa-calendar"></i> <?=($f["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["tarih"])?></span><br>
                    <span class="tag tag-warning">-</span> -
                    <span class="tag tag-warning">-</span><br>
                    <?if(trim($f["STAD"])!="YOK"):?>
                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["STAD"]?></span><br>
                    <?endif;?>
                <? } else {?>
                    <span class="tag tag-default"><i class="fa fa-calendar"></i> <?=($f["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["tarih"])?></span><br>
                    <span class="tag tag-warning"><?=$f["hscore"]?></span> -
                    <span class="tag tag-warning"><?=$f["ascore"]?></span><br>
                    <?if(trim($f["STAD"])!="YOK"):?>
                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["STAD"]?></span><br>
                    <?endif;?>
                <? }?>
                <a href="<?=$config["base"]?>mac/<?=$f["id"]?>">Maç Detayı</a>

            </td>
            <td class="vert-align"><?=$away["tname"]?></td>
        </tr>
    <?endforeach;?>

    <?
}

if($_GET["request"]=="table") {
?>

    <?php

    $ligID = $_POST["lig"];
    $table = array();
    if($_GET["t"]=="genel"){
        $table = getLeagueTable($database,$ligID,$_POST["hafta"]);
    }
    if($_GET["t"]=="home"){
        $table = getLeagueTableHome($database,$ligID,$_POST["hafta"]);
    }
    if($_GET["t"]=="away"){
        $table = getLeagueTableAway($database,$ligID,$_POST["hafta"]);
    }
    $gecenHafta = false;
    if($_POST["hafta"] > 1){

        if($_GET["t"]=="genel"){
            $gecenHafta = getLeagueTable($database,$ligID,$_POST["hafta"]-1);
        }
        if($_GET["t"]=="home"){
            $gecenHafta = getLeagueTableHome($database,$ligID,$_POST["hafta"]-1);
        }
        if($_GET["t"]=="away"){
            $gecenHafta = getLeagueTableAway($database,$ligID,$_POST["hafta"]-1);
        }

    }
    ?>
    <?for($i=0; $i < count($table); $i++):?>
        <tr>
            <td class=" text-md-center">
                <?if($gecenHafta):?>
                    <? $last = getTeamFromTable($gecenHafta,$table[$i]["Team"]);?>
                    <?if($last > $i):?>
                        <i title="Geçen Hafta <?=($last+1)?>" style="color: #2ecc55;" class="fa fa-arrow-up"></i>
                    <? elseif($last < $i):?>
                        <i title="Geçen Hafta <?=($last+1)?>" style="color: #cd0000;" class="fa fa-arrow-down"></i>
                    <? else:?>
                        <i title="Geçen Hafta <?=($last+1)?>" class="fa fa-circle"></i>
                    <?endif?>
                <? else:?>
                    <i class="fa fa-circle"></i>
                <?endif;?>
            </td>
            <td>
                <h5><?=($i+1)?></h5>
            </td>
            <td><?=$table[$i]["Team"]?></td>
            <td class=" text-md-center"><?=$table[$i]["P"]?></td>
            <td class=" text-md-center"><?=$table[$i]["W"]?></td>
            <td class=" text-md-center"><?=$table[$i]["D"]?></td>
            <td class=" text-md-center"><?=$table[$i]["L"]?></td>
            <td class=" text-md-center"><?=$table[$i]["F"]?></td>
            <td class=" text-md-center"><?=$table[$i]["A"]?></td>
            <td class=" text-md-center"><?=$table[$i]["GD"]?></td>
            <td class=" text-md-center"><strong><?=$table[$i]["Pts"]?></strong></td>
        </tr>
    <?endfor;?>
<?
}
if($_GET["request"]=="arsivTable"){
$ligID = $_POST["lig"];
$table = array();
if($_GET["t"]=="genel"){
    $table = getLeagueTableArchive($database,$ligID,$_POST["hafta"]);
}
if($_GET["t"]=="home"){
    $table = getLeagueTableHomeArchive($database,$ligID,$_POST["hafta"]);
}
if($_GET["t"]=="away"){
    $table = getLeagueTableAwayArchive($database,$ligID,$_POST["hafta"]);
}
$gecenHafta = false;
if($_POST["hafta"] > 1){

    if($_GET["t"]=="genel"){
        $gecenHafta = getLeagueTableArchive($database,$ligID,$_POST["hafta"]-1);
    }
    if($_GET["t"]=="home"){
        $gecenHafta = getLeagueTableHomeArchive($database,$ligID,$_POST["hafta"]-1);
    }
    if($_GET["t"]=="away"){
        $gecenHafta = getLeagueTableAwayArchive($database,$ligID,$_POST["hafta"]-1);
    }

}
?>
<?for($i=0; $i < count($table); $i++):?>
    <tr>
        <td class=" text-md-center">
            <?if($gecenHafta):?>
                <? $last = getTeamFromTable($gecenHafta,$table[$i]["Team"]);?>
                <?if($last > $i):?>
                    <i title="Geçen Hafta <?=($last+1)?>" style="color: #2ecc55;" class="fa fa-arrow-up"></i>
                <? elseif($last < $i):?>
                    <i title="Geçen Hafta <?=($last+1)?>" style="color: #cd0000;" class="fa fa-arrow-down"></i>
                <? else:?>
                    <i title="Geçen Hafta <?=($last+1)?>" class="fa fa-circle"></i>
                <?endif?>
            <? else:?>
                <i class="fa fa-circle"></i>
            <?endif;?>
        </td>
        <td>
            <h5><?=($i+1)?></h5>
        </td>
        <td><?=$table[$i]["Team"]?></td>
        <td class=" text-md-center"><?=$table[$i]["P"]?></td>
        <td class=" text-md-center"><?=$table[$i]["W"]?></td>
        <td class=" text-md-center"><?=$table[$i]["D"]?></td>
        <td class=" text-md-center"><?=$table[$i]["L"]?></td>
        <td class=" text-md-center"><?=$table[$i]["F"]?></td>
        <td class=" text-md-center"><?=$table[$i]["A"]?></td>
        <td class=" text-md-center"><?=$table[$i]["GD"]?></td>
        <td class=" text-md-center"><strong><?=$table[$i]["Pts"]?></strong></td>
    </tr>
<?endfor;

}

if($_GET["request"]=="arsivFixture"){
    ?>
    <?
    $fikstur = $database->query("SELECT * FROM  table_fixture WHERE  LEAGUE_ID=" .$_POST["lig"] . " and WEEK_NO=" . $_POST["hafta"])->fetchAll(PDO::FETCH_ASSOC);
    foreach ($fikstur as $f):
        $home =$database->query("SELECT team_name FROM table_team WHERE ID=" . $f["HOME"])->fetch(PDO::FETCH_ASSOC);
        $away =$database->query("SELECT team_name FROM table_team WHERE ID=" . $f["AWAY"])->fetch(PDO::FETCH_ASSOC);
        ?>
        <tr>
            <td class="vert-align"><?=$home["team_name"]?></td>
            <td class="vert-align">
                <? if($f["H_GOAL"]==-1 || $f["A_GOAL"]==-1) {?>

                    <span class="tag tag-primary"><i class="fa fa-calendar"></i> <?=($f["MATCH_DATE"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["MATCH_DATE"])?></span><br>
                    <span class="tag tag-warning">-</span> -
                    <span class="tag tag-warning">-</span><br>
                    <?if(trim($f["MATCH_STAD"])!="YOK"):?>
                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["MATCH_STAD"]?></span><br>
                    <?endif;?>
                <? } else {?>
                    <span class="tag tag-default"><i class="fa fa-calendar"></i> <?=($f["MATCH_DATE"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$f["MATCH_DATE"])?></span><br>
                    <span class="tag tag-warning"><?=$f["H_GOAL"]?></span> -
                    <span class="tag tag-warning"><?=$f["A_GOAL"]?></span><br>
                    <?if(trim($f["STAD"])!="YOK"):?>
                        <span class="tag tag-default"><i class="fa fa-soccer-ball-o"></i> <?=$f["MATCH_STAD"]?></span><br>
                    <?endif;?>
                <? }?>
                <a href="<?=$config["base"]?>mac-arsiv/<?=$f["ID"]?>">Maç Detayı</a>

            </td>
            <td class="vert-align"><?=$away["team_name"]?></td>
        </tr>
    <?endforeach;?>

    <?
}

if($_GET["request"]=="sezon"){
        header('Content-Type: application/json');
        $type = "n";
        if(!in_array($_POST["sezon"],["16/17", "17/18", "18/19"])){
            $type = "a";
            $ligler = $database->query("SELECT * FROM table_leagues WHERE league_season='".$_POST["sezon"]."' ORDER BY ID ASC")->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $ligler = $database->query("SELECT * FROM leagues WHERE sezon='".$_POST["sezon"]."' ORDER BY ID ASC")->fetchAll(PDO::FETCH_ASSOC);
        }
        echo json_encode(
            [
                "ligler" => $ligler,
                "tip"  => $type
            ]
        );

}
