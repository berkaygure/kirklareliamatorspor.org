
$(function() {
    // Breaking News
    $("#scroller").simplyScroll();

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    //back to top
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
                scrollTop: 0 ,
            }, scroll_top_duration
        );
    });

        /// media carousel
    $('#media').carousel({
        pause: true,
        interval: false,
    });


    $.setFontSizeUp = function (elementID) {
        var size = $("#" + elementID).css("font-size").replace('px', '');
        size++;
        $("#" + elementID).css("font-size",size + "px");
    }
    $.setFontSizeDown = function (elementID) {
        var size = $("#" + elementID).css("font-size").replace('px', '');
        size--;
        $("#" + elementID).css("font-size",size + "px");
    }


    $("#fiksturHafta").change(function () {
        var hafta = this.value;
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");
        $('#textHafta').text(hafta);
        $('#textHaftaTable').text(hafta);

        $.ajax({
            type:"post",
            url:"ajax.php?request=fixture",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {
                $("#fixtureContent").html(response);
                $.ajax({
                    type:"post",
                    url:"ajax.php?request=table&t=genel",
                    data:"lig="+ligid+"&hafta="+hafta,
                    success: function (response) {
                        $('#leagueOverlay').fadeOut("slow");
                        $("#tableContent").html(response);
                    }
                })

            }
        })

    });

    $("#arshivefiksturHafta").change(function () {
        var hafta = this.value;
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");
        $('#textHafta').text(hafta);
        $('#textHaftaTable').text(hafta);

        $.ajax({
            type:"post",
            url:"ajax.php?request=arsivFixture",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {
                $("#fixtureContent").html(response);
                $.ajax({
                    type:"post",
                    url:"ajax.php?request=arsivTable&t=genel",
                    data:"lig="+ligid+"&hafta="+hafta,
                    success: function (response) {
                        $('#leagueOverlay').fadeOut("slow");
                        $("#tableContent").html(response);
                    }
                })

            }
        })

    });

    $("#tableHafta").change(function () {
        var hafta = this.value;
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");
        $('#textHaftaTable').text(hafta);

        $.ajax({
            type:"post",
            url:"ajax.php?request=table&t=genel",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {
                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        })

    });

    $("#genel").click(function () {


        var hafta = $("#fiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=table&t=genel",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").addClass("tag-success").removeClass("tag-info");
                $("#home span").removeClass("tag-success").addClass("tag-info");
                $("#away span").removeClass("tag-success").addClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });

    });
    $("#away").click(function () {


        var hafta = $("#fiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=table&t=away",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").removeClass("tag-success").addClass("tag-info");
                $("#home span").removeClass("tag-success").addClass("tag-info");
                $("#away span").addClass("tag-success").removeClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });

    });

    $("#home").click(function () {


        var hafta = $("#fiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=table&t=home",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").removeClass("tag-success").addClass("tag-info");
                $("#home span").addClass("tag-success").removeClass("tag-info");
                $("#away span").removeClass("tag-success").addClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });
    });


    ///

    $("#a_genel").click(function () {


        var hafta = $("#arshivefiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=arsivTable&t=genel",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").addClass("tag-success").removeClass("tag-info");
                $("#home span").removeClass("tag-success").addClass("tag-info");
                $("#away span").removeClass("tag-success").addClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });

    });
    $("#a_away").click(function () {


        var hafta = $("#arshivefiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=arsivTable&t=away",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").removeClass("tag-success").addClass("tag-info");
                $("#home span").removeClass("tag-success").addClass("tag-info");
                $("#away span").addClass("tag-success").removeClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });

    });

    $("#a_home").click(function () {


        var hafta = $("#arshivefiksturHafta").val();
        var ligid = $("#ligId").val();
        $('#leagueOverlay').css("display","block");

        $.ajax({
            type:"post",
            url:"ajax.php?request=arsivTable&t=home",
            data:"lig="+ligid+"&hafta="+hafta,
            success: function (response) {

                $("#genel span").removeClass("tag-success").addClass("tag-info");
                $("#home span").addClass("tag-success").removeClass("tag-info");
                $("#away span").removeClass("tag-success").addClass("tag-info");

                $('#leagueOverlay').fadeOut("slow");
                $("#tableContent").html(response);
            }
        });
    });

    $("#printCurrent").click(function () {
        var ligId = $("#ligId").val();
        var hafta = $("#fiksturHafta").val();
        window.open("print.php?action=fixture&lig=" + ligId+"&hafta="+hafta);

    });

    $("#printWithTable").click(function () {
        var ligId = $("#ligId").val();
        var hafta = $("#fiksturHafta").val();
        window.open("print.php?action=table&lig=" + ligId + "&hafta=" + hafta, '_blank');
    });

    $("#printAll").click(function () {
        var ligId = $("#ligId").val();
        window.open("print.php?action=fixtureAll&lig=" + ligId);
    });


    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });


    if($("#arsiv_select_ligler").length > 0) {
        $("#arsiv_select_ligler").change(function (e) {
            var s = this.value;


            $.ajax({
                type: "post",
                url: "ajax.php?request=sezon",
                data: "sezon=" + s,
                success: function (msg) {
                    $("#lig_list").html("");
                    msg.ligler.forEach(function (e) {
                        if(msg.tip == 'a'){
                            $('#lig_list').append('<li><a href="archive.php?id=' + e.ID + '">' + e.league_name + '</a></li>');
                        }else{
                            $('#lig_list').append('<li><a href="leagues.php?id=' + e.ID + '">' + e.league_name + '</a></li>');

                        }
                    })
                }
            })
        });
        $("#arsiv_select_ligler").change();

    }


    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if(isAndroid) {
        $(".open-popup").fullScreenPopup({
            bgColor: '#458f1f'
        });
        $(".open-popup").click();
    }

});