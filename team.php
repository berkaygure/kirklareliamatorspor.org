<? include_once "_header_.php" ?>
<?

?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-12 ">
                <h4 class="blog-title">Üye Kulüplerimiz</h4>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-md-center">
                <nav>
                    <ul class="pagination pagination-sm">
                        <li class="page-item <?=(!isset($_GET["q"])?"active":"")?>"><a class="page-link" href="<?=$config["base"]?>kulupler.html">Tümü</a></li>
                        <? $harfler = array("A","B","C","Ç","D","E","F","G","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z");?>
                        <?foreach ($harfler as $harf):?>
                            <li class="page-item <?=(($_GET["q"]==$harf)?"active":"")?>"><a class="page-link" href="<?=$config["base"]?>kulupler.html?q=<?=$harf?>"><?=$harf?></a></li>
                        <?endforeach;?>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row" style="margin-top: 0px;">
            <? if(isset($_GET["q"])) {
                $takimlar = $database->query("SELECT * FROM kulupler WHERE ad like '" . $_GET["q"] . "%'");
            }else{
                $takimlar = $database->query("SELECT * FROM kulupler");

            }?>
            <?foreach ($takimlar as $takim):?>
            <div class="col-md-4" style="margin-bottom: 5px;">
                <a href="" class="list-group-item">
                    <img src="img/boslogo.png" style="width:32px;height: 32px;"/>
                    <strong style="color:#333;"><?=$takim["ad"]?></strong>
                 </a>
            </div>
            <?endforeach;?>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
