<?
include_once  "yonetim/config.php";

?>
<table class="table table-striped table-sm">
    <thead>
    <tr style="font-size:11px;">
        <th></th>
        <th></th>
        <th></th>
        <th class=" text-md-center">O</th>
        <th class=" text-md-center">G</th>
        <th class=" text-md-tcenter">B</th>
        <th class=" text-md-center">M</th>
        <th class=" text-md-center">A</th>
        <th class=" text-md-center">Y</th>
        <th class=" text-md-center">AV</th>
        <th class=" text-md-center">P</th>
    </tr>
    </thead>
    <tbody id="tableContent">
    <?php
    $ligID = 133;
    $table = getLeagueTableNew($database,$ligID,1);
    $gecenHafta = false;
    if(0){
        $gecenHafta = getLeagueTable($database,$ligID,13);
    }

/*
    for($i=0; $i < count($table); $i++){
        $table[$i]["Pts"] = ($table[$i]["Pts"]+$table[$i]["ceza"]);
    }*/

/*
    $sort = array();
    foreach($table as $k=>$v) {
        $sort['Pts'][$k] = $v['Pts'];
        $sort['GD'][$k] = $v['GD'];
    }
    # sort by event_type desc and then title asc
    array_multisort($sort['Pts'], SORT_DESC, $sort['GD'], SORT_DESC,$table);*/
    ?>
    <?for($i=0; $i < count($table); $i++):?>
        <tr>
            <td class=" text-md-center">
                <?if($gecenHafta):?>
                    <? $last = getTeamFromTable($gecenHafta,$table[$i]["Team"]);?>
                    <?if($last > $i):?>
                        <i title="Geçen Hafta <?=($last+1)?>" style="color: #2ecc55;" class="fa fa-arrow-up">u</i>
                    <? elseif($last < $i):?>
                        <i title="Geçen Hafta <?=($last+1)?>" style="color: #cd0000;" class="fa fa-arrow-down">d</i>
                    <? else:?>
                        <i title="Geçen Hafta <?=($last+1)?>" class="fa fa-circle">b</i>
                    <?endif?>
                <? else:?>
                    <i class="fa fa-circle"></i>
                <?endif;?>
            </td>
            <td>
                <h5><?=($i+1)?></h5>
            </td>
            <td><?=$table[$i]["Team"] .  " " . $table[$i]["Test"]?></td>
            <td class=" text-md-center"><?=$table[$i]["P"]?></td>
            <td class=" text-md-center"><?=$table[$i]["W"]?></td>
            <td class=" text-md-center"><?=$table[$i]["D"]?></td>
            <td class=" text-md-center"><?=$table[$i]["L"]?></td>
            <td class=" text-md-center"><?=$table[$i]["F"]?></td>
            <td class=" text-md-center"><?=$table[$i]["A"]?></td>
            <td class=" text-md-center"><?=$table[$i]["GD"]?></td>
            <td class=" text-md-center"><strong><?=($table[$i]["Pts"])?></strong></td>
        </tr>
    <?endfor;?>
    </tbody>
</table>
