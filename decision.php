<? include_once "_header_.php" ?>
<?
$baslik = "Tertip Komitesi Kararları";
$link = "tertip-komitesi-kararlari/";
$tip = 0;
if($_GET["tip"]=="disiplin"){
    $baslik = "Disiplin Kurulu Kararları";
    $link = "disiplin-kurulu-kararlari/";
    $tip = 1;
}else{
    $baslik = "Tertip Komitesi Kararları";
    $link = "tertip-komitesi-kararlari/";
    $tip = 0;

}
?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-12 blogShort">
                <h4 class="blog-title"><?=$baslik?></h4>

                <article id="article" style="text-align: justify">
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>Karar Başlığı</th>
                                <th>Karar Tarihi</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?
                            $karar = $database->query("SELECT * FROM kararlar WHERE tip=".$tip." ORDER by tarih")->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($karar as $k):?>
                            <tr>
                                <td><?=$k["baslik"]?></td>
                                <td><?=$k["tarih"]?></td>
                                <td><a href="<?=$cofig["base"].$link.$k["seo"]."-" . $k["id"]?>.html" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i> Oku</a> </td>
                            </tr>
                            <?
                            endforeach;
                            ?>



                        </tbody>
                    </table>

                </article>
                <div class="clearfix"></div>

            </div>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
