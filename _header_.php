<? include_once "yonetim/config.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ana Sayfa - Kırklareli Amatör Spor Kulüpleri Federasyonu</title>
    <base href="<?=$config["base"]?>" />
    <link rel="icon" type="image/png" href="img/favicon.png" sizes="32x32">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="font/css/font-awesome.min.css">
    <link rel="stylesheet" href="library/jquery.simplyscroll.css"/>
    <link rel="stylesheet" href="css/slider.css"/>
    <link rel="stylesheet" type="text/css"  href="//fonts.googleapis.com/css?family=Cutive+Mono">
    <link rel="stylesheet" type="text/css" href="library/fancybox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="library/fancybox/source/helpers/jquery.fancybox-buttons.css"/>
    <link rel="stylesheet" type="text/css" href="library/fancybox/source/helpers/jquery.fancybox-thumbs.css"/>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
    <script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script><!-- Tether for Bootstrap -->

    <script src="js/bootstrap.min.js"></script>
    <script src="library/jquery.simplyscroll.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/jquery.fullscreen-popup.min.js"></script>

    <script src="js/script.js"></script>
    <script src="widget/leagueTable/leagueTable.js"></script>
    <script src="library/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="library/fancybox/source/jquery.fancybox.js"></script>
    <script src="library/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="library/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script src="library/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script src="library/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
</head>
<script>
    $(function () {
        $(".fancybox").fancybox();

        $("#myModal").modal('toggle'); 
    })
</script>
<body>

<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                Kırklareli Amatör Spor Kulüpleri Federasyonu
            </div>
            <div class="col-md-6 text-md-right text-lg-right text-xs-right text-sm-right text-xl-right">
                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-xs btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-phone"></i> Bize Ulaşın
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="tel:+902882121911"><i class="fa fa-phone"></i> (0288) 212 1911</a>
                        <a class="dropdown-item" href="mailto:askf86@hotmail.com"><i class="fa fa-envelope"></i> askf86@hotmail.com</a>
                        <a class="dropdown-item" href=""><i class="fa fa-facebook-official"></i> Facebook</a>
                        <a class="dropdown-item" href=""><i class="fa fa-twitter-square"></i> Twitter</a>
                        <a class="dropdown-item" href=""><i class="fa fa-book"></i> İletişim Bilgilerimiz</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header">
    <div class="container">
        <div class="hidden-xs-down" id="banner">
            <img style="height: 200px;" class="img-fluid" src="img/banner/banner.gif">
        </div>
    </div>
    <nav class="menu navbar bg-danger navbar-dark">
        <div class="container">
            <!-- Toggle Button -->
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navcontent">
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-toggleable-xs" id="navcontent">
                <!--<a class="navbar-brand" href="#">Navbar</a>-->
                <ul class="nav navbar-nav">
                    <li class="menu nav-item active"><a class="nav-link" href="#"><i class="fa fa-home"></i> </a></li>
                    <?
                    $menuler = $database->query("SELECT * FROM menuler WHERE ustid=0 ORDER BY sira")->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($menuler as $menu) { ?>
                        <?
                        if($menu["link"]=="[ligler]"){
                            ?>
                            <li class="menu nav-item dropdown"><a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><?=$menu["baslik"]?></a>
                                <div class="dropdown-menu" aria-labelledby="Preview">
                                    <? $ligler = $database->query("SELECT * FROM leagues WHERE sezon='".$config["sezon"]."' ORDER BY ID ASC")->fetchAll(PDO::FETCH_ASSOC);?>
                                    <? foreach ($ligler as $lig) {?>

                                       <a class="dropdown-item" href="<?=$config["base"] . "ligler/" . $lig["seo"] . "-" . $lig["ID"]?>.html"><?=$lig["league_name"]?></a>
                                    <? } ?>
                                    <a class="dropdown-item" href="old_leagues.php">Arşiv</a>

                                </div>
                            </li>
                            <?
                            continue;
                        }
                        // altmenu
                        $altmenu = $database->query("SELECT * FROM menuler WHERE ustid=".$menu["id"]." ORDER BY sira")->fetchAll(PDO::FETCH_ASSOC);
                        if(count($altmenu) > 0){ ?>
                            <li class="menu nav-item dropdown"><a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><?=$menu["baslik"]?></a>
                                <div class="dropdown-menu" aria-labelledby="Preview">
                                    <? foreach ($altmenu as $alt) {?>
                                        <?
                                        if($alt["sayfa"] != 0){
                                            $sayfaData = $database->query("SELECT seo,id FROM sayfalar WHERE id=" . $alt["sayfa"])->fetchAll(PDO::FETCH_ASSOC);
                                            $link =  $config["base"]."sayfa/".$sayfaData[0]["seo"]."-".$sayfaData[0]["id"].".html";
                                        }else{
                                            $link = $alt["link"];
                                        }
                                        ?>
                                        <a class="dropdown-item" href="<?=$link?>"><?=$alt["baslik"]?></a>
                                    <? } ?>
                                </div>
                            </li>
                        <? }else{ ?>
                            <?
                            if($menu["sayfa"] != 0){
                                $sayfaData = $database->query("SELECT seo,id FROM sayfalar WHERE id=" . $menu["sayfa"])->fetchAll(PDO::FETCH_ASSOC);
                                $link =   $config["base"]."sayfa/".$sayfaData[0]["seo"]."-".$sayfaData[0]["id"].".html";
                            }else{
                                $link = $menu["link"];
                            }

                            ?>
                            <li class="menu nav-item "><a class="nav-link" href="<?=$link?>"><?=$menu["baslik"]?></a></li>
                        <? }?>
                    <? } ?>

                </ul>

            </div>
        </div>
    </nav>
    <div id="intr" class="container">
        <div class="row-fluid">
            <div class="brnews col-md-12">
                <h3><i class="fa fa-bullhorn"></i> Son Duyurular</h3>
                <ul id="scroller">
                    <?
                    $duyurular = $database->query("SELECT * FROM duyurular ORDER by tarih DESC LIMIT 0,10 ")->fetchAll(PDO::FETCH_ASSOC);
                    foreach($duyurular as $duyuru): ?>
                    <li><p><a rel="bookmark" href="<?=$config["base"]?>haberler/<?=$duyuru["seo"]?>/<?=$duyuru["id"]?>"><?=$duyuru["baslik"]?></a></p></li>
                    <?endforeach;?>

                </ul>
            </div>

        </div>
    </div>
</div>
