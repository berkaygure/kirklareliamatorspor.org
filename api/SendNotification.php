<?php

/**
 * Created by PhpStorm.
 * User: bgure
 * Date: 21.11.2016
 * Time: 20:44
 */
class SendNotification
{
    public static function send($key,$title,$body,$activity,$data = array()){

        $firebase_url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'to' => $key,
            'notification' => array('title' => $title, 'body' => $body, "sound"=> "default")

        );

        if(count($data)>0){
            $fields["data"] = $data;
        }

        $headers = array(
            'Authorization:key=' . "AIzaSyDa6va0NVORQNNUDAo03MPdcJpyDWxX7PM",
            'Content-Type:application/json'
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $firebase_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        curl_close($ch);
    }
}
