<?php
header('Content-Type: application/json');
include "../yonetim/config.php";
define("SEZON", $config["sezon"]);
class Api {

    const APP_ID = "berkaygure";
    const SECRET_KEY = "amatorspor";
    private $database = "";
    public function __construct($id, $secret, $func,PDO $db)
    {
        $this->database = $db;
        //if($id == self::APP_ID && $secret == self::SECRET_KEY){

            switch ($func){
                case "all":
                case "":
                    $all = [
                        "duyurular" => $this->duyurular(),
                        "haftalik" => $this->haftalik(),
                        "getLeaguesTitle" => $this->getLeaguesTitle()
                    ];
                    echo json_encode($all);
                break;
                case "match_details":

                    echo json_encode($this->match_details($_GET["id"]));
                    break;

                case "getFixture":
                    echo json_encode($this->getFixture($_GET["lig"],$_GET["hafta"]));

                    break;
                case "getLeagueTable":
                    echo json_encode($this->getLeagueTable($_GET["lig"],$_GET["hafta"]));

                    break;
                case "getReferee":
                    echo json_encode($this->getReferee());

                    break;
                case "getDocuments":
                    echo json_encode($this->getDocuments());

                    break;
                case "getMatchesWeekly":
                    echo json_encode($this->getMusabaka());

                    break;
                case "getKarar":
                    echo json_encode($this->getKarar($_GET["type"]));

                    break;
                case "addUser":
                    $this->addToUsers();
                    break;
                default:
                    echo json_encode($this->call($func));
                    break;

            }

       // }else{
          //  echo json_encode(array("test"=>"t"));
       // }
    }

    public function addToUsers(){
        $nRows = $this->database->query('select count(*) from mobile_users WHERE regKey="'.$_GET["regKey"].'"')->fetchColumn();
        if($nRows <= 0){
            $this->database->query("INSERT INTO mobile_users(regKey) VALUES('".$_GET["regKey"]."')")->execute();

        }


    }

    public function getKarar($type){

        $karar = $this->database->query("SELECT * FROM kararlar WHERE tip=".$type." ORDER by tarih")->fetchAll(PDO::FETCH_ASSOC);
        return $karar;


    }

    public function getReferee(){
        $retVal = array();
        $path = "../files/hakem_ve_gozlemci/";
        $dokumanlar = scandir($path);
        if(count($dokumanlar)-2 > 0){
            foreach ($dokumanlar as $doc) {
                if (strlen($doc) > 3) {

                    $file = array();
                    $file["text"] = $doc;
                    $file["type"] = "hakem";
                    $file["size"] = file_size($path . $doc,true);
                    $file["date"] = gen_tarih($path . $doc);
                    $file["link"] = "http://kirklareliamatorspor.org/download.php?t=hakem&file=".base64_encode($doc);
                    $retVal[]=$file;
                }
            }
        }

        return $retVal;


    }

    public function getDocuments(){
        $retVal = array();
        $path = "../files/dokumanlar/";
        $dokumanlar = scandir($path);
        if(count($dokumanlar)-2 > 0){
            foreach ($dokumanlar as $doc) {
                if (strlen($doc) > 3) {

                    $file = array();
                    $file["text"] = $doc;
                    $file["type"] = "dokumanlar";
                    $file["size"] = file_size($path . $doc,true);
                    $file["date"] = gen_tarih($path . $doc);
                    $file["link"] = "http://kirklareliamatorspor.org/download.php?file=".base64_encode($doc);
                    $retVal[]=$file;
                }
            }
        }

        return $retVal;


    }
    public function getMusabaka(){
        $retVal = array();
        $path = "../files/haftalikmusabaka/";
        $dokumanlar = scandir($path);
        if(count($dokumanlar)-2 > 0){
            foreach ($dokumanlar as $doc) {
                if (strlen($doc) > 3) {

                    $file = array();
                    $file["text"] = $doc;
                    $file["type"] = "musabaka";
                    $file["size"] = file_size($path . $doc,true);
                    $file["date"] = gen_tarih($path . $doc);
                    $file["link"] = "http://kirklareliamatorspor.org/download.php?t=musabaka&file=".base64_encode($doc);
                    $retVal[]=$file;
                }
            }
        }

        return $retVal;


    }

    public function call($func){
        $this->$func();
    }

    private function hello(){
        echo json_encode(array("hello"=>"its me"));
    }

    public function match_details($id){
        $data = array();

        $match = $this->database->query("SELECT * FROM maclar WHERE id=" . $id)->fetch(PDO::FETCH_ASSOC);
        $lig = $this->database->query("SELECT * FROM leagues WHERE ID=" . $match["LIG_ID"])->fetch(PDO::FETCH_ASSOC);
        $home = $this->database->query("SELECT * FROM takimlar WHERE id=" . $match["hteam"])->fetch(PDO::FETCH_ASSOC);
        $away = $this->database->query("SELECT * FROM takimlar WHERE id=" . $match["ateam"])->fetch(PDO::FETCH_ASSOC);
        $homeLast5 = $this->database->query("SELECT * FROM maclar WHERE (hteam=" . $home["id"] . " or ateam=" . $home["id"] . ") and (hscore <> -1 and ascore <> -1) ORDER BY tarih DESC LIMIT 0,5")->fetchAll(PDO::FETCH_ASSOC);
        $awayLast5 = $this->database->query("SELECT * FROM maclar WHERE (hteam=" . $away["id"] . " or ateam=" . $away["id"] . ") and (hscore <> -1 and ascore <> -1) ORDER BY tarih DESC LIMIT 0,5")->fetchAll(PDO::FETCH_ASSOC);
        $data["id"] = $match["id"];
        $data["home"] = $home["tname"];
        $data["away"] = $away["tname"];
        $data["leagueName"] = $lig["league_name"];

        if($match["hscore"]==-1 || $match["ascore"]==-1) {
            $data["score"] = turkcetarih('j.M.Y H.i',$match["tarih"]);

        }else{
            $data["score"] = $match["hscore"] . "-" . $match["ascore"];

        }
        $data["match_date"] = turkcetarih('j.M.Y H.i',$match["tarih"]);

        $data["stadium"] = $match["STAD"];
        $data["report"] = $match["ACIKLAMA"];
        $data["event_start"] = (strtotime($match["tarih"]) * 1000);
        $new_time = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($match["tarih"])));
        $data["event_end"] = (strtotime($new_time)*1000);
        $homeLast = array();
        foreach ($homeLast5 as $h) {
            $lhome = $this->database->query("SELECT * FROM takimlar WHERE id=" . $h["hteam"])->fetch(PDO::FETCH_ASSOC);
            $laway = $this->database->query("SELECT * FROM takimlar WHERE id=" . $h["ateam"])->fetch(PDO::FETCH_ASSOC);
            $mac = array();
            $mac["home"] = $lhome["tname"];
            $mac["away"] = $laway["tname"];
            $mac["score"] = $h["hscore"] . "-" . $h["ascore"];

            if($h["hteam"] == $home["id"]){
                $mac["tname"] = $laway["tname"] . " (Ev)";

                if($h["hscore"] > $h["ascore"]){
                    $mac["result"] = "G";
                }else if($h["hscore"] < $h["ascore"]){
                    $mac["result"] = "M";

                }else{
                    $mac["result"] = "B";
                }
            }else{
                $mac["tname"] = $lhome["tname"] . " (Mis.)";

                if($h["hscore"] > $h["ascore"]){
                    $mac["result"] = "M";
                }else if($h["hscore"] < $h["ascore"]){
                    $mac["result"] = "G";

                }else{
                    $mac["result"] = "B";
                }
            }
            $homeLast[]=$mac;
        }
        $awayLast = array();
        foreach ($awayLast5 as $h) {
            $lhome = $this->database->query("SELECT * FROM takimlar WHERE id=" . $h["hteam"])->fetch(PDO::FETCH_ASSOC);
            $laway = $this->database->query("SELECT * FROM takimlar WHERE id=" . $h["ateam"])->fetch(PDO::FETCH_ASSOC);
            $mac = array();
            $mac["home"] = $lhome["tname"];
            $mac["away"] = $laway["tname"];
            $mac["score"] = $h["hscore"] . "-" . $h["ascore"];

            if($h["hteam"] == $away["id"]){
                $mac["tname"] = $laway["tname"] . " (Ev)";;

                if($h["hscore"] > $h["ascore"]){
                    $mac["result"] = "G";
                }else if($h["hscore"] < $h["ascore"]){
                    $mac["result"] = "M";

                }else{
                    $mac["result"] = "B";
                }
            }else{
                $mac["tname"] = $lhome["tname"] . " (Mis.)";

                if($h["hscore"] > $h["ascore"]){
                    $mac["result"] = "M";
                }else if($h["hscore"] < $h["ascore"]){
                    $mac["result"] = "G";

                }else{
                    $mac["result"] = "B";
                }
            }
            $awayLast[]=$mac;
        }

        $data["homeLast5"] = $homeLast;
        $data["awayLast5"] = $awayLast;

        $data["leagueTable"] = getLeagueTable($this->database,$match["LIG_ID"],$match["HAFTA"]);


        return $data;
    }

    public function duyurular()
    {
        $i = 0;
        $data = array();
        $duyurular = $this->database->query("SELECT * FROM duyurular WHERE manset=1 ORDER by tarih DESC LIMIT 0,5 ")->fetchAll(PDO::FETCH_ASSOC);
        foreach ($duyurular as $duyuru){
            $d = array();
            $d["id"] = $duyuru["id"];
            $d["title"] = $duyuru["baslik"];
            $d["content"] = $duyuru["duyuru"];
            $d["date"] = turkcetarih('j F Y',$duyuru["tarih"]);
            $d["image"] = "upload/haberler/" . $duyuru["resim"];
            $data[]=$d;
        }

        return $data;
    }

    public function haftalik()
    {
        $i = 0;
        $retData = array();

        $datas = $this->database->query("SELECT * FROM maclar WHERE tarih >= DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY) and tarih <= DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY), INTERVAL 7 DAY) ORDER BY LIG_ID ASC")->fetchAll(PDO::FETCH_ASSOC);
        foreach ($datas as $data) {
            $lig = $this->database->query("SELECT league_name FROM leagues WHERE ID=" . $data["LIG_ID"])->fetch(PDO::FETCH_ASSOC);
            $home = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
            $away = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC);

            $mac = array();
            $mac["id"] = $data["id"];
            $mac["home"] = $home["tname"];
            $mac["away"] = $away["tname"];
            if($data["hsocre"]==-1 || $data["ascore"]==-1) {
                $mac["score"] = "vs";

            }else{
                $mac["score"] = $data["hscore"] . "-" . $data["ascore"];

            }
            $retData[]=$mac;
        }
        return $retData;
    }

    public function getFixture($league, $week)
    {
        $retData = array();

        $fikstur = $this->database->query("SELECT * FROM  maclar WHERE  LIG_ID=" . $league . " and HAFTA=" . $week)->fetchAll(PDO::FETCH_ASSOC);
        foreach ($fikstur as $f) {
            $home = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $f["hteam"])->fetch(PDO::FETCH_ASSOC);
            $away = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $f["ateam"])->fetch(PDO::FETCH_ASSOC);
            $mac = array();
            $mac["id"] = $f["id"];
            $mac["home"] = $home["tname"];
            $mac["away"] = $away["tname"];
            if($f["hsocre"]==-1 || $f["ascore"]==-1) {
                $mac["score"] = "vs";

            }else{
                $mac["score"] = $f["hscore"] . "-" . $f["ascore"];

            }
            $retData[]=$mac;
        }
        return $retData;
    }
    public function getLeagueTable($league, $week)
    {
        if($_GET["order"]=="normal"){
            return getLeagueTable($this->database,$league,$week);
        }else if ($_GET["order"]=="home"){
            return getLeagueTableHome($this->database,$league,$week);

        }else if($_GET["order"]=="away"){
            return getLeagueTableAway($this->database,$league,$week);

        }
    }

    public function getLeaguesTitle()
    {
        $lig = $this->database->query("SELECT id,league_name,week_count,AKTIF_HAFTA FROM leagues WHERE sezon='".SEZON."'")->fetchAll(PDO::FETCH_ASSOC);
       return $lig;
    }

}

$id =     "";
$secret ="";
$func =   isset($_GET["func"])?$_GET["func"]:"";
if($_POST){
    $id =     $_POST["id"];
    $secret = $_POST["secret"];
    $func =   isset($_GET["func"])?$_GET["func"]:"";
}
$api = new Api($id,$secret,$func, $database);
