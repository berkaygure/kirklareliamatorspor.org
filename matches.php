<? include_once "_header_.php" ?>
<?


?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-12">
                <h4 class="blog-title">Haftanın Maçları</h4>
                <table style="word-break: break-all" class="table table-hover text-md-center  table-striped table-sm">
                    <thead>
                        <tr>
                            <th class="text-md-center">Tarih</th>
                            <th class="text-md-center">Ev Sahibi</th>
                            <th class="text-md-center"></th>
                            <th class="text-md-center">Misafir</th>
                            <th class="text-md-center">Lig/Grup</th>
                            <th class="text-md-center">Saha</th>
                        </tr>
                    </thead>
                    <tbody>
                <?
                $datas = $database->query("SELECT * FROM maclar WHERE tarih >= DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY) and tarih <= DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY), INTERVAL 7 DAY) ORDER BY LIG_ID ASC")->fetchAll(PDO::FETCH_ASSOC);
                foreach ($datas as $data):
                    $lig = $database->query("SELECT league_name FROM leagues WHERE ID=" . $data["LIG_ID"])->fetch(PDO::FETCH_ASSOC);
                    $home = $database->query("SELECT tname FROM takimlar WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
                    $away = $database->query("SELECT tname FROM takimlar WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC); ?>
                    <?
                    $tarih = explode(":",explode(" ",$data["tarih"])[1]);
                    ?>
                    <tr data-href="<?=$config["base"]?>mac/<?=$data["id"]?>" class="clickable-row">
                        <td>
                            <span class="tag tag-warning"><?=turkcetarih('j.M.Y',$data["tarih"])?></span><br>
                            <span class="tag tag-warning"><?=$tarih[0]?>:<?=$tarih[1]?></span>
                        </td>
                        <td class="text-md-center" ><?=$home["tname"]?></td>
                        <td class="text-md-center">
                            <? if($data["hsocre"]==-1 || $data["ascore"]==-1) {?>
                                vs

                            <? } else {?>
                                <span class="tag tag-warning"><?=$data["hscore"]?></span> -
                                <span class="tag tag-warning"><?=$data["ascore"]?></span>
                            <? }?>
                        </td>
                        <td class="text-md-center"><?=$away["tname"]?></td>
                        <td class="text-md-center"><?=$lig["league_name"]?></td>
                        <td class="text-md-center" ><?=$data["STAD"]?></td>

                    </tr>
                <?endforeach; ?>
                </tbody>
                </table>

            </div>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
