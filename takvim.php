<?php
include "yonetim/config.php";
require('yonetim/libs/fpdf/rotation.php');

class LigYazdir extends PDF_Rotate
{
    private $database;
    private $ligData;
    private $cellSize;
    public function __construct($orientation, $unit, $size, PDO $database, $ligID)
    {
        $this->database = $database;
        parent::__construct($orientation, $unit, $size);
        $this->ligData = $this->getLigData($ligID);
    }


    function Header()
    {
        $this->SetFontSize(14);

        $this->Cell(15,30);
        $this->MultiCell(0,10,$this->turkce("KIRKLARELİ AMATÖR SPOR KULÜPLERİ FEDERASYONU"),0,"C",false);
        $this->SetFontSize(11);
        $this->MultiCell(0,10,$this->turkce($this->ligData["league_name"] . " TAKVİMİ"),0,"C",false);
        $this->Cell(15,30);

        $this->Image("img/logo.png",10,5,20,20);
        $this->Image("img/clients/client-1.png",($this->w-50),5,40,20);

        $this->SetDrawColor(242,242,242);

        $this->Line(10,30,$this->w-10,30);
        $this->Ln(10);
        /*
        $this->SetTextColor(242,242,242);
        $this->SetFontSize(22);
        $water = 'K I R K L A R E L I  A S K F';
        $this->RotatedText(($this->w/2) - ($this->GetStringWidth($water)/2),$this->h/2,$water,45);*/
    }

    private function getLigData($id){
        $ligDataQuery = $this->database->prepare("SELECT * FROM leagues WHERE ID=:id");
        $ligDataQuery->bindValue(":id", $id);
        $ligDataQuery->execute();
        $ligData = $ligDataQuery->fetch(PDO::FETCH_ASSOC);
        return $ligData;
    }

    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }

    function MacYaz()
    {
        $this->Init();
        $height = 0;
        for($i = 0; $i < $this->ligData["week_count"]; $i+=2){
            $height += 10;

            if($height == 120){
                $this->AddPage();

                $height=0;
            }
            $this->HaftaHeader($i);
            $height = $this->DrawMatch(($i+1),($i+2), $height);
            $this->Ln(6);

        }

       /* $i = 0;
        foreach ($fikstur as $f){
            $home = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $f["hteam"])->fetch(PDO::FETCH_ASSOC);
            $away = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $f["ateam"])->fetch(PDO::FETCH_ASSOC);
            $this->HaftaHeader($i);




            $i++;
        }*/

    }

    private function Init(){
        $this->AddPage();
        $this->cMargin=5;
        $this->SetFillColor(70, 157, 244);
        $this->SetDrawColor(0,0,0);
        $this->SetFontSize(11);
        $this->SetTextColor(255,255,255);
        $this->cellSize = ($this->w)/2;
    }

    private function HaftaHeader($i){
        $this->SetFontSize(11);
        $this->SetTextColor(255,255,255);

        $this->Cell($this->cellSize-5,5,"# H a f t a  " . ($i+1),1,0,'C',true);
        $this->Cell(2.2,5);
        $this->Cell($this->cellSize-5,5,"# H a f t a  " . ($i+2),1,0,'C',true);
        $this->Ln();
        $this->SetFontSize(8);
        $this->SetTextColor(0,0,0);

        $this->Cell($this->cellSize *0.14,5,"Tarih Saat",1,0,'C',false);
        $this->Cell($this->cellSize *0.30,5,"Ev Sahibi",1,0,'C',false);
        $this->Cell($this->cellSize *0.30,5,"Misafir",1,0,'C',false);
        $this->Cell($this->cellSize *0.226,5,"Saha",1,0,'C',false);
        $this->Cell(2.2,5);
        $this->Cell($this->cellSize *0.14,5,"Tarih Saat",1,0,'C',false);
        $this->Cell($this->cellSize *0.30,5,"Ev Sahibi",1,0,'C',false);
        $this->Cell($this->cellSize *0.30,5,"Misafir",1,0,'C',false);
        $this->Cell($this->cellSize *0.226,5,"Saha",1,0,'C',false);
        $this->Ln();
    }
    private function DrawMatch($hafta1, $hafta2, $height){
        $h = $height;
        $fikstur1 = $this->database->query("SELECT * FROM  maclar WHERE  LIG_ID=" .$this->ligData["ID"]. " and HAFTA=" . ($hafta1))->fetchAll(PDO::FETCH_ASSOC);
        $fikstur2 = $this->database->query("SELECT * FROM  maclar WHERE  LIG_ID=" . $this->ligData["ID"] . " and HAFTA=" . ($hafta2))->fetchAll(PDO::FETCH_ASSOC);
        for($i = 0; $i < count($fikstur1); $i++){
            $home1 = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $fikstur1[$i]["hteam"])->fetch(PDO::FETCH_ASSOC);
            $away1 = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $fikstur1[$i]["ateam"])->fetch(PDO::FETCH_ASSOC);
            $home2 = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $fikstur2[$i]["hteam"])->fetch(PDO::FETCH_ASSOC);
            $away2 = $this->database->query("SELECT tname FROM takimlar WHERE id=" . $fikstur2[$i]["ateam"])->fetch(PDO::FETCH_ASSOC);
            $this->SetFontSize(6);
            $this->Cell($this->cellSize *0.14,10,$this->turkce(($fikstur1[$i]["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$fikstur1[$i]["tarih"])),1,0,'C',false);
            $this->Cell($this->cellSize *0.30,10,$this->turkce($home1["tname"]),1,0,'C',false);
            $this->Cell($this->cellSize *0.30,10,$this->turkce($away1["tname"]),1,0,'C',false);
            $this->Cell($this->cellSize *0.226,10,$this->turkce($fikstur1[$i]["STAD"]),1,0,'C',false);
            $this->Cell(2.2,5);
            $this->Cell($this->cellSize *0.14,10,$this->turkce(($fikstur2[$i]["tarih"] == "0000-00-00 00:00:00")?"-":turkcetarih('j.M.Y H.i',$fikstur2[$i]["tarih"])),1,0,'C',false);
            $this->Cell($this->cellSize *0.30,10,$this->turkce($home2["tname"]),1,0,'C',false);
            $this->Cell($this->cellSize *0.30,10,$this->turkce($away2["tname"]),1,0,'C',false);
            $this->Cell($this->cellSize *0.226,10,$this->turkce($fikstur2[$i]["STAD"]),1,0,'C',false);
            $this->Ln();
            $h+=10;

        }
        return $h;
    }


    /*
    function ChapterTitle($label)
    {
//Title
        $this->SetFont('Arial','',12);
        $this->SetFillColor(200,220,255);
        $this->Cell(0,6,"$label",0,1,'L',true);
        $this->Ln(4);
//Save ordinate
        $this->y0=$this->GetY();
    }*/
    /*
        function ChapterBody($file)
        {

            $this->SetFont('Arial','',12);
            $this->MultiCell(180,5,"asdasd");
            $this->Ln();
            $this->SetFont('','I');
            $this->Cell(0,5,'(sayfa sonu)');
        }
    */
    function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->Cell(0,10,'Sayfa '.$this->PageNo(),0,0,'C');
    }

    /*
        function PrintChapter($title,$file)
        {
    //Add chapter
            $this->AddPage();
            $this->ChapterTitle($title);
            $this->ChapterBody($file);
        }*/
    function turkce($k)
    {
        return iconv("utf-8","ISO-8859-9",$k);
    }
}
if($_GET["lig"]!="") {
    $pdf = new LigYazdir('L', 'mm', "A4", $database, $_GET["lig"]);
    $pdf->AddFont('arial_tr', '', 'arial_tr.php');
    $pdf->AddFont('arial_tr', 'B', 'arial_tr_bold.php');
    $pdf->SetFont('arial_tr', '', 14);
    $pdf->SetTitle("Kırklareli Amatör Spor Kulüpleri Federasyonu");
    $pdf->SetMargins(5, 5, 5);
    $pdf->SetAuthor("Kırklareli ASKF", true);
    $pdf->MacYaz();

    $pdf->Output();
}