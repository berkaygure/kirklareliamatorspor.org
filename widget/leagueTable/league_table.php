<?php
include_once "../../yonetim/config.php";

if($_POST)
{

    $ligID = $_POST["id"];
    $ligler = $database->query("SELECT * FROM leagues WHERE ID=".$ligID."")->fetch(PDO::FETCH_ASSOC);
    $hafta = (isset($_POST["hafta"])?$_POST["hafta"]:$ligler["AKTIF_HAFTA"]);
    $table = getLeagueTable($database,$ligID,$hafta);
    ?>
    <?for($i=0; $i < count($table); $i++):?>
        <tr>
            <th scope="row"><?=($i+1)?></th>
            <td><?=$table[$i]["Team"]?></td>
            <td  class="text-md-right"><strong><?=$table[$i]["Pts"]?></strong></td>
        </tr>
    <?endfor;?>
    <?
}else{
    $ligler = $database->query("SELECT * FROM leagues WHERE SEZON='".$config["sezon"]."' ORDER BY ID ASC")->fetchAll(PDO::FETCH_ASSOC);
    $selectedIndex =0;
    $ligID = $ligler[$selectedIndex]["ID"];
    $hafta = $ligler[$selectedIndex]["AKTIF_HAFTA"];
    $table = getLeagueTable($database,$ligID,$hafta);
?>
<div class="card card-outline-success" style="position: relative" id="leagueTablePanel">
    <div class="card-header">
        <h6><i class="fa fa-calendar-check-o"></i> Liglerde Puan Durumu</h6>
    </div>
    <div class="card-block">
        <div id="leagueTableOverlay" class="panel-overlay"><img src="img/loading.gif"> </div>
        <form style="margin-bottom: 5px;">
            <select id="changeLeague" class="form-control">
                <?$i=0;?>
                <?foreach ($ligler as $lig):?>
                    <option <?=($i==$selectedIndex)?"selected":""?> value="<?=$lig["ID"]?>"><?=$lig["league_name"]?></option>
                    <?$i++;?>
                <?endforeach;?>
            </select>
        </form>
        <table style="color:#333 !important;" class="table  table-striped table-sm">

            <tbody id="leagueContent">
            <?for($i=0; $i < count($table); $i++):?>
            <tr>
                <th scope="row"><?=($i+1)?></th>
                <td><?=$table[$i]["Team"]?></td>
                <td  class="text-md-right"><strong><?=$table[$i]["Pts"]?></strong></td>
            </tr>
            <?endfor;?>

            </tbody>
        </table>
        <a style="color:#fff" id="detayliPuanDurumu" class="btn btn-sm btn-primary"><i class="fa fa-calendar"></i>  Detaylı Puan Durumu</a>

    </div>
</div>
<?
}
?>