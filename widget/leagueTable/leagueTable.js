$(function () {
   $("#changeLeague").change(function (e) {
       var leagueId = this.value;

       $('#leagueTableOverlay').css("display","block");

       $.ajax({
           type:"post",
           url:"widget/leagueTable/league_table.php",
           data:"id="+leagueId,
           success: function (response) {
               $('#leagueTableOverlay').fadeOut("slow");
               $("#leagueContent").html(response);
           }
       })
   });

    $("#detayliPuanDurumu").click(function () {
         var leagueId = $("#changeLeague").val();
        window.location.href = "leagues.php?id=" + leagueId;
    });
});