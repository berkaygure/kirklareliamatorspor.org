<?php
include_once "../../yonetim/config.php";?>
    <div class="card card-outline-success">
        <div class="card-header">
            <h6><i class="fa fa-calendar-check-o"></i> Haftanın Maçları</h6>
        </div>
        <div class="card-block" style="padding: 3px">

            <table style="word-break: break-all;color : #333 !important;" class="table table-hover  table-striped table-sm">

                <tbody>


<?
$date = date('Y-m-d H:i:s',time()-(7*86400)); // 7 days ago
$datas = $database->query("SELECT * FROM maclar WHERE tarih >= DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY) and tarih <= DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY), INTERVAL 7 DAY) ORDER BY tarih ASC LIMIT 0,10 ")->fetchAll(PDO::FETCH_ASSOC);
foreach ($datas as $data):
    $home = $database->query("SELECT tname FROM takimlar WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
    $away = $database->query("SELECT tname FROM takimlar WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC); ?>
    <?
    $tarih = explode(":",explode(" ",$data["tarih"])[1]);
    ?>
    <tr data-href="<?=$config["base"]?>mac/<?=$data["id"]?>" class="clickable-row">
        <td class="text-md-center" ><?=$home["tname"]?></td>
        <td class="text-md-center">
            <? if($data["hsocre"]==-1 || $data["ascore"]==-1) {?>
                <span class="tag tag-warning"><?=turkcetarih('j.M.Y',$data["tarih"])?></span><br>
                <span class="tag tag-warning"><?=$tarih[0]?>:<?=$tarih[1]?></span>

            <? } else {?>
                <span class="tag tag-warning"><?=$data["hscore"]?></span> -
                <span class="tag tag-warning"><?=$data["ascore"]?></span>
            <? }?>
        </td>
        <td class="text-md-center"><?=$away["tname"]?></td>
    </tr>
<?endforeach; ?>
                </tbody>
            </table>
            <? $count = $database->query("SELECT * FROM maclar WHERE tarih >= DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY) and tarih <= DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -WEEKDAY(CURRENT_DATE) DAY), INTERVAL 7 DAY) ORDER BY tarih ASC")->fetchAll(PDO::FETCH_ASSOC); ?>
            <a style="margin:10px;color:#fff" href="haftanin-maclari.html" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i>  Tüm Maçlar (<?=count($count)?> tane)</a>

        </div>
    </div>
