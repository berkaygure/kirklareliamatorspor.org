<footer>
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-md-2">
                    <img class="img-fluid" src="img/logo.png" alt="Kırklareli Amatör Spor Kulüplerleri Federasayonu">
                </div>
                <div class="col-md-6" style="color: #000;">
                    <h4>Adres</h4>
                    <p>100. Yıl Cad. Özküçük Apt. No:11 D:2, Kırklareli Merkez/Kırklareli
                    <h4>Telefon</h4>(0288) 212 1911 </p>
                </div>
                <div class="col-md-4"  style="color: #000;">
                    <h4>Toplam Ziyaretçi Sayımız</h4>
                    <p>Sitemizi Şuana Kadar <span class="tag tag-success"> 1.123.075 </span> kere ziyaret edilmiştir. Teşekkür ederiz</p>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="row">
                <div class="col-md-6">© kirklareliamatorspor.org | Tüm Hakları Saklıdır.</div>
                <div class="col-md-6 text-md-right">Meriç Bilgisayar</div>
            </div>
        </div>
    </div>

</footer>
<a href="#0" class="cd-top">Top</a>

</body>
</html>