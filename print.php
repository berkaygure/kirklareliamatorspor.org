<?
include "yonetim/config.php";
$action = $_GET["action"];
$title = "";
switch ($action){
	case "d":
		$duyuru = $database->query("SELECT * FROM duyurular WHERE id=" . $_GET["id"])->fetch(PDO::FETCH_ASSOC);
		$title = strip_tags(stripslashes($duyuru["baslik"]));
		$icerik = stripslashes($duyuru["duyuru"]);
		$tarih = turkcetarih('j F Y',$duyuru["tarih"]);
		break;
	case "fixture":
		// lig ve hafta GET edilmesi gerekiyor
		$hafta = $_GET["hafta"];
		$ligBilgiler  = $database->query("SELECT * FROM leagues WHERE ID=" . $_GET["lig"])->fetch(PDO::FETCH_ASSOC);
		$datalar = $database->query("SELECT * FROM maclar WHERE LIG_ID=" . $_GET["lig"] . " and HAFTA=" . $hafta . " ORDER BY ID")->fetchAll(PDO::FETCH_ASSOC);
		$title = $ligBilgiler["league_name"] . " " . $hafta . ". hafta fikstürü";
		break;
	case "fixtureAll":
		$hafta = $_GET["hafta"];
		$ligBilgiler  = $database->query("SELECT * FROM leagues WHERE ID=" . $_GET["lig"])->fetch(PDO::FETCH_ASSOC);
		$title = $ligBilgiler["league_name"] . " Tüm Lig fikstürü";
		break;
	case "table":
		$hafta = $_GET["hafta"];
		$ligBilgiler  = $database->query("SELECT * FROM leagues WHERE ID=" . $_GET["lig"])->fetch(PDO::FETCH_ASSOC);
		$datalar = $database->query("SELECT * FROM maclar WHERE LIG_ID=" . $_GET["lig"] . " and HAFTA=" . $hafta . " ORDER BY ID")->fetchAll(PDO::FETCH_ASSOC);
		$title = $ligBilgiler["league_name"] . " " . $hafta . ". hafta puan durumu ve fikstürü";
		break;
	default:
		break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title><?=$title?> - Kırklareli Amatör Spor Kulüpleri Federasyonu</title>
    <link rel="stylesheet" href="font/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/style.css"/>
    <style>
        body {
            background-image: none;
            margin:10px;
            color:#000;
        }

    </style>
    <script>
        window.print();
    </script>
</head>
<body>
<div class="container" >
	<?if($action == "d"):?>
        <div class="row">
            <div class="col-md-12 blogShort">
                <h3 class="blog-title"><?=$title?></h3>
                <span class="tag tag-primary"><i class="fa fa-calendar"></i> Yayım Tarihi: <?=$tarih;?></span>
                <span class="tag tag-success"><i class="fa fa-eye"></i> Okunma: <?=$duyuru["okunma"]?></span>
                <hr>
				<?if($duyuru["resimgoster"]=="1"):?>
                    <img style="width:300px;height: 250px;" src="upload/haberler/<?=$duyuru["resim"]?>" alt="<?=$baslik?>" class="pull-left img-responsive thumb margin10 img-thumbnail">
				<?endif;?>
                <article id="article" style="text-align: justify"><p>
						<?=strip_tags($icerik)?>
                    </p>
                </article>
            </div>
        </div>
	<?endif;?>

	<?if($action == "fixture"):?>
        <div class="row">
            <div class="col-md-12">
                <h2 style="text-align: center">KIRKLARELİ AMATÖR SPOR KULÜPLERİ FEDERASYONU</h2>
                <h3 style="text-align: center"><?=$ligBilgiler["league_name"]?> <?=$hafta?>. HAFTA FİKSTÜRÜ</h3>
                <table cellpadding="10" style="width:800px;" border="1">
                    <tr>
                        <th>Tarih</th>
                        <th>Ev Sahibi</th>
                        <th>Skor</th>
                        <th>Deplasman Takım</th>
                        <th>Stad</th>

                    </tr>
					<?

					$raporlar = array();

					foreach ($datalar as $data) {

						$takim1 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
						$takim2 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC);
						if($data["ACIKLAMA"]!="" && trim($data["ACIKLAMA"])!="YOK"){
							$r["aciklama"] = $data["ACIKLAMA"];
							$r["mac"] = $takim1["tname"] . " - " . $takim2["tname"] . " KARŞILAŞMASI";
							$raporlar[] = $r;
						}
						?>
                        <tr>
                            <td align="center"><?=turkcetarih('j.M.Y H.i',$data["tarih"]);  ?></td>
                            <td align="center"><?=$takim1["tname"]?></td>
                            <td align="center"><?=($data["hscore"]=="-1"?"-":$data["hscore"])?> - <?=($data["ascore"]=="-1"?"-":$data["ascore"])?></td>
                            <td align="center"><?=$takim2["tname"]?></td>
                            <td align="center"><?=$data["STAD"]?></td>
                        </tr>
						<?
					}
					?>

                </table>

				<? if(count($raporlar)> 0 ) {?>
                    <h4>Maç Raporları</h4>
                    <table cellpadding="10" style="width:800px;" border="1">
                        <tr>
                            <td style="font-size:11px">
								<? foreach ($raporlar as $rapor) {?>
                                    * <?=$rapor["aciklama"]?> (<?=$rapor["mac"]?>)<br>
								<?}?>
                            </td>
                        </tr>
                    </table>
				<?}?>
            </div>
        </div>
	<?endif;?>

	<?if($action == "fixtureAll"):?>
    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align: center">KIRKLARELİ AMATÖR SPOR KULÜPLERİ FEDERASYONU</h2>
            <h3 style="text-align: center"><?=$ligBilgiler["league_name"]?> <?=$hafta?> LİG FİKSTÜRÜ</h3>
			<?for($i=1 ; $i <= $ligBilgiler["week_count"];$i++):?>
                <table cellpadding="10" style="width:800px;" border="1">
                    <tr>
                        <th colspan="5"><?=$i?>. HAFTA</th>
                    </tr>
                    <tr>
                        <th>Tarih</th>
                        <th>Ev Sahibi</th>
                        <th>Skor</th>
                        <th>Deplasman Takım</th>
                        <th>Stad</th>

                    </tr>
					<?
					$raporlar = array();

					$datalar = $database->query("SELECT * FROM maclar WHERE LIG_ID=" . $_GET["lig"] . " and HAFTA=" . $i . " ORDER BY ID")->fetchAll(PDO::FETCH_ASSOC);

					foreach ($datalar as $data) {

						$takim1 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
						$takim2 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC);
						if($data["ACIKLAMA"]!="" && trim($data["ACIKLAMA"])!="YOK"){
							$r["aciklama"] = $data["ACIKLAMA"];
							$r["mac"] = $takim1["tname"] . " - " . $takim2["tname"] . " KARŞILAŞMASI";
							$raporlar[] = $r;
						}
						?>
                        <tr>
                            <td align="center"><?=turkcetarih('j.M.Y H.i',$data["tarih"]);  ?></td>
                            <td align="center"><?=$takim1["tname"]?></td>
                            <td align="center"><?=($data["hscore"]=="-1"?"-":$data["hscore"])?> - <?=($data["ascore"]=="-1"?"-":$data["ascore"])?></td>
                            <td align="center"><?=$takim2["tname"]?></td>
                            <td align="center"><?=$data["STAD"]?></td>
                        </tr>
						<?
					}
					?>

                </table>
                <br>
				<? if(count($raporlar)> 0 ) {?>
                    <h4>Maç Raporları</h4>
                    <table cellpadding="10" style="width:800px;" border="1">
                        <tr>
                            <td style="font-size:11px">
								<? foreach ($raporlar as $rapor) {?>
                                    * <?=$rapor["aciklama"]?> (<?=$rapor["mac"]?>)<br>
								<?}?>
                            </td>
                        </tr>
                    </table><br>
				<?}?><br>
			<?endfor;?>
        </div>
		<?endif;?>

		<?if($action == "table"):?>
            <div class="row">
                <div class="col-md-12">
                    <h2 style="text-align: center">KIRKLARELİ AMATÖR SPOR KULÜPLERİ FEDERASYONU</h2>
                    <h3 style="text-align: center"><?=$ligBilgiler["league_name"]?> <?=$_GET["hafta"]?>. HAFTA FİKSTÜR VE PUAN DURUMU</h3>
                    <table cellpadding="10" style="width:800px;" border="1">
                        <tr>
                            <th>Tarih</th>
                            <th>Ev Sahibi</th>
                            <th>Skor</th>
                            <th>Deplasman Takım</th>
                            <th>Stad</th>

                        </tr>
						<?

						$raporlar = array();

						foreach ($datalar as $data) {

							$takim1 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["hteam"])->fetch(PDO::FETCH_ASSOC);
							$takim2 = $database->query("SELECT * FROM takimlar  WHERE id=" . $data["ateam"])->fetch(PDO::FETCH_ASSOC);
							if($data["ACIKLAMA"]!="" && trim($data["ACIKLAMA"])!="YOK"){
								$r["aciklama"] = $data["ACIKLAMA"];
								$r["mac"] = $takim1["tname"] . " - " . $takim2["tname"] . " KARŞILAŞMASI";
								$raporlar[] = $r;
							}
							?>
                            <tr>
                                <td align="center"><?=turkcetarih('j.M.Y H.i',$data["tarih"]);  ?></td>
                                <td align="center"><?=$takim1["tname"]?></td>
                                <td align="center"><?=($data["hscore"]=="-1"?"-":$data["hscore"])?> - <?=($data["ascore"]=="-1"?"-":$data["ascore"])?></td>
                                <td align="center"><?=$takim2["tname"]?></td>
                                <td align="center"><?=$data["STAD"]?></td>
                            </tr>
							<?
						}
						?>

                    </table>

					<? if(count($raporlar)> 0 ) {?>
                        <h4>Maç Raporları</h4>
                        <table cellpadding="10" style="width:800px;" border="1">
                            <tr>
                                <td style="font-size:11px">
									<? foreach ($raporlar as $rapor) {?>
                                        * <?=$rapor["aciklama"]?> (<?=$rapor["mac"]?>)<br>
									<?}?>
                                </td>
                            </tr>
                        </table>
					<?}?>
					<?

					$sth = $database->prepare("SELECT * FROM leagues WHERE ID=:id");
					$sth->bindValue(":id", $_GET["lig"]);
					$sth->execute();
					$ligData = $sth->fetchAll(PDO::FETCH_ASSOC);
					$hafta = $hafta;

					?>


                    <br>
                    <h3>Puan Tablosu</h3>
                    <table cellpadding="10" style="width:800px;" border="1">
                        <thead>
                        <tr style="font-size:11px;">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class=" text-md-center">O</th>
                            <th class=" text-md-center">G</th>
                            <th class=" text-md-center">B</th>
                            <th class=" text-md-center">M</th>
                            <th class=" text-md-center">A</th>
                            <th class=" text-md-center">Y</th>
                            <th class=" text-md-center">AV</th>
                            <th class=" text-md-center">P</th>
                        </tr>
                        </thead>
                        <tbody id="tableContent">
						<?php
						$ligID = $_GET["lig"];
						$table = getLeagueTableNew($database,$ligID,$hafta);
						$gecenHafta = false;
						if($hafta > 1){
							$gecenHafta = getLeagueTableNew($database,$ligID,$hafta-1);
						}
						?>
						<?for($i=0; $i < count($table); $i++):?>
                            <tr>
                                <td class=" text-md-center">
									<?if($gecenHafta):?>
										<? $last = getTeamFromTable($gecenHafta,$table[$i]["Team"]);?>
										<?if($last > $i):?>
                                            <i title="Geçen Hafta <?=($last+1)?>" style="color: #2ecc55;" class="fa fa-arrow-up"></i>
										<? elseif($last < $i):?>
                                            <i title="Geçen Hafta <?=($last+1)?>" style="color: #cd0000;" class="fa fa-arrow-down"></i>
										<? else:?>
                                            <i title="Geçen Hafta <?=($last+1)?>" class="fa fa-circle"></i>
										<?endif?>
									<? else:?>
                                        <i class="fa fa-circle"></i>
									<?endif;?>
                                </td>
                                <td>
                                    <h5><?=($i+1)?></h5>
                                </td>
                                <td><?=$table[$i]["Team"]?><br><small><?=$table[$i]["Test"]?></small></td>
                                <td class=" text-md-center"><?=$table[$i]["P"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["W"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["D"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["L"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["F"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["A"]?></td>
                                <td class=" text-md-center"><?=$table[$i]["GD"]?></td>
                                <td class=" text-md-center"><strong><?=$table[$i]["Pts"]?></strong></td>
                            </tr>
						<?endfor;?>
                        </tbody>
                    </table>
                </div>
            </div>
		<?endif;?>
</body>
</html>
