<?
include "_header_.php";

?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-8">
                <h1 class="blog-title">Geçmiş Sezonlar</h1>

                <div class="form-group form-float">
                    <label class="form-label">Sezon Seçin</label>
                    <div class="form-line">

                        <select id="arsiv_select_ligler" class="form-control">
                            <option value="07/08">07/08</option>
                            <option value="08/09">08/09</option>
                            <option value="09/10">09/10</option>
                            <option value="10/11">10/11</option>
                            <option value="11/12">11/12</option>
                            <option value="12/13">12/13</option>
                            <option value="13/14">13/14</option>
                            <option value="14/15">14/15</option>
                            <option value="16/17">16/17</option>
                            <option value="17/18">17/18</option>
			    <option value="18/19">18/19</option>
                        </select>
                    </div>
                </div>


                <div style="margin-top:10px;text-align:center;padding:5px;">Ligler</div>
                <ul id="lig_list" class="ligler">

                </ul>

            </div>
            <div class="col-md-4">

                <div class="card card-outline-success">
                    <div class="card-header">
                        <h6><i class="fa fa-cloud"></i> Kırklareli 5 günlük Hava Tahmini</h6>
                    </div>
                    <ul class="list-group">
                        <img src="http://www.mgm.gov.tr/sunum/tahmin-show-2.aspx?m=KIRKLARELI&basla=0&bitir=5&rC=111&rZ=fff" class="img-fluid" alt="KIRKLARELİ" />
                    </ul>
                </div>
                <? include_once "widget/leagueTable/league_table.php";?>

            </div>
        </div>
    </div>
</div>
<?
include "_footer_.php";

?>
