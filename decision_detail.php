<? include_once "_header_.php" ?>
<?
$data = $database->query("SELECT * FROM kararlar WHERE id=" . $_GET["id"])->fetch(PDO::FETCH_ASSOC);

if(!is_array($data)){
    header("location: index.html");
}
$baslik = strip_tags(stripslashes($data["baslik"]));
$icerik = stripslashes($data["icerik"]);
$tarih = turkcetarih('j F Y',$data["tarih"]);

?>
<div class="container">
    <div class="outer-content">
        <div class="row">
            <div class="col-md-12 blogShort">
                <h4 class="blog-title"><?=$baslik?></h4>

                <article id="article" style="text-align: justify">
                    <p>
                        <?
                        /*
                        $dom = new DOMDocument;
                        $dom->loadHTML(mb_convert_encoding($icerik, 'HTML-ENTITIES', 'UTF-8'));

                        $xpath = new DOMXPath($dom);
                        $nodes = $xpath->query('//*[@style]');
                        foreach ($nodes as $node) {
                            $node->removeAttribute('style');
                        }
                        echo $dom->saveHTML();*/
                        echo $icerik;
                        ?>
                    </p>
                </article>
                <div class="clearfix"></div>

            </div>

        </div>
    </div>
</div>

<? include "_footer_.php"; ?>
